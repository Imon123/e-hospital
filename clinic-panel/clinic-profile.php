<?php
include '../header.php';
include 'csession.php';
?>

        <div class="container sec-pdd1">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-3">
                        <?php include 'sidebar.php'; ?>
                    </div>
                    <div class="col-xs-9">
                        <div class="mystate">
                        <h4 class="dinfotp"><u>Clinic Details</u></h4>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td>Clinic Name</td>
                                <td><?= $c_prf['clinic_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Clinic Location</td>
                                <td><?= $c_prf['clinic_location'] ?></td>
                            </tr>
                            <tr>
                                <td>Owner Type</td>
                                <td><?= $c_prf['ownertype'] ?></td>
                            </tr>
                            <tr>
                                <td>Owner Name</td>
                                <td><?= $c_prf['ownername'] ?></td>
                            </tr>
                            <tr>
                                <td>Clinic Type</td>
                                <td><?= $c_prf['clinictype'] ?></td>
                            </tr>

                            <tr>
                                <td>Licens Number</td>
                                <td><?= $c_prf['licnumber'] ?></td>
                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <td><?= $c_prf['email'] ?></td>
                            </tr>
                            <tr>
                                <td>Mobile Number</td>
                                <td><?= $c_prf['mobile_no'] ?></td>
                            </tr>
                            <tr>
                                <td>Start Date</td>
                                <td><?= $c_prf['startdate'] ?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?= $c_prf['clinicaddress'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
<?php include '../footer.php';?>

