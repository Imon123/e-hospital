<?php
include '../header.php';
include 'csession.php';
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-3">
            <?php include 'sidebar.php'; ?>
        </div>
        <div class="col-xs-9">
            <div class="mystate">
            <div class="row">
                <?php 
                    if($_SERVER['REQUEST_METHOD'] && isset($_POST['cupdate_btn'])){
                      $clinic->update_clinic_profile_by_id($_POST); 
                      $c_prf= $clinic->get_clinic_profile_by_id($cid); 
                    }
                ?>  
            </div>
            <form name="edit_clinic_reg" id="clinicsignup" action="" method="post">
                <h4 class="dinfotp"><u>Clinic Details Update</u></h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="clinic_name">Clinic Name <span class="required">*</span></label>
                            <input type="text" name="clinic_name" class="form-control" id="clinic_name" value="<?= $c_prf['clinic_name']; ?>">
                            <input type="hidden" name="c_id" lass="form-control" id="clinic_name" value="<?= $c_prf['c_id']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="clinic_location">Clinic Location <span class="required">*</span></label>
                            <select name="clinic_location" class="form-control" id="clinic_location">
                                <option value="">Choose Clinic Location </option>
                                <option <?php if($c_prf['clinic_location'] == 'Rural'): echo 'selected'; endif;?> value="Rural">Rural</option>
                                <option <?php if($c_prf['clinic_location'] == 'Urban'): echo 'selected'; endif;?> value="Urban">Urban</option>
                                <option <?php if($c_prf['clinic_location'] == 'Metro'): echo 'selected'; endif;?> value="Metro">Metro</option>
                                <option <?php if($c_prf['clinic_location'] == 'Notified/inaccessible areas'): echo 'selected'; endif;?> value="Critical">Notified/inaccessible areas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="ownertype">Owner Type <span class="required">*</span></label>
                            <select class="form-control" name="ownertype" id="ownertype">
                                <option value="">Choose Type</option>
                                <option <?php if($c_prf['ownertype'] == 'Public'): echo 'selected'; endif;?> value="Public">Governmental</option>
                                <option <?php if($c_prf['ownertype'] == 'Private'): echo 'selected'; endif;?> value="Private">Private</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="ownername">Owner Name <span class="required">*</span></label>
                            <input type="text" name="ownername" class="form-control" id="ownername" value="<?= $c_prf['ownername']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                        <label for="clinictype">Clinic Type<span class="required">*</span></label>
                        <select class="form-control" name="clinictype" id="clinictype">
                            <option value="">Choose Clinic Type</option>
                            <option <?php if($c_prf['clinictype'] == 'Hospital'): echo 'selected'; endif;?> value="Hospital">Hospital</option>
                            <option <?php if($c_prf['clinictype'] == 'Maternity'): echo 'selected'; endif;?> value="Maternity">Maternity</option>
                            <option <?php if($c_prf['clinictype'] == 'Free'): echo 'selected'; endif;?> value="Free">Free Clinics</option>
                            <option <?php if($c_prf['clinictype'] == 'Outpatient Clinic'): echo 'selected'; endif;?> value="Outpatient Clinic">Outpatient Clinic</option>
                            <option <?php if($c_prf['clinictype'] == 'Care Clinics'): echo 'selected'; endif;?> value="Care Clinics">Convenient Care Clinics</option>
                            <option <?php if($c_prf['clinictype'] == 'Specialist Clinics'): echo 'selected'; endif;?> value="Specialist Clinics">Specialist Clinics</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="licnumber">Registration/License Number <span class="required">*</span></label>
                            <input type="text" name="licnumber" class="form-control" id="licnumber" value="<?= $c_prf['licnumber']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="cemail">Email Address <span class="required">*</span></label>
                            <input type="email" name="cemail" class="form-control" id="cemail" value="<?= $c_prf['email']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="mobile_no">Mobile Number <span class="required">*</span></label>
                            <input type="text" name="mobile_no" class="form-control" id="mobile_no" value="<?= $c_prf['mobile_no']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="startdate">Clinic Start Date <span class="optional">(Optional)</span></label>
                            <input type="date" name="startdate" class="form-control" id="startdate" value="<?= $c_prf['startdate']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="clinicaddress">Clinic Address <span class="required">*</span></label>
                            <textarea name="clinicaddress" class="form-control" id="clinicaddress" style="resize: none;"><?= $c_prf['clinicaddress']; ?></textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="cl-submit button btn-primary" id="clinic_button" name="cupdate_btn">Update</button>
            </form>
        </div>
    </div>
    </div>
</div>
<?php
include '../footer.php';
?>
