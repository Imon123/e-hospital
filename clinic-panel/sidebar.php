<div class="leftsidebar">
    <ul>
        <li>
            <a <?php if($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="">My State</a>
        </li>
        <li>
            <a <?php if($current_page == 'clinic-profile'): echo 'class="menu-active"'; endif; ?>  href="clinic-profile">View Profile</a>
        </li>
        <li>
            <a <?php if($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?>  href="edit-profile">Edit Profile</a>
        </li>
    </ul>
</div>
