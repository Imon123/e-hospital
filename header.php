<?php
include_once "helpers/Format.php";
$fm = new Format();

$absolute_url = $fm->full_url($_SERVER);
$urlParts = explode("/", $absolute_url);
$pre_link = '';
if ((in_array('doctor-panel', $urlParts)) || ((in_array('patient-panel', $urlParts))) || ((in_array('clinic-panel', $urlParts)))) {
    $pre_link = '../';
} else {
    $pre_link = '';
}
spl_autoload_register(function( $class ) {
    include_once "controllers/" . $class . ".php";
});
$patient = new Patient();
$doctor = new Doctor();
$clinic = new Clinic();
// get current page from script
$path = $_SERVER['SCRIPT_FILENAME'];
$current_page = basename($path, '.php');
?>
<?php
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: max-age=2592000");
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Design and Implimentation e-health Telemedicine System</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css" />
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!-- google font -->   
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- font awesome -->
        <link rel="stylesheet" type="text/css" href="<?php echo $fm->base_url(); ?>css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/select2.min.css">
        <link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/datepicker3.css">
        <link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/imageuploadify.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $fm->base_url(); ?>fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo $fm->base_url(); ?>fonts/fonts.css"/>	
        <link rel="stylesheet" type="text/css" href="<?php echo $fm->base_url(); ?>style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $fm->base_url(); ?>css/responsive.css"/>

    </head>
    <body onload="fun">
        <header class="header-wrp">
            <div class="top-header-bg clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p class="wel-msg">Welcome to Mediline Center</p>
                        </div>
                        <div class="col-sm-9">
                            <ul class="header-top-social">
                                <?php
                                if (isset($_GET['plogout'])) {
                                    $patient->user_logout();
                                }elseif (isset($_GET['dlogout'])) {
                                    $doctor->unset_doctor_online(Session::session_get('dID'));
                                    $doctor->user_logout();
                                }elseif(isset($_GET['clogout'])){
                                    $doctor->user_logout();
                                }
                                $ptLogin = Session::session_get('ptLogin');
                                $dLogin = Session::session_get('dLogin');
                                $cLogin = Session::session_get('cLogin');
                                if (($ptLogin == false) AND ( $dLogin == false) AND ($cLogin == false)) {
                                    echo '<li><a href="login">Login</a></li>';
                                    echo '<li><a href="create-account">Create account</a></li>';
                                } else {
                                    $userId = '';
                                    $userPanel = '';
                                    $ptID = Session::session_get('ptID');
                                    $dID = Session::session_get('dID');
                                    $cID = Session::session_get('cID');
                                    if (!empty($ptID)) {
                                        $userId = $ptID;
                                        if ($current_page == 'patient-profile' || $current_page == 'edit-patient-profile' || $current_page == 'package' 
                                           || $current_page == 'new-message' || $current_page == 'message-details') {
                                            $userPanel = '';
                                        }elseif((in_array('patient-panel', $urlParts))) { 
                                            $userPanel = '';
                                        }else {
                                            $userPanel = 'patient-panel';
                                        }
                                        echo "<li><a href='" . $userPanel . "?plogout=" . $userId . "'>Logout</a></li>";
                                    } elseif (!empty($dID)) {
                                        $userId = $dID;
                                        if ($current_page == 'doctor-profile' || $current_page == 'edit-profile' 
                                            || $current_page == 'latest-appointment' || $current_page == 'appointment-details') {
                                            $userPanel = '';
                                        }elseif((in_array('doctor-panel', $urlParts))) { 
                                            $userPanel = '';
                                        }else {
                                            $userPanel = 'doctor-panel';
                                        }
                                        echo "<li><a href='" . $userPanel . "?dlogout=" . $userId . "'>Logout</a></li>";
                                    }elseif(!empty($cID)){
                                        $userId = $cID;
                                        if($current_page == 'clinic-profile' || $current_page == 'edit-profile'){
                                            $userPanel = '';
                                        }elseif((in_array('clinic-panel', $urlParts))) { 
                                            $userPanel = '';
                                        }else{
                                            $userPanel = 'clinic-panel';
                                        }
                                        echo "<li><a href='".$userPanel."?clogout=".$userId."'>Logout</a></li>";  
                                    }
                                }
                                if($dLogin == false){
                                ?>
                                <li><i class="fa fa-phone"></i><a href="tel:01822336655">01822336655</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>             
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <h1 class="logo">
                            <a href="<?php echo $fm->base_url(); ?>">
                                <img src="<?php echo $fm->base_url(); ?>images/logo.png" alt="logo">
                            </a>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <nav class="menu-area">
                            <ul class="nav-menu">
                                <li class="active"><a href="<?php echo $fm->base_url(); ?>">Home</a></li>
                                <?php
                                if ($dLogin == true) {
                                    echo "<li><a href='".$fm->base_url().'doctor-panel'."'>Profile</a></li>"; 
                                } else {
                                    if ($ptLogin == true) {
                                        echo "<li><a href='".$fm->base_url().'patient-panel'."'>Profile</a></li>";
                                    }
                                    if ($cLogin == true) {
                                        echo "<li><a href='".$fm->base_url().'clinic-panel'."'>Profile</a></li>";
                                    }
                                    ?>  
                                <?php } ?>
                                <li><a href="<?php echo $fm->base_url(); ?>doctors">Doctor</a></li>
                                <li><a href="<?php echo $fm->base_url(); ?>">Contact</a></li>
                                <?php
                                if ($dLogin == false &&  $cLogin == false) {
                                    echo ' <li class="pull-right emergency-menu"><a class="call-us" href="#"><span>Emergency</span></a></li>';
                                }else{
                                    ?>
                                    <style type="text/css">
                                        .menu-area{
                                            margin-bottom: 5px;
                                        }
                                    </style>
                                    <?php
                                }
                                ?>


                            </ul>
                        </nav>
                    </div>				
                </div>
            </div>
        </header>
        <?php
        $height = '';
        $page_img = '';
        $height = '290';
        if ($current_page == 'login') {
            $height = '200';
            $page_img = 'style="background:rgba(0, 0, 0, 0.50)url(images/page-header.jpg); background-size: cover; background-position: center center; background-repeat: no-repeat; height: ' . $height . 'px; "';
        } elseif ($pre_link == '../' || $current_page == 'doctors' || $current_page == 'doctor-details') {
            $page_img = '';
        } else {
            $page_img = 'style="background:rgba(0, 0, 0, 0.50)url(images/page-header.jpg); background-size: cover; background-position: center center; background-repeat: no-repeat; height: ' . $height . 'px; "';
        }
        if (!empty($page_img)) {
            ?>
            <section class="banner-session" <?php echo $page_img; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="doc-sp hb-text-<?php echo $height; ?>">
                                <div class="inner-doc-sp">
                                    <h4>Advanced Technology</h4>
                                    <h1>For all your medical needs</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        <?php } else { ?>
            <div class="hd-btm-bar"></div>
        <?php } ?>