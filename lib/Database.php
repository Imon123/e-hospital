<?php
	$path = realpath(dirname(__FILE__));
	include_once($path.'/../config/config.php');
/*
* Database class
*/

class Database{

	private $host 	= HOST;
	private $user 	= USER;
	private $pass 	= PASS;
	private $db_name = DB_NAME;

	public $link;
	public $error;

	// database construct function
	public function __construct(){
		$this->DB_connect();
	}

	// database connection
	private function DB_connect(){
		$this->link = new mysqli($this->host, $this->user, $this->pass, $this->db_name);
		if(!$this->link):
			$this->error = "Database Connection Error". $this->link->connect_error;
			return false;
		endif;
	}

	// db insert query function below
	public function insert( $query ){
		$insert_row = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $insert_row ){
			return $insert_row;
		}else{
			return false;
		}
	}

	// db select query function below
	public function select( $query ){
		$result = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $result->num_rows > 0 ){
			return $result;
		}else{
			return false;
		}
	}


	// db update query function below
	public function update( $query ){
		$update_row = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $update_row ){
			return $update_row;
		}else{
			return false;
		}
	}

	// db delete query function below
	public function delete( $query ){
		$delete_row = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $delete_row ){
			return $delete_row;
		}else{
			return false;
		}

	}
}

?>