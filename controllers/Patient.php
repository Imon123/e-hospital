<?php

$path = realpath(dirname(__FILE__));
include_once($path . '/../lib/Main_controller.php');
$fmp = new Format();
$absolute_url = $fmp->full_url($_SERVER);
$urlParts = explode("/", $absolute_url);
if ((in_array('doctor-panel', $urlParts)) || ((in_array('patient-panel', $urlParts))) || ((in_array('clinic-panel', $urlParts)))) {
    include '../lib/Session.php';
} else {
    include 'lib/Session.php';
}
Session::init();

/*
 * Admin Login class
 */

/**
 * 
 */
class Patient extends Main_controller {

    public function __construct() {
        parent::__construct();
    }

    public function patient_signup($post) {
        $data['first_name'] = $this->fm->sanitize($post['f_name']);
        $data['last_name'] = $this->fm->sanitize($post['l_name']);
        $data['father_name'] = $this->fm->sanitize($post['fa_name']);
        $data['mother_name'] = $this->fm->sanitize($post['mo_name']);
        $data['email'] = $this->fm->sanitize($post['email']);
        $data['mobile_no'] = $this->fm->sanitize($post['mobile_no']);
        $data['height'] = $this->fm->sanitize($post['height']);
        $data['weight'] = $this->fm->sanitize($post['weight']);
        $data['blood_group'] = $this->fm->validation($post['bl_group']);
        $data['nid'] = $this->fm->sanitize($post['nid']);
        $data['gender'] = $this->fm->sanitize($post['gender']);
        $data['address'] = $this->fm->sanitize($post['address']);
        $data['birthday'] = $this->fm->sanitize($post['birthday']);
        $password = $this->fm->sanitize($post['password']);
        $data['password'] = $this->fm->password_encrypt($password);
        $data['account_type'] = $this->fm->sanitize($post['acc_type']);
        if ($data['account_type'] == 'Free Trial') {
            $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $data['expire_start'] = $ex_dt->format('Y-m-d H:i:s');
            $data['expire_date'] = '7';
        }
        if ($post['ptbtn']) {
            $email = $data['email'];
            $sql = "SELECT email FROM patients WHERE email = '$email'";
            $ck_mail = $this->db->select($sql);
            if ($ck_mail != false) {
                echo "checkmail";
            } else {
                $insertPt = $this->insert_data_by_array('patients', $data);
                if ($insertPt) {
                    $insertId = $this->db->link->insert_id;
                    echo "mail";
                    /* $to = $data['email'];
                      $from = 'azizul.softsw@gmail.com';
                      $subject = 'Activation Email';
                      // To send HTML mail, the Content-type header must be set
                      $headers  = 'MIME-Version: 1.0' . "\r\n";
                      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                      // Create email headers
                      $headers .= 'From: '.$from."\r\n".
                      'Reply-To: '.$from."\r\n" .
                      'X-Mailer: PHP/' . phpversion();

                      // Compose a simple HTML email message
                      $message = '<html><body>';
                      $message .= '<div class="mailbody"><h1 style="color:#f40;">Hi "'.$data['first_name'].'"!</h1>';
                      $message .= '<div style="color:#f40;"><strong>User ID:</strong> "'.$to.'"!</div>';
                      $message .= '<div style="color:#f40;"><strong>Password:</strong> "'.$password.'"!</div>';
                      $message .= '<div><p style="color:#080;font-size:18px;">Will you marry me?</p>';
                      $message .= '<p style="color:#080;font-size:18px;">To activation your account click the link below :</p>';
                      $message .= '<p style="color:#080;font-size:18px;">Activation Link :
                      <a href="'.'login.php?activated='.$insertId.'">Activated Account</a></p></div>';
                      $message .= '</div></body></html>';
                      $send_msg = mail( $to, $subject, $message, $headers );
                      if($send_msg){
                      echo "yes";
                      }else{
                      echo "nosend";
                      } */
                } else {
                    echo 'no';
                }
            }
        }
    }

    // customer check login
    public function check_login($post) {
        $email = $this->fm->sanitizeReal($this->db->link, $post['email']);
        $password = $this->fm->sanitizeReal($this->db->link, $post['password']);
        $password = $this->fm->password_encrypt($password);
        $query = "SELECT * FROM patients WHERE email = '$email' AND password = '$password'";
        $result = $this->db->select($query);
        if ($result != false) { // Success
            $value = $result->fetch_assoc();
//            var_dump($value);
            Session::session_set('ptLogin', true);
            Session::session_set('Fullname', $value['first_name'] . ' ' . $value['last_name']);
            Session::session_set('ptID', $value['pt_id']);
            //header("Location:patient-penal.php");
            echo "succesLogon";
        } else {
            echo "unsuccessLogin";
        }
    }

    public function get_patient_profile_by_id($pid) {
        $pt_id = $this->fm->sanitizeReal($this->db->link, $pid);
        $query = "SELECT * FROM patients WHERE pt_id = '$pt_id'";
        $result = $this->db->select($query)->fetch_assoc();
        return $result;
    }

    public function update_patient_profile_by_id($post) {
        $pt_id = $this->fm->sanitizeReal($this->db->link, $post['pt_id']);
        $first_name = $this->fm->sanitizeReal($this->db->link, $post['first_name']);
        $last_name = $this->fm->sanitizeReal($this->db->link, $post['last_name']);
        $father_name = $this->fm->sanitizeReal($this->db->link, $post['father_name']);
        $mother_name = $this->fm->sanitizeReal($this->db->link, $post['mother_name']);
        $email = $this->fm->sanitizeReal($this->db->link, $post['email']);
        $mobile_no = $this->fm->sanitizeReal($this->db->link, $post['mobile_no']);
        $height = $this->fm->sanitizeReal($this->db->link, $post['height']);
        $weight = $this->fm->sanitizeReal($this->db->link, $post['weight']);
        $blood_group = $this->fm->validation($post['blood_group']);
        $nid = $this->fm->sanitizeReal($this->db->link, $post['nid']);
        $gender = $this->fm->sanitizeReal($this->db->link, $post['gender']);
        $address = $this->fm->sanitizeReal($this->db->link, $post['address']);
        $birthday = $this->fm->sanitizeReal($this->db->link, $post['birthday']);
        $account_type = $this->fm->sanitizeReal($this->db->link, $post['account_type']);
        $query = "UPDATE patients
            SET
            first_name = '$first_name',
            last_name = '$last_name',
            father_name = '$father_name',
            mother_name = '$mother_name',
            email = '$email',
            mobile_no = '$mobile_no',
            height = '$height',
            weight = '$weight',
            blood_group = '$blood_group',
            nid = '$nid',
            gender = '$gender',
            address = '$address',
            birthday = '$birthday',
            account_type = '$account_type'
            WHERE pt_id = '$pt_id'";
        $updateInfo = $this->db->update($query);
        if ($updateInfo) {
            echo "<script>alert('The information has been update Successfully');</script>";
        } else {
            echo "<script>alert('Not update');</script>";
        }
    }

    public function get_expire_membership($post) {
        $pt_prf = $this->get_patient_profile_by_id($post['pid']);
        $userMembershipDate = $pt_prf['expire_start'];
        $membershipEnds = date("Y-m-d ", strtotime(date("Y-m-d", strtotime($userMembershipDate)) . ' + ' . $pt_prf['expire_date'] . ' day'));
        $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        if ($pt_prf['expire_date'] == '') {
            echo '<h4 class="expired">Please make first membership! <a href="package">click here</a></h4>';
        } else {
            if ($ex_dt->format('Y-m-d') < $membershipEnds) {
                $date1 = date("Y-m-d", strtotime($userMembershipDate));
                $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                $date2 = $ex_dt->format('Y-m-d');
                $datetime1 = new DateTime($date1);
                $datetime2 = new DateTime($date2);
                $interval = $datetime1->diff($datetime2);
                $days = ($pt_prf['expire_date'] - $interval->format('%a'));
                if ($days > 1) {
                    echo "<h4 class='not-expired'>Membership Expires in $days days</h4>";
                } else {
                    echo "<h4 class='not-expired'>Membership Expires in $days day</h4>";
                }
            } else {
                echo '<h4 class="expired">Membership has expired. Please renew! <a href="package">click here</a></h4>';
            }
        }
    }

    public function save_membership_package($post) {
        $data['pt_id'] = $this->fm->sanitize($post['pid']);
        $data['transection_id'] = $this->fm->sanitize($post['trans_id']);
        $data['amount'] = $this->fm->sanitize($post['amount']);
        $data['package_day'] = $this->fm->sanitize($post['package']);
        $insertPt = $this->insert_data_by_array('payment_history', $data);
        if ($insertPt) {
            echo "yes";
        } else {
            echo 'no';
        }
    }

    public function get_payment_history_by_id($pid) {
        $pt_id = $this->fm->sanitizeReal($this->db->link, $pid);
        $query = "SELECT * FROM payment_history WHERE pt_id = '$pt_id' ORDER BY pay_date DESC LIMIT 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function make_appointment($files, $post) {
        if (!empty($files['file']['name'])) {
            for ($i = 0; $i < count($files['file']['name']); $i++) {
                $validextensions_for_image = array("jpeg", "jpg", "png");
                $ext = explode('.', basename($files['file']['name'][$i]));
                $file_extension = end($ext);
                if (!empty($files['file']['name']) && in_array($file_extension, $validextensions_for_image)) {
                    $targetPath = "uploads/" . time() . '_' . $files['file']['name'][$i];
                    if (move_uploaded_file($files['file']['tmp_name'][$i], $targetPath)) {
                        $imageUrl[] = $targetPath;
                    }
                }
            }
        }
        if (!empty($files['audiovideo']['name'])) {
            $validextensions_for_video_audio = array("mp3", "avi", "mov", "mp4", "wmv", "3GP", "amv", "ogg", "Ogg");
            $ext = explode('.', basename($files['audiovideo']['name']));
            $file_extension = end($ext);
            if (in_array($file_extension, $validextensions_for_video_audio)) {

                $targetPaths = "audiovideo/" . basename($files['audiovideo']['name']);
                $audiovideo = '';
                if (move_uploaded_file($files['audiovideo']['tmp_name'], $targetPaths)) {
                    $audiovideo = $targetPaths;
                }
            } else {
                echo '<span id="error">***Invalid file Type***</span><br/><br/>';
            }
        }

        $data['did'] = $this->fm->sanitize($post['d_id']);
        $data['ptid'] = $this->fm->sanitize($post['pt_id']);
        $data['problem'] = $this->fm->sanitize($post['problem']);
        $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $data['created_at'] = $ex_dt->format('Y-m-d H:i:s');
        if (!empty($imageUrl)) {
            $multiple_img = implode(', ', $imageUrl);
            $data['problem_image'] = $multiple_img;
        }
        if (!empty($targetPaths)) {
            $data['audio_video'] = $targetPaths;
        }
        $insertPt = $this->insert_data_by_array('tbl_appointment', $data);
        if ($insertPt) {
            echo "<script>alert('Appointment Request has been sent Successfully');</script>";
        } else {
            echo "<script>alert('Not sent');</script>";
        }
    }
    public function conversation_doctor_to_patient($files, $post){
        if(!empty($files['file']['name']) || !empty($files['audiovideo']['name']) || !empty($post['message'])){
                    if (!empty($files['file']['name'])) {
            for ($i = 0; $i < count($files['file']['name']); $i++) {
                $validextensions_for_image = array("jpeg", "jpg", "png");
                $ext = explode('.', basename($files['file']['name'][$i]));
                $file_extension = end($ext);
                if (!empty($files['file']['name']) && in_array($file_extension, $validextensions_for_image)) {
                    $targetPath = "../uploads/" . time() . '_' . $files['file']['name'][$i];
                    if (move_uploaded_file($files['file']['tmp_name'][$i], $targetPath)) {
                        $imageUrl[] = $targetPath;
                    }
                }
            }
        }
        if (!empty($files['audiovideo']['name'])) {
            $validextensions_for_video_audio = array("mp3", "avi", "mov", "mp4", "wmv", "3GP", "amv", "ogg", "Ogg");
            $ext = explode('.', basename($files['audiovideo']['name']));
            $file_extension = end($ext);
            if (in_array($file_extension, $validextensions_for_video_audio)) {

                $targetPaths = "../audiovideo/" . basename($files['audiovideo']['name']);
                $audiovideo = '';
                if (move_uploaded_file($files['audiovideo']['tmp_name'], $targetPaths)) {
                    $audiovideo = $targetPaths;
                }
            } else {
                echo '<span id="error">***Invalid file Type***</span><br/><br/>';
            }
        }

            $data['appoint_id'] = $this->fm->sanitize($post['appoint_id']);
            $data['message'] = $this->fm->sanitize($post['message']);
            $data['sender'] = $this->fm->sanitize($post['sender']);
            if (!empty($imageUrl)) {
                $multiple_img = implode(', ', $imageUrl);
                $data['image'] = $multiple_img;
            }
            if (!empty($targetPaths)) {
                $data['media'] = $audiovideo;
            }
            $ap_id = $data['appoint_id'];
            $query1 = "UPDATE tbl_appointment SET 
                      patient_status = 'unread'
                      WHERE patient_status = 'read' AND  appoint_id = '$ap_id'"; 
            $this->db->update($query1);
            $insertPt = $this->insert_data_by_array('discussion', $data);
            if ($insertPt) {
                echo "<script>alert('Message has been sent Successfully');</script>";
            } else {
                echo "<script>alert('Not sent');</script>";
            }    
        }else{
            echo "<script>alert('at least one field must be filled out');</script>";

        }

    }
    public function delete_patient_photo($pt_id) {
        $pt_id = $this->fm->sanitizeReal($this->db->link, $pt_id);
        $sql = "SELECT profile_img FROM patients WHERE pt_id = '$pt_id' LIMIT 1";
        $del_img = $this->db->select($sql);
        if ($del_img) {
            while ($row = $del_img->fetch_assoc()) {
                $del_full = $row['profile_img'];
                unlink($del_full);
            }
        }
        $query = "UPDATE patients SET
        profile_img = ''
        WHERE pt_id = '$pt_id'";
        $update = $this->db->update($query);
        if ($update) {
            echo "yes";
        } else {
            echo "no";
        }
    }

    public function upload_patient_profile_image($files, $p_id) {
        if (isset($files["file"]["type"])) {
            $validextensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $files["file"]["name"]);
            $sourcePath = $files['file']['tmp_name']; // Storing source path of the file in a variable
            $file_extension = strtolower(end($temporary));
            list($width, $height) = @getimagesize($sourcePath);

            if (empty($files["file"]["name"])) {
                echo "<span id='invalid'>Please select image</span>";
            } elseif ($width > "300" || $height > "300") {
                echo "<span id='invalid'>Error : image size must be 300 x 300 pixels.</span>";
            } else if ($files["file"]["size"] > 102400) {
                echo "<span id='invalid'>Image Size Should Be 100kb</span>";
            } elseif (in_array($file_extension, $validextensions) === FALSE) {
                echo "<span id='invalid'>You can upload only :-" . implode(', ', $validextensions) . "</span>";
            } else {
                if ($files["file"]["error"] > 0) {
                    echo "Return Code: " . $files["file"]["error"] . "<br/><br/>";
                } else {
                    if (file_exists("uploads/" . $files["file"]["name"])) {
                        echo "<span id='invalid'><span>" . $files["file"]["name"] . "</span><b>already exists.</b></span></span>";
                    } else {
                        $targetPath = "uploads/" . $files['file']['name']; // Target path where file is to be stored
                        move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                        $query = "UPDATE patients SET
                        profile_img = '$targetPath'
                        WHERE pt_id = '$p_id'";
                        $update = $this->db->insert($query);
                        if ($update) {
                            echo "<span id='success'>Image has been uploaded successfully...!!</span>";
                        } else {
                            echo "<span id='invalid'>Image has been not uploaded successfully...!!</span>";
                        }
                    }
                }
            }
        }
    }
    
    public function doctor_patient_discussion_count(){
        if(isset($_GET['newmessage'])){
            $pid = $_GET['newmessage'];
            $query = "SELECT COUNT(*) FROM discussion 
                        INNER JOIN tbl_appointment 
                        ON discussion.appoint_id = tbl_appointment.appoint_id 
                        WHERE tbl_appointment.ptid = '$pid' AND discussion.doctor_status = 'unread'";
            $result = $this->db->select($query);
            $row = $result->fetch_row();
            $query1 = "SELECT COUNT(*) FROM tbl_appointment WHERE ptid = '$pid'";
            $result1 = $this->db->select($query1);
            $row1 = $result1->fetch_row();
            if($row[0] > 0){
                $notify = '<span class="check_notify">Check Message</span>';
            }else{
                $notify = '';
            }
            if($row1[0] > 0){
                echo '<th><a href="new-message">Doctor Appointed '.$notify.'</a></th>
                      <td><a href="new-message">'.$row1[0].'</a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>';
            }else{
                echo '<th>Doctor Appointed</th>
                  <td>0</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>';
            }
            
        }
    }  
    public function get_patient_unread_discussion(){
        if(isset($_GET['upmessage'])){
            $pid = $_GET['upmessage'];
            $query = "SELECT COUNT(*) FROM discussion 
                        INNER JOIN tbl_appointment 
                        ON discussion.appoint_id = tbl_appointment.appoint_id 
                        WHERE tbl_appointment.ptid = '$pid' AND discussion.doctor_status = 'unread'";
            $result = $this->db->select($query);
            $row = $result->fetch_row();
            if($row[0] > 0){
                echo $notify = '<span class="check_notify">Check Message</span>';
            }else{
                $notify = '';
            }     
        }  
    }
    public function get_patient_unread_discussion_details(){
        if(isset($_GET['upmessage_details'])){
            $pid = $_GET['upmessage_details'];
            $query = "SELECT COUNT(*) FROM discussion 
                        INNER JOIN tbl_appointment 
                        ON discussion.appoint_id = tbl_appointment.appoint_id 
                        WHERE tbl_appointment.ptid = '$pid' AND discussion.doctor_status = 'unread'";
            $result = $this->db->select($query);
            $row = $result->fetch_row();
            if($row[0] > 0){
                echo $notify = '<a id="goToBottom" href=""><span class="dcotor check_notify">Check Message</span></a>';
            }else{
                $notify = '';
            }     
        }  
    } 
    public function get_doctor_appointed_list($id = null, $pid = null){
        if(empty($id)){
            $query = "SELECT tbl_appointment.*, doctors.*
                      FROM tbl_appointment
                      INNER JOIN doctors
                      ON tbl_appointment.did = doctors.d_id WHERE ptid = '$pid' ORDER BY created_at ASC"; 
            $result = $this->db->select($query);
            return $result;
        }else{
            $query = "SELECT tbl_appointment.*, doctors.*
                      FROM tbl_appointment
                      INNER JOIN doctors
                      ON tbl_appointment.did = doctors.d_id WHERE appoint_approval_status = '1' AND appoint_id = '$id' ORDER BY created_at ASC"; 
            $result = $this->db->select($query);
            return $result;
        }
    }
    public function get_patient_presscribe_test(){
        if(isset($_GET['presscribe'])){
            $pid = $_GET['presscribe'];
            $query = "SELECT COUNT(*) FROM prescription 
                        INNER JOIN doctors 
                        ON prescription.did = doctors.d_id 
                        WHERE prescription.pid = '$pid'";
            $result = $this->db->select($query);
            $row = $result->fetch_row();
            if($row[0] > 0){
                echo '<th><a href="new-prescribe">Dcotor Prescribe / Test</a></th>
                      <td><a href="new-presscribe">'.$row[0].'</a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>';
            }else{
                echo '<th>Dcotor Prescribe / Test</th>
                  <td>0</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>';
            }
            
        } 
    }
    public function get_doctor_presscribe($id = null, $pid){
        if(empty($id)){
            $query = "SELECT prescription.*, doctors.*
                      FROM prescription
                      INNER JOIN doctors
                      ON prescription.did = doctors.d_id WHERE prescription.pid = '$pid' ORDER BY create_date ASC"; 
            $result = $this->db->select($query);
            return $result;
        }else{
            $query = "SELECT prescription.*, doctors.*
                      FROM prescription
                      INNER JOIN doctors
                      ON prescription.did = doctors.d_id WHERE prescription.id = '$id'"; 
            $result = $this->db->select($query);
            return $result;
        }   
    }
    public function get_new_message_from_doctor($id){
        $query1 = "UPDATE discussion SET 
                  doctor_status = 'read'
                  WHERE doctor_status = 'unread' AND  appoint_id = '$id'"; 
        $this->db->update($query1);
        $query = "SELECT * FROM discussion WHERE appoint_id = '$id' AND doctor_status = 'read' ORDER BY create_message ASC"; 
        $result = $this->db->select($query);
        return $result; 
    }
    // function for customer logout
    public function user_logout() {
        Session::user_destroy();
    }

}

?>
