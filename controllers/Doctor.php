<?php

//include 'lib/Session.php';
$path = realpath(dirname(__FILE__));
include_once($path . '/../lib/Main_controller.php');
Session::init();
/*
 * Admin Login class
 */

/**
 * 
 */
class Doctor extends Main_controller {

    public function __construct() {
        parent::__construct();
    }

    public function doctor_signup($post) {
        $data['first_name'] = $this->fm->sanitize($post['f_name']);
        $data['last_name'] = $this->fm->sanitize($post['l_name']);
        $data['idnetity_no'] = $this->fm->sanitize($post['id_no']);
        $data['nid_passport'] = $this->fm->sanitize($post['nid']);
        $data['email'] = $this->fm->sanitize($post['email']);
        $data['mobile_no'] = $this->fm->sanitize($post['mobile_no']);
        $data['gender'] = $this->fm->sanitize($post['gender']);
        $birthday = $this->fm->sanitize($post['doctbirth']);
        $data['birthday'] = $this->fm->getDate($birthday, 'Y-m-d');
        $password = $this->fm->sanitize($post['password']);
        $data['password'] = $this->fm->password_encrypt($password);
        $data['slug'] = $this->fm->url_slug($data['first_name'] . ' ' . $data['last_name']. ' ' .$data['idnetity_no']);
        if ($post['doctbtn']) {
            $email = $data['email'];
            $sql = "SELECT email FROM doctors WHERE email = '$email'";
            $ck_mail = $this->db->select($sql);
            if ($ck_mail != false) {
                echo "checkmail";
            } else {
                $insert = $this->insert_data_by_array('doctors', $data);
                if ($insert) {
                    $insertId = $this->db->link->insert_id;
                    echo "mail";
                    /* $to = $data['email'];
                      $from = 'azizul.softsw@gmail.com';
                      $subject = 'Activation Email';
                      // To send HTML mail, the Content-type header must be set
                      $headers  = 'MIME-Version: 1.0' . "\r\n";
                      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                      // Create email headers
                      $headers .= 'From: '.$from."\r\n".
                      'Reply-To: '.$from."\r\n" .
                      'X-Mailer: PHP/' . phpversion();

                      // Compose a simple HTML email message
                      $message = '<html><body>';
                      $message .= '<div class="mailbody"><h1 style="color:#f40;">Hi "'.$data['first_name'].'"!</h1>';
                      $message .= '<div style="color:#f40;"><strong>User ID:</strong> "'.$to.'"!</div>';
                      $message .= '<div style="color:#f40;"><strong>Password:</strong> "'.$password.'"!</div>';
                      $message .= '<div><p style="color:#080;font-size:18px;">Will you marry me?</p>';
                      $message .= '<p style="color:#080;font-size:18px;">To activation your account click the link below :</p>';
                      $message .= '<p style="color:#080;font-size:18px;">Activation Link :
                      <a href="'.'login.php?activated='.$insertId.'">Activated Account</a></p></div>';
                      $message .= '</div></body></html>';
                      $send_msg = mail( $to, $subject, $message, $headers );
                      if($send_msg){
                      echo "yes";
                      }else{
                      echo "nosend";
                      } */
                } else {
                    echo 'no';
                }
            }
        }
    }

    // customer check login
    public function doctor_check_login($post) {
        $email = $this->fm->sanitizeReal($this->db->link, $post['demail']);
        $password = $this->fm->sanitizeReal($this->db->link, $post['dpassword']);
        $password = $this->fm->password_encrypt($password);
        $query = "SELECT * FROM doctors WHERE (email = '$email' AND password = '$password') AND active_status = '0'";
        $result = $this->db->select($query);
        if ($result != false) { // Success
            $value = $result->fetch_assoc();
            $this->set_doctor_online($value['d_id']);
            Session::session_set('dLogin', true);
            Session::session_set('Fullname', $value['first_name'] . ' ' . $value['last_name']);
            Session::session_set('dID', $value['d_id']);
            //header("Location:doctor-penal");
            echo "dsuccesLogon";
        } else {
            echo "dunsuccessLogin";
        }
    }

    private function set_doctor_online($d_id) {
        $query = "UPDATE doctors SET
                online_status = '1'
                WHERE d_id = '$d_id'";
        $this->db->update($query);
    }

    public function unset_doctor_online($d_id) {
        $query = "UPDATE doctors SET
                online_status = '0'
                WHERE d_id = '$d_id'";
        $this->db->update($query);
    }

    public function update_doctor_info($post, $did) {
        $f_name = $this->fm->sanitize($post['first_name']);
        $l_name = $this->fm->sanitize($post['last_name']);
        $idnty_no = $this->fm->sanitize($post['idnetity_no']);
        $nid = $this->fm->sanitize($post['nid_passport']);
        $email = $this->fm->sanitize($post['email']);
        $mobile_no = $this->fm->sanitize($post['mobile_no']);
        $gender = $this->fm->sanitize($post['gender']);
        $dbirthday = $this->fm->sanitize($post['birthday']);
        $birthday = $this->fm->getDate($dbirthday, 'Y-m-d');
        $specialists = $this->fm->sanitize($post['specialists']);
        $pre_address = $this->fm->sanitize($post['present_address']);
        $per_address = $this->fm->sanitize($post['permanent_address']);
        $self_about = $this->fm->sanitize($post['self_about']);
        $slug = $this->fm->url_slug($f_name . ' ' . $f_name. ' ' .$idnty_no);
        $query = "UPDATE doctors SET
				first_name = '$f_name',
				last_name = '$l_name',
				idnetity_no = '$idnty_no',
				nid_passport = '$nid',
				email = '$email',
				mobile_no = '$mobile_no',
				gender = '$gender',
                birthday = '$birthday',
				specialists = '$specialists',
				present_address = '$pre_address',
                permanent_address = '$per_address',
				self_about = '$self_about',
                slug = '$slug'
				WHERE d_id = '$did'";
        $dupdate = $this->db->update($query);
        if ($dupdate) {
            echo "<script>alert('The information has been update Successfully');</script>";
        } else {
            echo "<script>alert('Not update');</script>";
        }
    }
    public function delete_doctor_photo($did){
        $d_id = $this->fm->sanitizeReal($this->db->link, $did);
        $sql = "SELECT profile_image FROM doctors WHERE d_id = '$did' LIMIT 1";
        $del_img = $this->db->select($sql);
        if( $del_img ){
            while( $row = $del_img->fetch_assoc()){
                $del_full = $row['profile_image'];
                unlink($del_full);
            }
        }
        $query = "UPDATE doctors SET
        profile_image = ''
        WHERE d_id = '$did'";
        $update = $this->db->update($query);
        if ($update) {
            echo "yes";
        } else {
            echo "no";
        }
    }
    public function save_doctor_education($post) {
        $data['d_id'] = $this->fm->sanitize($post['did']);
        $data['degree_title'] = $this->fm->sanitize($post['dg_title']);
        $data['degree_form'] = $this->fm->sanitize($post['dg_form']);
        $data['institute_name'] = $this->fm->sanitize($post['int_name']);
        $data['passing_year'] = $this->fm->sanitize($post['pass_year']);
        $data['major_subject'] = $this->fm->sanitize($post['major_sub']);
        $insert = $this->insert_data_by_array('deducations', $data);
        if ($insert) {
            echo 'yes';
        } else {
            echo 'no';
        }
    }

    public function get_deducation_ajax($post) {
        $did = $this->fm->sanitize($post['did']);
        $query = "SELECT * FROM deducations WHERE d_id = '$did'";
        $result = $this->db->select($query);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $output = '<table class="dedu_view">';
                $output.= '<tr><td><b>Degree Name</b></td><td>:</td><td>' . $row['degree_title'] . '</td></tr>';
                $output.= '<tr><td><b>Degree Short Form</b></td><td>:</td><td>' . $row['degree_form'] . '</td></tr>';
                $output.= '<tr><td><b>Institute Name</b></td><td>:</td><td>' . $row['institute_name'] . '</td></tr>';
                $output.= '<tr><td><b>Passing Year</b></td><td>:</td><td>' . $row['passing_year'] . '</td></tr>';
                $output.= '<tr><td><b>Major Subject</b></td><td>:</td><td>' . $row['major_subject'] . '</td></tr>';
                $output.= '<tr><td>
            <button href="#editdedu-' . $row['dedu_id'] . '" data-toggle="modal" class="btn-warning  text-center">Edit</button>
            <div class="modal fade education-modal" data-keyboard="false" data-backdrop="static" id="editdedu-' . $row['dedu_id'] . '" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <div id="succes_notify"></div>
                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                            <form id="deduupdate_form">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="form-group">
                                            <label for="degree_title">Degree Title <span class="required">*</span></label>
                                            <input type="hidden" id="' . $row['dedu_id'] . '" value="' . $row['dedu_id'] . '">
                                            <input type="text" class="form-control" id="degree_title-' . $row['dedu_id'] . '" value="' . $row['degree_title'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <label for="degree_form">Degree Short Form <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="degree_form-' . $row['dedu_id'] . '" value="' . $row['degree_form'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="institute_name">Institute Name <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="institute_name-' . $row['dedu_id'] . '" value="' . $row['institute_name'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="passing_year">Passing Year <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="passing_year-' . $row['dedu_id'] . '" value="' . $row['passing_year'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="major_subject">Major/Group <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="major_subject-' . $row['dedu_id'] . '" value="' . $row['major_subject'] . '">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr>
                                        <button type="submit" onclick="dupdateData(' . $row['dedu_id'] . ')" class="doct-submit button btn-primary">Update</button>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="deduDelete(' . $row['dedu_id'] . ')" class="btn-danger  text-center" type="submit">Delete</button>   
            </td></tr>';
                $output.= '</table>';
                echo $output;
            }
            echo '<a href="#educationmodal" data-toggle="modal" class="add-btn crt-acc">Add More</a>';
        } else {
            echo '<a href="#educationmodal" data-toggle="modal" class="add-btn crt-acc">Add</a>';
        }
    }

    public function update_doctor_education($post) {
        $dedu_id = $this->fm->sanitize($post['deduid']);
        $degree_title = $this->fm->sanitize($post['degree_title']);
        $degree_form = $this->fm->sanitize($post['degree_form']);
        $institute_name = $this->fm->sanitize($post['institute_name']);
        $passing_year = $this->fm->sanitize($post['passing_year']);
        $major_subject = $this->fm->sanitize($post['major_subject']);
        $qurey = "UPDATE deducations SET 
                    degree_title = '$degree_title', 
                    degree_form = '$degree_form', 
                    institute_name = '$institute_name', 
                    passing_year = '$passing_year', 
                    major_subject = '$major_subject' 
                    WHERE dedu_id = '$dedu_id'";

        $update = $this->db->update($qurey);
        if ($update) {
            echo 'yes';
        } else {
            echo 'no';
        }
    }

    public function delete_doctor_education($did) {
        $qurey = "DELETE FROM deducations WHERE dedu_id = '$did'";
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "yes";
        } else {
            echo "no";
        }
    }
    public function save_doctor_training($post) {
        $data['d_id'] = $this->fm->sanitize($post['did']);
        $data['training_title'] = $this->fm->sanitize($post['training_title']);
        $data['institute_name'] = $this->fm->sanitize($post['institute_name']);
        $data['address'] = $this->fm->sanitize($post['address']);
        $data['description'] = $this->fm->sanitize($post['description']);
        $insert = $this->insert_data_by_array('dtrainings', $data);
        if ($insert) {
            echo "yes";
        } else {
            echo "no";
        }
    }

    public function add_more_doctor_training($post) {
        $data['d_id'] = $this->fm->sanitize($post['training_did']);
        $data['training_title'] = $this->fm->sanitize($post['training_title']);
        $data['institute_name'] = $this->fm->sanitize($post['institute_name']);
        $data['address'] = $this->fm->sanitize($post['address']);
        $data['description'] = $this->fm->sanitize($post['description']);
        $insert = $this->insert_data_by_array('dtrainings', $data);
        if ($insert) {
            echo "<script>alert('The information has been inserted Successfully');</script>";
        } else {
            echo "<script>alert('Not inserted');</script>";
        }
    }

    public function get_dtraining_info($post) {
        $did = $this->fm->sanitize($post['did']);
        $query = "SELECT * FROM dtrainings WHERE d_id = '$did'";
        $result = $this->db->select($query);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $output = '<table class="dedu_view">';
                $output.= '<tr><td><b>Training Title</b></td><td>:</td><td>' . $row['training_title'] . '</td></tr>';
                $output.= '<tr><td><b>Institute Name</b></td><td>:</td><td>' . $row['institute_name'] . '</td></tr>';
                $output.= '<tr><td><b>Institute Address</b></td><td>:</td><td>' . $row['address'] . '</td></tr>';
                $output.= '<tr><td><b>Description (optional)</b></td><td>:</td><td>' . $row['description'] . '</td></tr>';
                $output.= '<tr><td>
            <button href="#editdedu-' . $row['dtr_id'] . '" data-toggle="modal" class="btn-warning  text-center">Edit</button>
            <div class="modal fade education-modal" data-keyboard="false" data-backdrop="static" id="editdedu-' . $row['dtr_id'] . '" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <div id="succes_notify"></div>
                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                            <form id="deduupdate_form">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="form-group">
                                            <label for="degree_title">Degree Title <span class="required">*</span></label>
                                            <input type="hidden" id="' . $row['dtr_id'] . '" value="' . $row['dtr_id'] . '">
                                            <input type="text" class="form-control" id="training_title-' . $row['dtr_id'] . '" value="' . $row['training_title'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <label for="degree_form">Institute Name <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="institute_name-' . $row['dtr_id'] . '" value="' . $row['institute_name'] . '">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="address">Institute Name <span class="required">*</span></label>
                                            <textarea class="form-control" id="address-' . $row['dtr_id'] . '">' . $row['address'] . '</textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="description">Description (optional)</label>
                                            <textarea class="form-control" id="description-' . $row['dtr_id'] . '">' . $row['description'] . '</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr>
                                        <button type="submit" onclick="dupdateTrainingData(' . $row['dtr_id'] . ')" class="doct-submit button btn-primary">Update</button>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="dtrainingDelete(' . $row['dtr_id'] . ')" class="btn-danger  text-center" type="submit">Delete</button>   
            </td></tr>';
                $output.= '</table>';
                echo $output;
            }
            echo '<a href="#dtrainingmodal" data-toggle="modal" class="add-btn crt-acc">Add More</a>';
        } else {
            echo '<a href="#dtrainingmodal" data-toggle="modal" class="add-btn crt-acc">Add</a>';
        }
    }

    public function get_dtraining_info_by_id($did) {
        $query = "SELECT * FROM dtrainings WHERE d_id = '$did'";
        $result = $this->db->select($query);
        return $result;
    }

    public function update_doctor_training($post) {

        $dtr_id = $this->fm->sanitize($post['dtr_id']);
        $training_title = $this->fm->sanitize($post['training_title']);
        $institute_name = $this->fm->sanitize($post['institute_name']);
        $address = $this->fm->sanitize($post['address']);
        $description = $this->fm->sanitize($post['description']);

        $qurey = "UPDATE dtrainings SET 
                    training_title = '$training_title', 
                    institute_name = '$institute_name', 
                    address = '$address', 
                    description = '$description' 
                    WHERE dtr_id = '$dtr_id'";
        $update = $this->db->update($qurey);
        if ($update) {
            echo 'yes';
        } else {
            echo 'no';
        }
    }

    public function delete_doctor_training($did) {
        $qurey = "DELETE FROM dtrainings WHERE dtr_id = '$did'";
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "yes";
        } else {
            echo "no>";
        }
    }

    public function save_doctor_job_history_ajax($post) {
        $data['d_id'] = $this->fm->sanitize($post['did']);
        $data['doc_office_name'] = $this->fm->sanitize($post['doc_office_name']);
        $start_date = $this->fm->sanitize($post['start_date']);
        $data['start_date'] = $this->fm->getDate($start_date, 'Y-m-d');
        $end_date = $this->fm->sanitize($post['end_date']);
        if ($end_date == 'Continuing') {
            $data['end_date'] = $end_date;
        } else {
            $data['end_date'] = $this->fm->getDate($end_date, 'Y-m-d');
        }
        $data['office_address'] = $this->fm->sanitize($post['office_address']);
        $insert = $this->insert_data_by_array('doctor_job_history', $data);
        if ($insert) {
            echo 'yes';
        } else {
            echo 'no';
        }
    }

    public function add_more_doctor_job($post) {
        $data['d_id'] = $this->fm->sanitize($post['addmorejob']);
        $data['doc_office_name'] = $this->fm->sanitize($post['doc_office_name']);
        $start_date = $this->fm->sanitize($post['start_date']);
        $data['start_date'] = $this->fm->getDate($start_date, 'Y-m-d');
        $end_date = $this->fm->sanitize($post['end_date']);
        if ($end_date == 'Continuing') {
            $data['end_date'] = $end_date;
        } else {
            $data['end_date'] = $this->fm->getDate($end_date, 'Y-m-d');
        }
        $data['office_address'] = $this->fm->sanitize($post['office_address']);
        $insert = $this->insert_data_by_array('doctor_job_history', $data);
        if ($insert) {
            echo "<script>alert('The information has been inserted Successfully');</script>";
        } else {
            echo "<script>alert('Not inserted');</script>";
        }
    }
    public function get_djobhistory_info_ajax($did) {
        $d_id = $this->fm->sanitizeReal($this->db->link, $did);
        $query = "SELECT * FROM doctor_job_history WHERE d_id = '$d_id'";
        $result = $this->db->select($query);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $checked = ($row['end_date'] == 'Continuing') ? 'checked' : '';

                $con_date = ($row['end_date'] == 'Continuing') ? '<tr><td><b>End Date</b></td><td>:</td><td>' . $row['start_date'] . '<span class="separate">To</span>' . $row['end_date'] . '</td></tr>' : '<tr><td><b>End Date</b></td><td>:</td><td>' . $row['start_date'] . '</td></tr><tr><td><b>End Date</b></td><td>:</td><td>' . $row['end_date'] . '</td></tr>';

                $output = '<table class="dedu_view">';
                $output.= '<tr><td><b>Office Name</b></td><td>:</td><td>' . $row['doc_office_name'] . '</td></tr>';
                $output.= $con_date;
                $output.= '<tr><td><b>Office Address</b></td><td>:</td><td>' . $row['office_address'] . '</td></tr>';
                $output.= '<tr><td>
            <button href="#editdedu-' . $row['doc_job_id'] . '" data-toggle="modal" class="btn-warning  text-center">Edit</button>
            <div class="modal fade education-modal" data-keyboard="false" data-backdrop="static" id="editdedu-' . $row['doc_job_id'] . '" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <div id="succes_notify"></div>
                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                            <script>
                                $(function () {
                                    if ("' . $row['end_date'] . '"=="Continuing"){
                                        $("#end_date-' . $row['doc_job_id'] . '").val("' . $row['end_date'] . '").attr("disabled", true);
                                    }else{
                                        $("#end_date-' . $row['doc_job_id'] . '").val("' . $this->fm->getDate($row['end_date'], 'd-m-Y') . '").removeAttr("disabled", true);
                                    }
                                    $(".continue-' . $row['doc_job_id'] . '").click(function () {
                                        if ($(this).is(":checked")) {
                                            $("#end_date-' . $row['doc_job_id'] . '").val("Continuing").attr("disabled", true);
                                        } else {
                                            $("#end_date-' . $row['doc_job_id'] . '").val("").removeAttr("disabled", true);
                                        }
                                    });
                                   $(".fromDate").datepicker({
                                      autoclose: true
                                    });
                                    $(".toDate").datepicker({
                                      autoclose: true
                                    });
                                    
                                });
                            </script>
                            <form>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="doc_office_name">Office Name <span class="required">*</span></label>
                                            <input type="hidden" id="' . $row['doc_job_id'] . '" value="' . $row['doc_job_id'] . '">
                                            <input type="text" class="form-control" id="doc_office_name-' . $row['doc_job_id'] . '" value="' . $row['doc_office_name'] . '" placeholder="Office Name">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="start_date">Job Period<span class="required">*</span></label>
                                            <input type="text" class="form-control fromDate start-date" id="start_date-' . $row['doc_job_id'] . '" value="' . $this->fm->getDate($row['start_date'], 'd-m-Y') . '">
                                        </div>
                                    </div>
                                       <div class="col-xs-6">
                                        <div class="form-group">
                                        <label for="end_date"></span></label>
                                            <input type="checkbox" ' . $checked . ' class="continue-' . $row['doc_job_id'] . '">&nbsp; Currently Working
                                            <div class="addDate">
                                            <input type="text" class="form-control toDate" id="end_date-' . $row['doc_job_id'] . '">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="office_address">Office Address (optional)</label>
                                            <textarea class="form-control" id="office_address-' . $row['doc_job_id'] . '" placeholder="Office Address">' . $row['office_address'] . '</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr>
                                        <button type="submit" onclick="dupdateJobhistoryData(' . $row['doc_job_id'] . ')" class="doct-submit button btn-primary">Update</button>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="djobhistoryDelete(' . $row['doc_job_id'] . ')" class="btn-danger  text-center" type="submit">Delete</button>   
            </td></tr>';
                $output.= '</table>';
                echo $output;
            }
            echo '<a href="#docjobhistorymodal" data-toggle="modal" class="add-btn crt-acc">Add More</a>';
        } else {
            echo '<a href="#docjobhistorymodal" data-toggle="modal" class="add-btn crt-acc">Add</a>';
        }
    }

    public function get_doctor_job_info_by_id($did) {
        $d_id = $this->fm->sanitizeReal($this->db->link, $did);
        $query = "SELECT * FROM doctor_job_history WHERE d_id = '$d_id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function update_doctor_job_history_ajax($post) {
        $doc_job_id = $this->fm->sanitizeReal($this->db->link, $post['doc_job_id']);
        $doc_office_name = $this->fm->sanitizeReal($this->db->link, $post['doc_office_name']);
        $st_date = $this->fm->sanitizeReal($this->db->link, $post['start_date']);
        $start_date = $this->fm->getDate($st_date, 'Y-m-d');
        $e_date = $this->fm->sanitizeReal($this->db->link, $post['end_date']);
        if ($e_date == 'Continuing') {
            $end_date = $e_date;
        } else {
            $end_date = $this->fm->getDate($e_date, 'Y-m-d');
        }
        $office_address = $this->fm->sanitizeReal($this->db->link, $post['office_address']);

        $qurey = "UPDATE doctor_job_history SET 
                    doc_office_name = '$doc_office_name', 
                    start_date = '$start_date', 
                    end_date = '$end_date', 
                    office_address = '$office_address'
                    WHERE doc_job_id = '$doc_job_id'";
        $update = $this->db->update($qurey);
        if ($update) {
            echo 'yes';
        } else {
            echo 'no';
        }
    }

    public function delete_doctor_job_history_ajax($did) {
        $doc_job_id = $this->fm->sanitizeReal($this->db->link, $did);
        $qurey = "DELETE FROM doctor_job_history WHERE doc_job_id = '$doc_job_id'";
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "yes";
        } else {
            echo "no";
        }
    }

    public function get_education_info_by_id($did) {
        $query = "SELECT * FROM deducations WHERE d_id = '$did'";
        $result = $this->db->select($query);
        return $result;
    }

    public function get_doctor_allinfo_by_id($did) {
        $expr = '/^[1-9][0-9]*$/';
        $d_id = $this->fm->sanitizeReal($this->db->link, $did);
        if (preg_match($expr, $did) && filter_var($did, FILTER_VALIDATE_INT)) {
            $query = "SELECT * FROM doctors WHERE d_id = '$d_id'";
        } else {
            $query = "SELECT * FROM doctors WHERE slug = '$d_id'";
        }
        $result = @$this->db->select($query)->fetch_assoc();
       if (preg_match($expr, $did) && filter_var($did, FILTER_VALIDATE_INT)) {
           $query = "SELECT * FROM doctors WHERE d_id = '$d_id'";  
        }else{
            $query = "SELECT * FROM doctors WHERE slug = '$d_id'";
        }
        $result = @$this->db->select($query)->fetch_assoc(); 
        return $result;
    }

    public function upload_docotr_profile_image($files, $did) {
        if (isset($files["file"]["type"])) {
            $validextensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $files["file"]["name"]);
            $sourcePath = $files['file']['tmp_name']; // Storing source path of the file in a variable
            $file_extension = strtolower(end($temporary));
            list($width, $height) = @getimagesize($sourcePath);

            if (empty($files["file"]["name"])) {
                echo "<span id='invalid'>Please select image</span>";
            } elseif ($width > "300" || $height > "300") {
                echo "<span id='invalid'>Error : image size must be 300 x 300 pixels.</span>";
            } else if ($files["file"]["size"] > 102400) {
                echo "<span id='invalid'>Image Size Should Be 100kb</span>";
            } elseif (in_array($file_extension, $validextensions) === FALSE) {
                echo "<span id='invalid'>You can upload only :-" . implode(', ', $validextensions) . "</span>";
            } else {
                if ($files["file"]["error"] > 0) {
                    echo "Return Code: " . $files["file"]["error"] . "<br/><br/>";
                } else {
                    if (file_exists("uploads/" . $files["file"]["name"])) {
                        echo "<span id='invalid'><span>" . $files["file"]["name"] . "</span><b>already exists.</b></span></span>";
                    } else {
                        $targetPath = "uploads/" . $files['file']['name']; // Target path where file is to be stored
                        move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                        $query = "UPDATE doctors SET
                        profile_image = '$targetPath'
                        WHERE d_id = '$did'";
                        $update = $this->db->insert($query);
                        if ($update) {
                            echo "<span id='success'>Image has been uploaded successfully...!!</span>";
                        } else {
                            echo "<span id='invalid'>Image has been not uploaded successfully...!!</span>";
                        }
                    }
                }
            }
        }
    }

    public function profile_stap_complete($did) {
        $dprofile = $this->get_doctor_allinfo_by_id($did);
        $dedresult = $this->get_education_info_by_id($did);
        $dtrresult = $this->get_dtraining_info_by_id($did);
        $djobresult = $this->get_doctor_job_info_by_id($did);
        echo '<ul>';
        if (empty($dprofile['present_address']) || empty($dprofile['permanent_address']) || empty($dprofile['specialists'])) {
            echo '<li><a href="index.php"><span>✖</span>Personal Info.</a></li>';
        } else {
            echo '<li><a href="index.php"><span>✔</span>Personal Info.</a></li>';
        }
        if ($dedresult) {
            echo '<li><a href="deducation"><span>✔</span>Add Qualification</a></li>';
        } else {
            echo '<li><a href="deducation"><span>✖</span>Add Qualification</a></li>';
        }
        if ($dtrresult) {
            echo '<li><a href="dtraining"><span>✔</span>Add Training</a></li>';
        } else {
            echo '<li><a href="dtraining"><span>✖</span>Add Training</a></li>';
        }
        if ($djobresult) {
            echo '<li><a href="djob-history"><span>✔</span> Add Job History</a></li>';
        } else {
            echo '<li><a href="djob-history"><span>✖</span> Add Job History</a></li>';
        }
        if (empty($dprofile['profile_image'])) {
            echo '<li><a href="profile-image"><span>✖</span>Add Profile Picture</a></li>';
        } else {
            echo '<li><a href="profile-image"><span>✔</span>Add Profile Picture</a></li>';
        }
        echo '</ul>';
        if (!empty($dprofile['specialists']) && !empty($dprofile['profile_image']) && !empty($dprofile['present_address']) && !empty($dprofile['permanent_address']) &&
                ($dedresult->num_rows > 0) && ($dtrresult->num_rows > 0) && ($djobresult->num_rows > 0)) {
            echo '<script type="text/javascript">';
            echo 'window.location.href="'.$this->fm->base_url().'doctor-panel/"';
            echo '</script>';
        }
    }

    // return all approved doctor 
    public function get_all_approved_doctory($limit) {
        $query = "SELECT * FROM doctors  WHERE approval_status = '1' ORDER BY first_name $limit";
        $result = $this->db->select($query);
        return $result;
    }
    public function get_all_approved_specialist_doctor(){
        $query = "SELECT DISTINCT specialists FROM doctors  WHERE approval_status = '1' ORDER BY specialists";
        $result = $this->db->select($query);
        return $result;
    }
    public function get_all_approved_doctor_list(){
        $query = "SELECT COUNT(d_id) FROM doctors  WHERE approval_status = '1'";
        $result = $this->db->select($query); 
        return $result;
    }
    public function get_relative_doctor($did){
        $expr = '/^[1-9][0-9]*$/';
        $d_id = $this->fm->sanitizeReal($this->db->link, $did);
        if (preg_match($expr, $did) && filter_var($did, FILTER_VALIDATE_INT)) {
            $query = "SELECT * FROM doctors WHERE d_id = '$d_id'";
        } else {
            $query = "SELECT * FROM doctors WHERE slug = '$d_id'";
        }
        $result = @$this->db->select($query)->fetch_assoc();
        $specialists = $result['specialists'];
        $query = "SELECT * FROM doctors  WHERE approval_status = '1' AND specialists = '$specialists' ORDER BY first_name";
        $result = $this->db->select($query);
        $res = array();
        while ($row = @$result->fetch_assoc()) {
            $res[] = $row;
        }
        return $res;

    }
    public function check_doctor_online() {
        $res = $this->get_all_approved_doctory(0, 12);
        echo '<div class="area">';
        foreach ($res as $drow):
            $url = "doctor-details?d=" . $drow['slug'];
            $profle_image = (!empty($drow['profile_image'])) ? '<img src="' . $drow['profile_image'] . '" class="img-responsive" alt="' . $drow['first_name'] . '">' : '<img src="images/avatar-default.png" class="img-responsive" alt="avatar default">';
            $online_status = ($drow['online_status'] == 1) ? '<span class="online"></span>' : '<span class="offline"></span>';
            echo'<div class="col-xs-12 col-sm-6 col-md-3"><div class="doc-sty-box">';
            echo'<a href="doctor-details?d=' . $drow['slug'] . '">
                        '.$profle_image.'
                    </a>';
            echo'<div class="doc-sty-inner">
        <input type="hidden" class="d_id" value="' . $drow['d_id'] . '">
        <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
        </ul>
        <h5>
        <div id="online_status">' . $online_status . '</div>
        <a href="' . $url . '">Dr. ' . $drow['first_name'] . ' ' . $drow['last_name'] . '</a></h5>
        <span>' . $drow['specialists'] . '</span></div></div></div>';
        endforeach;
        echo '</div>';
    }

    public function doctor_online_offline_status($post) {
        $drow = $this->get_doctor_allinfo_by_id($post['did']);
        $online_status = ($drow['online_status'] == 1) ? '<span class="online"></span>' : '<span class="offline"></span>';
        echo $online_status;
    }

    // function for customer logout
    public function user_logout() {
        Session::user_destroy();
    }

    public function doctor_personal_details($did) {
        $qurey = "SELECT * FROM doctors WHERE d_id = '$did'";
        $result = $this->db->select($qurey)->fetch_assoc();
        return $result;
    }

    public function doctor_education_info($did) {
        $qurey = "SELECT * FROM deducations WHERE d_id = '$did'";
        $result = $this->db->select($qurey);
        return $result;
    }

    public function doctor_training_info($did) {
        $qurey = "SELECT * FROM dtrainings WHERE d_id = '$did'";
        $result = $this->db->select($qurey);
        return $result;
    }

    public function doctor_job_history($did) {
        $qurey = "SELECT * FROM doctor_job_history WHERE d_id = '$did'";
        $result = $this->db->select($qurey);
        return $result;
    }

    public function update_doctor_education_by_id($post, $dedu_id) {
        $dedu_id = $this->fm->sanitizeReal($this->db->link, $dedu_id);
        $degree_title = $this->fm->sanitizeReal($this->db->link, $post['degree_title']);
        $degree_form = $this->fm->sanitizeReal($this->db->link, $post['degree_form']);
        $institute_name = $this->fm->sanitizeReal($this->db->link, $post['institute_name']);
        $passing_year = $this->fm->sanitizeReal($this->db->link, $post['passing_year']);
        $major_subject = $this->fm->sanitizeReal($this->db->link, $post['major_subject']);
        $qurey = "UPDATE deducations SET 
                    degree_title = '$degree_title', 
                    degree_form = '$degree_form', 
                    institute_name = '$institute_name', 
                    passing_year = '$passing_year', 
                    major_subject = '$major_subject' 
                    WHERE dedu_id = '$dedu_id'";

        $dupdate = $this->db->update($qurey);
        if ($dupdate) {
            echo "<script>alert('The information has been update Successfully');</script>";
        } else {
            echo "<script>alert('Not update');</script>";
        }
    }

    public function update_doctor_training_by_id($post, $dtr_id) {
        $dtr_id = $this->fm->sanitizeReal($this->db->link, $dtr_id);
        $training_title = $this->fm->sanitizeReal($this->db->link, $post['training_title']);
        $institute_name = $this->fm->sanitizeReal($this->db->link, $post['institute_name']);
        $address = $this->fm->sanitizeReal($this->db->link, $post['address']);
        $description = $this->fm->sanitizeReal($this->db->link, $post['description']);

        $qurey = "UPDATE dtrainings SET 
                    training_title = '$training_title', 
                    institute_name = '$institute_name', 
                    address = '$address', 
                    description = '$description' 
                    WHERE dtr_id = '$dtr_id'";
        $update = $this->db->update($qurey);
        if ($update) {
            echo "<script>alert('The information has been update Successfully');</script>";
        } else {
            echo "<script>alert('Not update');</script>";
        }
    }

    public function update_doctor_job_history_by_id($post, $doc_job_id) {
        $doc_office_name = $this->fm->sanitizeReal($this->db->link, $post['doc_office_name']);
        $st_date = $this->fm->sanitizeReal($this->db->link, $post['start_date']);
        $start_date = $this->fm->getDate($st_date, 'Y-m-d');
        $e_date = $this->fm->sanitizeReal($this->db->link, $post['end_date']);
        if ($e_date == 'Continuing') {
            $end_date = $e_date;
        } else {
            $end_date = $this->fm->getDate($e_date, 'Y-m-d');
        }
        $office_address = $this->fm->sanitizeReal($this->db->link, $post['office_address']);
        $qurey = "UPDATE doctor_job_history SET 
                    doc_office_name = '$doc_office_name', 
                    start_date = '$start_date', 
                    end_date = '$end_date', 
                    office_address = '$office_address'
                    WHERE doc_job_id = '$doc_job_id'";
        $update = $this->db->update($qurey);
        if ($update) {
            echo "<script>alert('The information has been update Successfully');</script>";
        } else {
            echo "<script>alert('Not update');</script>";
        }
    }

    public function get_all_search_doctor($str){
        $search = $this->fm->sanitizeReal($this->db->link, $str);
        $query = "SELECT * FROM doctors  WHERE approval_status = '1' AND specialists = '$search' ORDER BY first_name";
        $result = $this->db->select($query);
        $res = array();
        while ($row = @$result->fetch_assoc()) {
            $res[] = $row;
        }
        return $res;
    }

    public function latest_appointment_count(){
        if(isset($_GET['latestapp'])){
            $did = $_GET['latestapp'];
            $query1 = "SELECT COUNT(*) FROM discussion 
                        INNER JOIN tbl_appointment 
                        ON discussion.appoint_id = tbl_appointment.appoint_id 
                        WHERE tbl_appointment.did = '$did' AND discussion.patient_status = 'unread'";
            $result1 = $this->db->select($query1);
            $row1 = $result1->fetch_row();            
            $query = "SELECT COUNT(*) FROM tbl_appointment WHERE did = '$did' AND appoint_approval_status = '1' AND DATE(`created_at`) = CURDATE()";
            $result = $this->db->select($query);
            $row = $result->fetch_row();
            if($row1[0] > 0){
                $notify = '<span class="check_notify">Check Message</span>';
            }else{
                $notify = '';
            }
            if($row[0] > 0){
                echo '<th><a href="latest-appointment">New Appontment '.$notify.'</a></th>
                      <td><a href="latest-appointment">'.$row[0].'</a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>';
            }else{
                echo '<th>New Appontment</th>
                  <td>0</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>';
            }
            
        }
    }
    public function get_doctor_unread_discussion(){
        if(isset($_GET['dupmessage'])){
            $did = $_GET['dupmessage'];
            $query1 = "SELECT COUNT(*) FROM discussion 
                        INNER JOIN tbl_appointment 
                        ON discussion.appoint_id = tbl_appointment.appoint_id 
                        WHERE tbl_appointment.did = '$did' AND discussion.patient_status = 'unread'";
            $result1 = $this->db->select($query1);
            $row1    = $result1->fetch_row();     
            if($row1[0] > 0){
                echo $notify = '<a id="goToBottom" href=""><span class="dcotor check_notify">Check Message</span></a>';
            }else{
                $notify = '';
            }     
        }  
    } 
    public function latest_appointment_list($id = null, $did = null){
        if(empty($id)){
            $query = "SELECT tbl_appointment.*, patients.*
                      FROM tbl_appointment
                      INNER JOIN patients
                      ON tbl_appointment.ptid = patients.pt_id WHERE did = '$did' AND appoint_approval_status = '1' AND DATE(`created_at`) = CURDATE() ORDER BY created_at ASC"; 
            $result = $this->db->select($query);
            return $result;
        }else{
            $query1 = "UPDATE tbl_appointment SET 
                      patient_status = 'read'
                      WHERE patient_status = 'unread' AND  appoint_id = '$id'"; 
            $this->db->update($query1);
            
           $query = "SELECT tbl_appointment.*, patients.*
                      FROM tbl_appointment
                      INNER JOIN patients
                      ON tbl_appointment.ptid = patients.pt_id WHERE appoint_approval_status = '1' AND DATE(`created_at`) = CURDATE()  AND appoint_id = '$id' ORDER BY created_at ASC"; 
            $result = $this->db->select($query);
            return $result;
        }
    }
    public function latest_appointment_list_ajax($did){
        $query = "SELECT tbl_appointment.*, patients.*
                  FROM tbl_appointment
                  INNER JOIN patients
                  ON tbl_appointment.ptid = patients.pt_id WHERE tbl_appointment.did = '$did' AND tbl_appointment.appoint_approval_status = '1' AND DATE(tbl_appointment.created_at) = CURDATE() ORDER BY created_at ASC"; 
        $result = $this->db->select($query);

        echo '<table class="table table-inside">
            <thead>
                <tr>
                    <th>SL NO.</th>
                    <th>Patient Name</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>';
        if($result):
            $i = 0;
        while ( $arow = $result->fetch_assoc()) {  
            $i++;
            if($arow['patient_status'] == 'unread'){
                $notify = '<span class="check_notify">Check Message</span>';
            }else{
                $notify = '';
            } 
         echo '<tr>
            <td>'.$notify.$i.'</td>
            <td>'.$arow['first_name'] .' '.$arow['last_name'].'</td>
            <td>'.$this->fm->getDate($arow['created_at'], 'jS M Y,  g:i:s').'</td>
            <td>
                <a href="appointment-details?ap-id='.$arow['appoint_id'].'">View Details</a>
            </td>
        </tr>';
        } else:
        endif; 
        echo '</tbody>
    </table>';  
    }
    public function get_new_message_from_patient($id){
        $query1 = "UPDATE discussion SET 
                  patient_status = 'read'
                  WHERE patient_status = 'unread' AND  appoint_id = '$id'"; 
        $this->db->update($query1);
        $query = "SELECT * FROM discussion WHERE appoint_id = '$id' AND patient_status = 'read' ORDER BY create_message ASC"; 
        $result = $this->db->select($query);
        return $result; 
    }
    public function send_prescribe_doctor_to_patient($post){
            $data['appoint_id'] = $this->fm->sanitize($post['appoint_id']);
            $data['did'] = $this->fm->sanitize($post['did']);
            $data['pid'] = $this->fm->sanitize($post['pid']);
            $data['deseise'] = $this->fm->sanitize($post['deseise']);
            $data['medicine'] = $this->fm->sanitize($post['medicine']);
            $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $data['create_date'] = $ex_dt->format('Y-m-d H:i:s');
            $insert = $this->insert_data_by_array('prescription', $data);
            if ($insert) {
                echo "<script>alert('The prescription has been sent Successfully');</script>";
            } else {
                echo "<script>alert('Not sent');</script>";
            }
    }
}

?>