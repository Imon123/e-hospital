<?php 
$path = realpath(dirname(__FILE__));
include_once($path . '/../lib/Main_controller.php');
Session::init();
/*
 * Admin Login class
 */

/**
 * 
 */
class Clinic extends Main_controller {

    public function __construct() {
        parent::__construct();
    }

    public function clinic_signup($post) {
        $data['clinic_name'] = $this->fm->sanitize($post['c_name']);
        $data['clinic_location'] = $this->fm->sanitize($post['c_location']);
        $data['ownertype'] = $this->fm->sanitize($post['ownertype']);
        $data['ownername'] = $this->fm->sanitize($post['ownername']);
        $data['clinictype'] = $this->fm->sanitize($post['c_type']);
        $data['licnumber'] = $this->fm->sanitize($post['licnumber']);
        $data['email'] = $this->fm->sanitize($post['cemail']);
        $data['mobile_no'] = $this->fm->sanitize($post['cmobile_no']);
        $data['startdate'] = $this->fm->sanitize($post['startdate']);
        $data['clinicaddress'] = $this->fm->sanitize($post['c_address']);
        $passwordc = $this->fm->sanitize($post['passwordc']);
        $data['password'] = $this->fm->password_encrypt($passwordc);
        if ($post['clibtn']) {
            $cemail = $data['email'];
            $sql = "SELECT email FROM clinics WHERE email = '$cemail'";
            $ck_mail = $this->db->select($sql);
            if ($ck_mail != false) {
                echo "checkmail";
            } else {
                $insert = $this->insert_data_by_array('clinics', $data);
                if ($insert) {
                    $insertId = $this->db->link->insert_id;
                    echo "mail";
                    /* $to = $data['email'];
                      $from = 'azizul.softsw@gmail.com';
                      $subject = 'Activation Email';
                      // To send HTML mail, the Content-type header must be set
                      $headers  = 'MIME-Version: 1.0' . "\r\n";
                      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                      // Create email headers
                      $headers .= 'From: '.$from."\r\n".
                      'Reply-To: '.$from."\r\n" .
                      'X-Mailer: PHP/' . phpversion();

                      // Compose a simple HTML email message
                      $message = '<html><body>';
                      $message .= '<div class="mailbody"><h1 style="color:#f40;">Hi "'.$data['first_name'].'"!</h1>';
                      $message .= '<div style="color:#f40;"><strong>User ID:</strong> "'.$to.'"!</div>';
                      $message .= '<div style="color:#f40;"><strong>Password:</strong> "'.$password.'"!</div>';
                      $message .= '<div><p style="color:#080;font-size:18px;">Will you marry me?</p>';
                      $message .= '<p style="color:#080;font-size:18px;">To activation your account click the link below :</p>';
                      $message .= '<p style="color:#080;font-size:18px;">Activation Link :
                      <a href="'.'login.php?activated='.$insertId.'">Activated Account</a></p></div>';
                      $message .= '</div></body></html>';
                      $send_msg = mail( $to, $subject, $message, $headers );
                      if($send_msg){
                      echo "yes";
                      }else{
                      echo "nosend";
                      } */
                } else {
                    echo 'no';
                }
            }
        }
    }

    // clinic check login
    public function clinic_check_login($post) {
        $cemail = $this->fm->sanitizeReal($this->db->link, $post['cemail']);
        $password = $this->fm->sanitizeReal($this->db->link, $post['cpassword']);
        $password = $this->fm->password_encrypt($password);
        $query = "SELECT * FROM clinics WHERE email = '$cemail' AND password = '$password'";
        $result = $this->db->select($query);
        if ($result != false) { 
            $value = $result->fetch_assoc();
            Session::session_set('cLogin', true);
            Session::session_set('Clinicname', $value['clinic_name']);
            Session::session_set('cID', $value['c_id']);
            echo "csuccesLogon";
        } else {
            echo "unsuccessLogin";
        }
    }

    public function get_clinic_profile_by_id($cid) {
        $c_id = $this->fm->sanitizeReal($this->db->link, $cid);
        $query = "SELECT * FROM clinics WHERE c_id = '$c_id'";
        $result = $this->db->select($query)->fetch_assoc();
        return $result;
    }

    //update clinic profile
    public function update_clinic_profile_by_id($post){
        $c_id = $this->fm->sanitizeReal($this->db->link, $post['c_id']);
        $c_name = $this->fm->sanitizeReal($this->db->link, $post['clinic_name']);
        $c_location = $this->fm->sanitizeReal($this->db->link, $post['clinic_location']);
        $ownertype = $this->fm->sanitizeReal($this->db->link, $post['ownertype']);
        $ownername = $this->fm->sanitizeReal($this->db->link, $post['ownername']);
        $c_type = $this->fm->sanitizeReal($this->db->link, $post['clinictype']);
        $licnumber = $this->fm->sanitizeReal($this->db->link, $post['licnumber']);
        $email = $this->fm->sanitizeReal($this->db->link, $post['cemail']);
        $mobile_no= $this->fm->sanitizeReal($this->db->link, $post['mobile_no']);
        $startdate = $this->fm->sanitizeReal($this->db->link, $post['startdate']);
        $clinicaddress = $this->fm->sanitizeReal($this->db->link, $post['clinicaddress']);
        $query = "UPDATE clinics
            SET
            clinic_name = '$c_name',
            clinic_location = '$c_location',
            ownertype = '$ownertype',
            ownername = '$ownername',
            clinictype = '$c_type',
            licnumber = '$licnumber',
            email = '$email',
            mobile_no = '$mobile_no',
            startdate = '$startdate',
            clinicaddress = '$clinicaddress'
            WHERE c_id = '$c_id'";
         $cupdate = $this->db->update($query);
        if($cupdate){
         echo "<script>alert('The information has been update Successfully');</script>";
        }else{
         echo "<script>alert('Not update');</script>";
        }
    }


    





}


?>