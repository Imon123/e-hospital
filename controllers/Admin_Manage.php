<?php

$path = realpath(dirname(__FILE__));
include_once($path . '/../lib/Main_Controller.php');
/*
 * Student Info class
 */

class Admin_Manage extends Main_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function save_dep_cat($post, $file) {
        $dep_title = $this->fm->sanitize($post['dep_title']);
        $dep_description = $this->fm->sanitize($post['dep_description']);
        $image = $this->profile_image_created($file);

        $error = '';
        if (empty($dep_title)) {
            $error = true;
            $this->msg['dep_title'] = "Title is required!";
        }
        if (empty($dep_description)) {
            $error = true;
            $this->msg['dep_description'] = "Description is required!";
        }

        if (!$error) {
            $upload_file = "../uploads/" . $image[1];
            move_uploaded_file($image[0], $upload_file);
            $query = "INSERT INTO department (dep_title, dep_description, image) 
    				  VALUES (' $dep_title', '$dep_description', '$upload_file')";
            $insertdep = $this->db->insert($query);
            if ($insertdep) {
                $this->msg['success'] = "Admitted Successfully!";
            } else {
                $this->msg['error'] = "Error in Department...Please try again later!!";
            }
        }
        return $this->msg;
    }

    private function profile_image_created($_file) {
        $permited_type = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_file['image']['name'];
        $file_size = $_file['image']['size'];
        $file_temp = $_file['image']['tmp_name'];

        $file_divided = explode('.', $file_name);
        $file_ex = strtolower(end($file_divided));
        $unique_file = substr(md5(time()), 0, 20) . ".$file_ex";

        list($width, $height) = @getimagesize($file_temp);

        if (empty($file_name)) {
            $this->msg['error'] = "Image required !!";
            return $this->msg;
        } elseif ($width > "150" || $height > "150") {
            $this->msg['error'] = "Error : image size must be 150 x 150 pixels.";
            return $this->msg;
        } elseif (in_array($file_ex, $permited_type) === FALSE) {
            $this->msg['error'] = "You can upload only :-" . implode(', ', $permited_type);
            return $this->msg;
        } else {
            return array($file_temp, $unique_file);
        }
    }

    public function show_dep_cat() {
        $query = "SELECT *FROM department";
        $result = $this->db->select($query);
        return $result;
    }

    public function doctor_list() {
        $qurey = "SELECT * FROM doctors";
        $result = $this->db->select($qurey);
        return $result;
    }

    public function delete_doctor_all_info($did) {
        $qurey = "DELETE doctors FROM doctors 
                LEFT JOIN deducations ON doctors.d_id = deducations.d_id 
                LEFT JOIN dtrainings ON deducations.d_id = dtrainings.d_id
                LEFT JOIN doctor_job_history ON dtrainings.d_id = doctor_job_history.d_id
                WHERE doctors.d_id = '$did'";
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "<script>alert('The information has been deleted Successfully');</script>";
        } else {
            echo "<script>alert('Not deleted');</script>";
        }
    }

    public function approve_this_doctor($did) {
        //$self_about = $this->fm->sanitize($post['self_about']);
        $query = "UPDATE doctors SET approval_status = 1 WHERE d_id = '$did'";
        $dupdate = $this->db->update($query);
        if ($dupdate) {
            echo "<script>alert('The doctor is approved successfully');</script>";
        } else {
            echo "<script>alert('Not approved');</script>";
        }
    }

    public function unapprove_this_doctor($did) {
        //$self_about = $this->fm->sanitize($post['self_about']);
        $query = "UPDATE doctors SET approval_status = 0 WHERE d_id = '$did'";
        $dupdate = $this->db->update($query);
        if ($dupdate) {
            echo "<script>alert('The doctor is unapproved successfully');</script>";
        } else {
            echo "<script>alert('Not unapproved');</script>";
        }
    }

    public function patient_list() {
        $qurey = "SELECT * FROM patients ORDER BY create_date DESC";
        $result = $this->db->select($qurey);
        return $result;
    }

    public function get_patient_profile_by_id($pid) {
        $query = "SELECT * FROM patients WHERE pt_id = '$pid'";
        $result = $this->db->select($query)->fetch_assoc();
        return $result;
    }

    public function delete_patient($pid) {
        $qurey = "DELETE FROM `patients` WHERE pt_id = '$pid'" or die("Not Deleted");
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "<script>alert('The information has been deleted Successfully');</script>";
        } else {
            echo "<script>alert('Not deleted');</script>";
        }
    }

    public function notification_count() {
        $query = "SELECT * FROM patients WHERE read_status = 'unread'" or die("Failed to count");
        $result = $this->db->select($query);
        $count = @mysqli_num_rows($result);
        return $count;
    }

    public function notifications() {
        $query = "SELECT * FROM patients WHERE read_status = 'unread'" or die("Failed to select");
        $result = $this->db->select($query);
        return $result;
    }

    public function read_notification($pt_id) {
        $query = "UPDATE patients SET read_status = 'read' WHERE pt_id = '$pt_id'" or die("Faild to read notification");
        $updateInfo = $this->db->update($query);
        return $updateInfo;
    }

    //Riyad
    public function all_doctors_apporve_show() {
        $query = "SELECT *FROM doctors WHERE approval_status = 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function all_patient_show_num() {
        $query = "SELECT *FROM patients";
        $result = $this->db->select($query);
        return $result;
    }

    public function all_clinic_show_num() {
        $query = "SELECT *FROM clinics";
        $result = $this->db->select($query);
        return $result;
    }
	/*public function show_dep_cat(){
		$query = "SELECT *FROM department";
		$result = $this->db->select($query);
		return $result;
    }*/
//Riyad End

    public function appointment_list() {
        $query = "SELECT tbl_appointment.*, patients.pt_id, patients.first_name AS pt_fname, patients.last_name AS pt_lname, patients.profile_img, doctors.d_id, doctors.first_name, doctors.last_name, doctors.specialists FROM tbl_appointment
					INNER JOIN doctors ON tbl_appointment.did = doctors.d_id 
					INNER JOIN patients ON tbl_appointment.ptid = patients.pt_id
					WHERE appoint_approval_status = 0
					ORDER BY created_at ASC";
        $result = $this->db->select($query);
        return $result;
    }

    public function approved_appointment_list() {
        $query = "SELECT tbl_appointment.*, patients.pt_id, patients.first_name AS pt_fname, patients.last_name AS pt_lname, patients.profile_img, doctors.d_id, doctors.first_name, doctors.last_name, doctors.specialists FROM tbl_appointment
					INNER JOIN doctors ON tbl_appointment.did = doctors.d_id 
					INNER JOIN patients ON tbl_appointment.ptid = patients.pt_id
					WHERE appoint_approval_status = 1
					ORDER BY created_at ASC";
        $result = $this->db->select($query);
        return $result;
    }

    public function delete_appointment($apt_id) {
        $qurey = "DELETE FROM `tbl_appointment` WHERE appoint_id = '$apt_id'" or die("Not Deleted");
        $delete = $this->db->delete($qurey);
        if ($delete) {
            echo "<script>alert('The information has been deleted Successfully');</script>";
        } else {
            echo "<script>alert('Not deleted');</script>";
        }
    }

    public function approve_this_appointment($apt_id) {
        $query = "UPDATE tbl_appointment SET appoint_approval_status = 1 WHERE appoint_id = '$apt_id'";
        $dupdate = $this->db->update($query);
        if ($dupdate) {
            echo "<script>alert('The appointment is approved successfully');</script>";
        } else {
            echo "<script>alert('Not approved');</script>";
        }
    }

    public function unapprove_this_appointment($apt_id) {
        $query = "UPDATE tbl_appointment SET appoint_approval_status = 0 WHERE appoint_id = '$apt_id'";
        $dupdate = $this->db->update($query);
        if ($dupdate) {
            echo "<script>alert('The appointment is unapproved successfully');</script>";
        } else {
            echo "<script>alert('Not unapproved');</script>";
        }
    }

    public function patient_transaction_history($pt_id) {
        $query = "SELECT * FROM payment_history
				  INNER JOIN patients ON payment_history.pt_id = patients.pt_id
				  WHERE payment_history.pt_id = '$pt_id'";
        $result = @$this->db->select($query)->fetch_assoc();
        return $result;
    }    
    public function patient_payment_list() {
        $query = "SELECT * FROM payment_history INNER JOIN patients ON payment_history.pt_id = patients.pt_id";
        $result = $this->db->select($query);
        return $result;
    }

    public function payment_verify($payment_id, $post) {
        
        $pt_id = $this->fm->sanitizeReal($this->db->link, $post['pt_id']);
        $expire_date = $this->fm->sanitizeReal($this->db->link, $post['expire_date']);
        $expire_start = date('Y-m-d H:i:s');
        $query = "UPDATE payment_history
                 SET
                 payment_approve_status = 1 WHERE id = '$payment_id'";
        $query1 = "UPDATE patients 
                 SET 
                 expire_start = '$expire_start', 
                 expire_date = '$expire_date' WHERE pt_id = '$pt_id'";
        $dupdate = $this->db->update($query);
        $pdupdate = $this->db->update($query1);
        if ($dupdate && $pdupdate) {
            echo "<script>alert('The payment is approved successfully');</script>";
        } else {
            echo "<script>alert('Not approved');</script>";
        }
    }
	public function paid_patients() {
        $query = "SELECT *FROM payment_history";
        $result = $this->db->select($query);
        return $result;
    }
	public function appointment_history($patient_id){
		$query = "SELECT tbl_appointment.*, patients.pt_id, patients.first_name AS pt_fname, patients.last_name AS pt_lname, patients.profile_img, doctors.d_id, doctors.first_name, doctors.last_name, doctors.specialists FROM tbl_appointment
					INNER JOIN doctors ON tbl_appointment.did = doctors.d_id 
					INNER JOIN patients ON tbl_appointment.ptid = patients.pt_id
					WHERE ptid = '$patient_id'
					ORDER BY created_at ASC";
		$result = $this->db->select($query);
        return $result;
	}	public function appointment_history_by_appoint_id($patient_id){
		$query = "SELECT tbl_appointment.*, patients.pt_id, patients.first_name AS pt_fname, patients.last_name AS pt_lname, patients.profile_img, doctors.d_id, doctors.first_name, doctors.last_name, doctors.specialists FROM tbl_appointment
					INNER JOIN doctors ON tbl_appointment.did = doctors.d_id 
					INNER JOIN patients ON tbl_appointment.ptid = patients.pt_id
					WHERE appoint_id = '$patient_id'
					ORDER BY created_at ASC";
		$result = $this->db->select($query);
        return $result;
	}
	public function inactive_doctors(){
		$query = "SELECT * FROM doctors WHERE approval_status = 0";
		$result = $this->db->select($query);
        return $result;
	}

}

?>