<?php
include '../header.php';
include 'psession.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['dquery'])) {
    $patient->conversation_doctor_to_patient($_FILES, $_POST);
}
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                    <div id="pup_message"></div>
                    <div class="row">
                     <?php include 'membership-message.php'; ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php 
                            if(isset($_GET['ap-id']) && $_GET['ap-id'] != null){
                                $result = $patient->get_doctor_appointed_list($_GET['ap-id'], $v = null);
                                $mresult = $patient->get_new_message_from_doctor($_GET['ap-id']);
                            }
                                if($result):
                                while ( $arow = $result->fetch_assoc()) {
                            ?>
                            <div class="app-details">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="p-image">
                                            <?php if(!empty($arow['profile_image'])){ ?>
                                            <img class="preview_photo" src="../<?= $arow['profile_image'] ?>" width="150" height="150"/>
                                            <?php }else{ ?>
                                            <img src="../images/avatar-default.png" class="img-responsive" alt="avatar default">
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <h3><span class="doctor-title">Dr. <?= $arow['first_name'] . ' ' . $arow['last_name'] ?></span></h3>
                                        <div class="qualif">
                                            <?php
                                            $edu_result = $doctor->get_education_info_by_id($arow['d_id']);
                                            if ($edu_result):
                                                while ($edu_row = $edu_result->fetch_assoc()):
                                                    $degree[] = $edu_row['degree_form'] . ' (' . $edu_row['major_subject'] . ') ';
                                                    $print_degree = implode(',  ', $degree);
                                                endwhile;
                                                echo '<p>' . $print_degree . '</p>';
                                            endif;
                                            ?>
                                        </div>
                                        <div class="dp-dg"><?= $arow['specialists']; ?></div>
                                    </div>
                                    <div class="col-sm-5">
                                        <h4 class="well warning">Patient Problem Discussion</h4>
                                    </div>
                                    <div class="col-xs-12">  
                                        <hr>
                                        <div class="spacific-problem"><span><b>Send Spacific Problem by You:</b>
                                        <span><?= $fm->getDate($arow['created_at'], 'jS M Y,  g:i:s'); ?></span>
                                        </span> <p><?= $arow['problem']; ?></p></div>
                                        <?php if(!empty($arow['problem_image'])): ?>
                                        <div class="image-gellary">
                                            <?php
                                             $array =  explode(',', $arow['problem_image']);
                                             foreach ($array as $value) {
                                                $value = str_replace(' ','',$value);
                                                echo '<a rel="example_group" href="../'.$value.'" title=""><img alt="" src="../'.$value.'"  width="150" height="150"/></a>';
                                              } 
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(!empty($arow['audio_video'])): ?>
                                    <div class="audio-message">
                                        <?php 
                                            $audio_type     = array( 'mp3', 'ogg', 'wav');
                                            $audio_divided = explode('.', $arow['audio_video']);
                                            $audio_ext      = strtolower( end($audio_divided) );
                                            if( in_array( $audio_ext, $audio_type) === TRUE ){
                                        ?>
                                        <audio controls>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/mpeg"/>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/ogg"/>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/wav"/>
                                            Your browser does not support the video tag.
                                        </audio>
                                        <?php }else{ ?>
                                            <video width="320" height="240" controls>
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/mp4">
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/ogg">
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/webm">
                                            Your browser does not support the video tag.
                                            </video>
                                        <?php } ?>
                                    </div>
                                    <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php if($mresult){ ?>
                            <div class="row">
                                <?php while ($mrow = $mresult->fetch_assoc()) {?>
                                <div class="col-xs-12 bottom-border">
                                    <div class="spacific-problem"><span><b>Reply to <?= $mrow['sender']?>:</b>
                                    <span><?= $fm->getDate($mrow['create_message'], 'jS M Y,  g:i:s'); ?></span>
                                    </span> <p><?= $mrow['message']; ?></p></div>
                                    <?php if(!empty($mrow['image'])): ?>
                                    <div class="image-gellary">
                                        <?php
                                         $array =  explode(',', $mrow['image']);
                                         foreach ($array as $value) {
                                            $value = str_replace(' ','',$value);
                                            echo '<a rel="example_group" href="'.$value.'" title=""><img alt="" src="'.$value.'"  width="150" height="150"/></a>';
                                          } 
                                        ?>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(!empty($mrow['media'])): ?>
                                    <div class="audio-message">
                                        <?php 
                                            $audio_type     = array( 'mp3', 'ogg', 'wav');
                                            $audio_divided = explode('.', $mrow['media']);
                                            $audio_ext      = strtolower( end($audio_divided) );
                                            if( in_array( $audio_ext, $audio_type) === TRUE ){
                                        ?>
                                        <audio controls>
                                          <source src="<?= $mrow['media']; ?>" type="audio/mpeg"/>
                                          <source src="<?= $mrow['media']; ?>" type="audio/ogg"/>
                                          <source src="<?= $mrow['media']; ?>" type="audio/wav"/>
                                            Your browser does not support the video tag.
                                        </audio>
                                        <?php }else{ ?>
                                            <video width="320" height="240" controls>
                                              <source src="<?= $mrow['media']; ?>" type="video/mp4">
                                              <source src="<?= $mrow['media']; ?>" type="video/ogg">
                                              <source src="<?= $mrow['media']; ?>" type="video/webm">
                                            Your browser does not support the video tag.
                                            </video>
                                        <?php } ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-xs-12">
                                   <div class="accordion">
                                        <div class="accordion-section">
                                            <a class="accordion-section-title" href="#accordion-1">Send Message to Doctor</a>
                                            <div id="accordion-1" class="accordion-section-content">
                                            <form name="" id="" action="" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="appoint_id" value="<?= $arow['appoint_id']; ?>">
                                                <input type="hidden" name="sender" value="Patient">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <label for="doc_office_name">Message</label>
                                                            <textarea name="message" style="resize: none;" cols="6" rows="6" class="required form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                                <input name="file[]" id="img_file" type="file" multiple accept="image/*"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="form-group row">
                                                            <label for="inputEmail3" class="col-sm-3">Audio or Video</label>
                                                            <div class="col-sm-9">
                                                                <input name="audiovideo" type="file" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group row">
                                                            <hr>
                                                            <div class="col-sm-12">
                                                                <input type="submit"  id="upload" class="upload doct-submit button btn-primary" name="dquery" value="Send">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div><!--end .accordion-section-content-->
                                        </div><!--end .accordion-section-->
                                    </div><!--end .accordion-->
                                </div>
                            </div> 
                            <?php   
                                }
                                else: 

                                endif; 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php';?>