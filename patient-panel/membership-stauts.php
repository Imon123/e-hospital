<div class="membership_status clearfix">
    <div class="col-xs-6">
     <h4 class='type-style'>Account Type : <?= ucfirst($pt_prf['account_type']) ?></h4>
    </div>
    <div class="col-xs-6">
        <div id="membership_expire">
            <?php 
                $userMembershipDate = $pt_prf['expire_start'];
                $membershipEnds = date("Y-m-d ", strtotime(date("Y-m-d", strtotime($userMembershipDate)).' + '.$pt_prf['expire_date'].' day'));
                $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                if($pt_prf['expire_date'] == ''){
                    echo '<h4 class="expired">Please make first membership! <a href="package">click here</a></h4>';
                  }else{
                    if($ex_dt->format('Y-m-d') < $membershipEnds){
                    $date1 = date("Y-m-d",strtotime($userMembershipDate));
                    $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                    $date2 = $ex_dt->format('Y-m-d');
                    $datetime1 = new DateTime($date1);
                    $datetime2 = new DateTime($date2);
                    $interval = $datetime1->diff($datetime2);
                    $days = ($pt_prf['expire_date'] - $interval->format('%a'));
                        if($days > 1 ){
                            echo "<h4 class='not-expired'>Membership Expires in $days days</h4>";
                        }else{
                            echo "<h4 class='not-expired'>Membership Expires in $days day</h4>";
                        }              
                    }else{
                        echo '<h4 class="expired">Membership has expired. Please renew! <a href="package">click here</a></h4>';
                    }
                  }
                
            ?>
        </div>
    </div> 
</div>