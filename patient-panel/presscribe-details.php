<?php
include '../header.php';
include 'psession.php';
?>
<script >
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                    <h2>Prescription</h2>
                    <hr>
                        <button class="btnprint" onclick="printDiv('printableArea')"><i class="fa fa-print" style="font-size: 17px;"> </i> Print</button>
                    <div id="pup_message"></div>
                    <div class="row">
                     <?php include 'membership-message.php'; ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php 
                            if(isset($_GET['presscribe-id']) && $_GET['presscribe-id'] != null){
                                $result = $patient->get_doctor_presscribe($_GET['presscribe-id'], $v = null);
                            }
                            if($result):
                            while ( $arow = $result->fetch_assoc()) {
                            ?>
                            <form id="form1">
                            <div class="app-details press" id="printableArea">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="p-image">
                                            <?php if(!empty($arow['profile_image'])){ ?>
                                            <img class="preview_photo" src="../<?= $arow['profile_image'] ?>" width="150" height="150"/>
                                            <?php }else{ ?>
                                            <img src="../images/avatar-default.png" class="img-responsive" alt="avatar default">
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <h3><span class="doctor-title">Dr. <?= $arow['first_name'] . ' ' . $arow['last_name'] ?></span></h3>
                                        <div class="qualif">
                                            <?php
                                            $edu_result = $doctor->get_education_info_by_id($arow['d_id']);
                                            if ($edu_result):
                                                while ($edu_row = $edu_result->fetch_assoc()):
                                                    $degree[] = $edu_row['degree_form'] . ' (' . $edu_row['major_subject'] . ') ';
                                                    $print_degree = implode(',  ', $degree);
                                                endwhile;
                                                echo '<p>' . $print_degree . '</p>';
                                            endif;
                                            ?>
                                        </div>
                                        <div class="dp-dg"><?= $arow['specialists']; ?></div>
                                    </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6 col-sm-3"><span><strong>Name: </strong><?= $pt_prf['first_name'] . ' ' . $pt_prf['last_name'];?></span></div>
                                <div class="col-xs-6 col-sm-3"><span><strong>ID No: </strong><?= $arow['id']; ?></span></div>
                                <div class="col-xs-6 col-sm-3"><span><strong>Age: </strong><?= $pt_prf['birthday'] ?></span></div>
                                <div class="col-xs-6 col-sm-3"><span><strong>Date: </strong><?= $fm->getDate($arow['create_date'], 'd-M-Y'); ?></span></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-3"><span><strong>Phone No: </strong><?= $pt_prf['mobile_no'] ?></span></div>
                                
                            </div>
                            <hr>
                            <div class="row">
                                <?php 
                                    $deseise = $arow['deseise'];
                                if(!empty($arow['deseise'])){ ?>
                                <div class="col-xs-12 col-sm-4">
                                   <div class="desise-inner"> <p><?= $arow['deseise']; ?></p></div>
                                </div>
                                <?php } ?>
                                <div class="col-xs-12 col-sm-<?php if(empty($deseise)){ echo '12'; }else{ echo '8';}?>">
                                    <p><?= $arow['medicine']; ?></p>
                                </div>
                                
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <img src="../images/logo.png" alt="site logo">
                                </div>
                                <div class="col-xs-12 col-sm-6"></div>
                            </div>
                            <?php   
                                }
                                else: 

                                endif; 
                            ?>
                        </div>

    <input type="hidden" value="Print Div Contents" id="btnPrint" />
    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include '../footer.php';?>
   <script type="text/javascript">
        // $("#btnPrint").on("click", function () {
        //     var divContents = $("#dvContainer").html();
        //     var printWindow = window.open('', '', 'height=400,width=800');
        //     printWindow.document.write('<html><head><title>Prescription</title>');
        //     document.write('<link rel="stylesheet" href="<?php echo $fm->base_url(); ?>css/bootstrap.min.css">');
        //     printWindow.document.write('</head><body >');
        //     printWindow.document.write(divContents);
        //     printWindow.document.write('</body></html>');
        //     printWindow.document.close();
        //     printWindow.print();
        // });
    </script>