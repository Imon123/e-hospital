<?php
include '../header.php';
include 'psession.php';
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                    <div class="row">
                     <?php include 'membership-message.php'; ?>
                    </div>
                    <h2>New Message List</h2>
                    <table class="table table-inside">
                        <thead>
                            <tr>
                                <th>SL NO.</th>
                                <th>Doctor Name</th>
                                <th>Spacialist</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $result = $patient->get_doctor_presscribe($id = null, $pid);

                                if($result):
                                    $i = 0;
                                while ( $arow = $result->fetch_assoc()) {  
                                    $i++;
                            ?>
                            <tr>
                                <td><span id="update_message"></span> <?= $i; ?></td>
                                <td><?= $arow['first_name'] .' '.$arow['last_name']; ?></td>
                                <td><?= $arow['specialists']; ?></td>
                                <td><?= $fm->getDate($arow['create_date'], 'd M Y,  g:i:s a'); ?></td>
                                <td>
                                    <a href="presscribe-details?presscribe-id=<?= $arow['id']?>">Prescribe Details</a>
                                </td>
                            </tr>
                        <?php } else: ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php';?>