<?php
include '../header.php';
include 'psession.php';
?>

<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                <div class="row">
                    <?php include 'membership-message.php'; ?>
                    <?php include 'membership-stauts.php'; ?>  
                </div>
                <table class="table table-bordered table-hover">
                    <tr>
                        <td>Name </td>
                        <td><?= $pt_prf['first_name'] . ' ' . $pt_prf['last_name']; ?></td>
                    </tr>
                    <tr>
                        <td>Father Name</td>
                        <td><?= $pt_prf['father_name'] ?></td>
                    </tr>
                    <tr>
                        <td>Mother Name</td>
                        <td><?= $pt_prf['mother_name'] ?></td>
                    </tr>
                    <tr>
                        <td>Email Address </td>
                        <td><?= $pt_prf['email'] ?></td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td><?= $pt_prf['mobile_no'] ?></td>
                    </tr>

                    <tr>
                        <td>Height </td>
                        <td><?= $pt_prf['height'] ?></td>
                    </tr>
                    <tr>
                        <td>Weight</td>
                        <td><?= $pt_prf['weight'] ?></td>
                    </tr>
                    <tr>
                        <td>Blood Group</td>
                        <td><?= $pt_prf['blood_group'] ?></td>
                    </tr>
                    <tr>
                        <td>National ID No.</td>
                        <td><?= $pt_prf['nid'] ?></td>
                    </tr>
                    <tr>
                        <td>Gender </td>
                        <td><?= $pt_prf['gender'] ?></td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td><?= $pt_prf['birthday'] ?></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><?= $pt_prf['address'] ?></td>
                    </tr>
                </table>
            </div>
         </div>
        </div>

    </div>
</div>
<?php include '../footer.php'; ?>

