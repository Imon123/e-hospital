<?php
include '../header.php';
include 'psession.php';
$pt_prf= $patient->get_patient_profile_by_id($pid);
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-3">
            <?php include 'patient-sidebar.php'; ?>
        </div>
        <div class="col-xs-9">
            <div class="mystate">
            <div class="row">
                <?php include 'membership-message.php'; ?>
                <?php 
                    if($_SERVER['REQUEST_METHOD'] && isset($_POST['pupdate_btn'])){
                      $patient->update_patient_profile_by_id($_POST); 
                      $pt_prf= $patient->get_patient_profile_by_id($pid); 
                    }
                ?>  
            </div>
            <div class="col-xs-4 col-xs-offset-8" style="padding-bottom: 30px">
                <table class="table-hover">
                    <?php if($pt_prf['profile_img']){ ?>
                    <tr>
                        <td>
                            <img class="preview_photo" src="../<?= $pt_prf['profile_img'] ?>" width="150" height="150"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <button type="submit" onclick="deletePatientPhoto(<?= $pid; ?>)" class="btn-primary">
                             Change Photo
                           </a>
                        </td>
                    </tr>
                    <?php }else{
                        ?>
                    <tr>
                        <td>
                            <div class="dupdate-profile">
                                <div class="profile-image">
                                <form id="uploadpimage" action="" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="pt_id" value="<?php echo $pid; ?>">
                                <div id="image_preview"><img id="previewing" src="noimage.png" width="150" height="150"/></div>
                                <div id="selectImage">
                                <input type="file" name="file" id="file"/>
                                <div class="message-wrrp"><div id="message"></div></div>
                                <div class="photo-button">
                                <span id='loading' ><img src="../images/loading_circle.gif"></span>
                                <input type="submit" value="Upload Photo" class="submit" />
                                </div>
                                </div>
                                </form>
                                
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                </div>
                <form name="edit_patient_reg" id="ptsignup" action="" method="post">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="main-agileits form-group clearfix">
                                <?php if($pt_prf['account_type'] != 'Premium'){ ?>
                                <div class="four w3grids-agile">
                                    <input <?php if($pt_prf['account_type'] == 'Premium'): echo 'checked'; endif;?> type="radio" name="account_type" value="Premium" id="r1">
                                    <label for="r1">
                                        <h4>Premium</h4>
                                    </label>
                                </div>
                                <div class="four w3grids-agile">
                                    <input <?php if($pt_prf['account_type'] == 'Free Trial'): echo 'checked'; endif;?> type="radio" name="account_type" value="Free Trial" id="r3">
                                    <label for="r3">
                                        <h4>Free Trail</h4>
                                    </label>
                                </div> 
                                <?php }else{ ?>
                                    <div class="four w3grids-agile">
                                    <input <?php if($pt_prf['account_type'] == 'Premium'): echo 'checked'; endif;?> type="radio" name="account_type" value="Premium" id="r1">
                                    <label for="r1">
                                        <h4>Premium</h4>
                                    </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="first_name">First Name <span class="required">*</span></label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" value="<?= $pt_prf['first_name']; ?>">
                                    <input type="hidden" name="pt_id" lass="form-control" id="first_name" value="<?= $pt_prf['pt_id']; ?>">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="last_name">Last Name <span class="required">*</span></label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" value="<?= $pt_prf['last_name']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label for="father_name">Father Name</label>
                                <input type="text" name="father_name" class="form-control" id="father_name" value="<?= $pt_prf['father_name']; ?>">
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="mother_name">Mother Name </label>
                                <input type="text" name="mother_name" class="form-control" id="mother_name" value="<?= $pt_prf['mother_name']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label for="email">Email Address <span class="required">*</span></label>
                                <input type="text" name="email" class="form-control" id="email" value="<?= $pt_prf['email']; ?>">
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="mobile_no">Mobile Number <span class="required">*</span></label>
                                <input type="text" name="mobile_no" class="form-control" id="mobile_no" value="<?= $pt_prf['mobile_no']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="height">Height <span class="required">*</span></label>
                                    <input type="text" name="height" class="form-control" id="height" value="<?= $pt_prf['height']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="weight">Weight <span class="required">*</span></label>
                                    <input type="text" name="weight" class="form-control" id="weight" value="<?= $pt_prf['weight']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="blood_group">Blood Group <span class="optional">(Optional)</span></label>
                                    <select name="blood_group" class="form-control" id="blood_group">
                                        <option value="">Select Blood Group</option>
                                        <option <?php if($pt_prf['blood_group'] == 'A+'): echo 'selected'; endif;?> value="A+">A+</option>
                                        <option <?php if($pt_prf['blood_group'] == 'A-'): echo 'selected'; endif;?> value="A-">A-</option>
                                        <option <?php if($pt_prf['blood_group'] == 'B+'): echo 'selected'; endif;?> value="B+">B+</option>
                                        <option <?php if($pt_prf['blood_group'] == 'B-'): echo 'selected'; endif;?> value="B-">B-</option>
                                        <option <?php if($pt_prf['blood_group'] == 'O+'): echo 'selected'; endif;?> value="O+">O+</option>
                                        <option <?php if($pt_prf['blood_group'] == 'O-'): echo 'selected'; endif;?> value="O-">O-</option>
                                        <option <?php if($pt_prf['blood_group'] == 'AB+'): echo 'selected'; endif;?> value="AB+">AB+</option>
                                        <option <?php if($pt_prf['blood_group'] == 'AB-'): echo 'selected'; endif;?> value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nid">NID or Passport NO. <span class="optional">(Optional)</span></label>
                                    <input type="text" name="nid" class="form-control" id="nid" value="<?= $pt_prf['nid']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="gender">Gender <span class="required">*</span></label>
                                <select name="gender" class="form-control" id="gender">
                                    <option value="">Select Gender</option>
                                    <option <?php if($pt_prf['gender'] == 'Male'): echo 'selected'; endif;?> value="Male">Male</option>
                                    <option <?php if($pt_prf['gender'] == 'Female'): echo 'selected'; endif;?> value="Female">Female</option>
                                    <option <?php if($pt_prf['gender'] == 'Others'): echo 'selected'; endif;?> value="Others">Others</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-sm-6">
                                 <div class="form-group">
                                    <label for="birthday">Age <span class="required">*</span></label>
                                    
                                    <select name="birthday" class="form-control" id="pbirthday">
                                        <option value="<?= $pt_prf['birthday']; ?>"><?= $pt_prf['birthday']; ?></option>
                                        <?php 
                                        $j = 0;
                                        for($j = 0; $j<=12; $j++){ ?>
                                            <option value="<?php echo $j." "."Months";?>"><?php echo $j." "."Months";?></option>
                                        <?php }
                                        $i = 0;
                                        for($i = 1; $i<=110; $i++){ ?>
                                            <option value="<?php  echo $i." "."Years";?>"><?php echo $i." "."Years";?></option>
                                       <?php  } ?>
                                    </select>
                                    <!--$fm->getDate($pt_prf['birthday'], "m-d-Y")-->
                                </div>
                            </div>
                        </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="address">Address <span class="required">*</span></label>
                                <textarea class="form-control" id="address" name="address" style="resize: none"><?= $pt_prf['address']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="pt-submit button btn-primary" id="pt_button" name="pupdate_btn">Update</button>
                </form>
        </div>
    </div>
    </div>
</div>
<?php
include '../footer.php';
?>
