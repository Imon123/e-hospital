-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 03:07 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telemedicine`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(42) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fullname`, `username`, `email`, `password`, `createdate`) VALUES
(1, 'Azizul Haque', 'azizul', 'azizul@gmail.com', '131ddb54f8a94da689fb1565372cdfb9d33b2106', '2017-07-28 18:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `clinics`
--

CREATE TABLE `clinics` (
  `c_id` int(11) NOT NULL,
  `clinic_name` varchar(255) NOT NULL,
  `clinic_location` varchar(255) NOT NULL,
  `ownertype` varchar(55) NOT NULL,
  `ownername` varchar(55) NOT NULL,
  `clinictype` varchar(55) NOT NULL,
  `licnumber` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `clinicaddress` text NOT NULL,
  `approval_status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `clinics`
--

INSERT INTO `clinics` (`c_id`, `clinic_name`, `clinic_location`, `ownertype`, `ownername`, `clinictype`, `licnumber`, `email`, `mobile_no`, `startdate`, `clinicaddress`, `approval_status`, `created_at`, `password`) VALUES
(10, 'Dhaka Medical', 'Metro', 'Public', 'Jamal', 'Outpatient Clinic', '2222222222', 'admin@gmail.com', 101010101, '2014-12-24', 'Dhaka', 0, '2017-09-08 16:49:36', '131ddb54f8a94da689fb1565372cdfb9d33b2106'),
(11, 'Comilla Medical', 'Critical', 'Private', 'Mamun', 'Outpatient', '12121201', 'info@gmail.com', 1214212121, '2014-05-05', 'Comilla, Bangladesh', 0, '2017-09-08 16:49:36', '131ddb54f8a94da689fb1565372cdfb9d33b2106');

-- --------------------------------------------------------

--
-- Table structure for table `deducations`
--

CREATE TABLE `deducations` (
  `dedu_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `degree_title` varchar(255) NOT NULL,
  `degree_form` varchar(50) NOT NULL,
  `institute_name` text NOT NULL,
  `passing_year` varchar(4) NOT NULL,
  `major_subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deducations`
--

INSERT INTO `deducations` (`dedu_id`, `d_id`, `degree_title`, `degree_form`, `institute_name`, `passing_year`, `major_subject`) VALUES
(56, 13, 'test', 'MD, Univ ISCM, 2013, Cuba', 'test', '1995', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `discussion`
--

CREATE TABLE `discussion` (
  `id` int(11) NOT NULL,
  `appoint_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `media` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `patient_status` varchar(100) DEFAULT 'unread',
  `doctor_status` varchar(100) NOT NULL DEFAULT 'unread',
  `create_message` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discussion`
--

INSERT INTO `discussion` (`id`, `appoint_id`, `message`, `image`, `media`, `sender`, `patient_status`, `doctor_status`, `create_message`) VALUES
(1, 27, 'Describe your problem please..', '', '', 'Doctor', 'read', 'read', '2017-09-15 18:49:08'),
(2, 27, 'Please listen the audio', '', '../audiovideo/asdddddasdf.ogg', 'Patient', 'read', 'read', '2017-09-15 18:51:16');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `d_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `idnetity_no` varchar(50) NOT NULL,
  `nid_passport` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `specialists` varchar(255) NOT NULL,
  `password` varchar(42) NOT NULL,
  `present_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `self_about` text NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `active_status` tinyint(4) DEFAULT '0',
  `online_status` tinyint(4) NOT NULL DEFAULT '0',
  `approval_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`d_id`, `first_name`, `last_name`, `idnetity_no`, `nid_passport`, `email`, `mobile_no`, `gender`, `birthday`, `specialists`, `password`, `present_address`, `permanent_address`, `self_about`, `profile_image`, `slug`, `active_status`, `online_status`, `approval_status`) VALUES
(13, 'Kajram', 'Deoram', '1234560', '123365558899', 'imran@gmail.com', '01258635547', 'Male', '1970-01-01', 'Medicine', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/download2.jpg', 'kajram-kajram-1234560', 0, 1, 1),
(14, 'Kanhai', 'Mario', '1234561', '123365558899', 'Kanhai@gmail.com', '01258635547', 'Female', '1970-01-01', 'Medicine', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/download1.jpg', 'kajram-kajram-1234561', 0, 0, 1),
(15, 'Kasim', 'Razia', '1234562', '123365558899', 'kasim@gmail.com', '01258635547', 'Male', '1970-01-01', 'Cardiologist ', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/images.jpg', 'kajram-kajram-1234562', 0, 0, 1),
(16, 'Kendall', 'Johnathan', '1234563', '123365558899', 'kendall@gmail.com', '01258635547', 'Female', '1970-01-01', 'Cardiologist ', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/download4.jpg', 'kajram-kajram-1234563', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_job_history`
--

CREATE TABLE `doctor_job_history` (
  `doc_job_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `doc_office_name` varchar(250) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(80) NOT NULL,
  `office_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_job_history`
--

INSERT INTO `doctor_job_history` (`doc_job_id`, `d_id`, `doc_office_name`, `start_date`, `end_date`, `office_address`) VALUES
(17, 13, 'Labaid', '1970-01-01', 'Continuing', 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `dtrainings`
--

CREATE TABLE `dtrainings` (
  `dtr_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `training_title` varchar(255) NOT NULL,
  `institute_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtrainings`
--

INSERT INTO `dtrainings` (`dtr_id`, `d_id`, `training_title`, `institute_name`, `address`, `description`) VALUES
(11, 13, 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `pt_id` bigint(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `height` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `nid` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `birthday` varchar(25) NOT NULL,
  `password` varchar(42) NOT NULL,
  `profile_img` varchar(255) CHARACTER SET ucs2 NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `expire_start` datetime NOT NULL,
  `expire_date` varchar(10) NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '0',
  `read_status` varchar(100) NOT NULL DEFAULT 'unread',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`pt_id`, `first_name`, `last_name`, `father_name`, `mother_name`, `email`, `mobile_no`, `height`, `weight`, `blood_group`, `nid`, `gender`, `address`, `birthday`, `password`, `profile_img`, `account_type`, `expire_start`, `expire_date`, `active_status`, `read_status`, `create_date`) VALUES
(20, 'Rana', 'Khan', 'Hamid Khan', 'Rahima Begum', 'rana@gmail.com', '01255888445', '5 Feet 5 Inch', '65 Kg', 'B', '12554745', 'Male', 'Dhaka', '24 Years', '131ddb54f8a94da689fb1565372cdfb9d33b2106', '', 'Premium', '2017-09-15 14:46:07', '182', 0, 'unread', '2017-09-15 12:46:07');

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int(11) NOT NULL,
  `pt_id` int(11) NOT NULL,
  `transection_id` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `package_day` varchar(20) NOT NULL,
  `payment_approve_status` tinyint(1) DEFAULT '0',
  `pay_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `pt_id`, `transection_id`, `amount`, `package_day`, `payment_approve_status`, `pay_date`) VALUES
(33, 20, '25896', '10000.00', '6 months', 1, '2017-09-15 18:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `id` int(11) NOT NULL,
  `appoint_id` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `deseise` text NOT NULL,
  `medicine` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`id`, `appoint_id`, `did`, `pid`, `deseise`, `medicine`, `create_date`) VALUES
(2, 27, 13, 20, '', 'Pentobarbital sodium (NembutalÂ®)\r\nDiazepam (ValiumÂ®)\r\nAlprazolam (XanaxÂ®)', '2017-09-15 18:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointment`
--

CREATE TABLE `tbl_appointment` (
  `appoint_id` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `ptid` int(11) NOT NULL,
  `problem` text NOT NULL,
  `problem_image` varchar(250) NOT NULL,
  `audio_video` varchar(250) NOT NULL,
  `appoint_approval_status` tinyint(1) NOT NULL DEFAULT '0',
  `patient_status` varchar(100) NOT NULL DEFAULT 'unread',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_appointment`
--

INSERT INTO `tbl_appointment` (`appoint_id`, `did`, `ptid`, `problem`, `problem_image`, `audio_video`, `appoint_approval_status`, `patient_status`, `created_at`) VALUES
(27, 13, 20, 'test', '', '', 1, 'read', '2017-09-15 18:46:34');

-- --------------------------------------------------------

--
-- Table structure for table `working_hours`
--

CREATE TABLE `working_hours` (
  `hour_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `day_name` varchar(20) NOT NULL,
  `from_to` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinics`
--
ALTER TABLE `clinics`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `deducations`
--
ALTER TABLE `deducations`
  ADD PRIMARY KEY (`dedu_id`);

--
-- Indexes for table `discussion`
--
ALTER TABLE `discussion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `doctor_job_history`
--
ALTER TABLE `doctor_job_history`
  ADD PRIMARY KEY (`doc_job_id`);

--
-- Indexes for table `dtrainings`
--
ALTER TABLE `dtrainings`
  ADD PRIMARY KEY (`dtr_id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_appointment`
--
ALTER TABLE `tbl_appointment`
  ADD PRIMARY KEY (`appoint_id`);

--
-- Indexes for table `working_hours`
--
ALTER TABLE `working_hours`
  ADD PRIMARY KEY (`hour_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clinics`
--
ALTER TABLE `clinics`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `deducations`
--
ALTER TABLE `deducations`
  MODIFY `dedu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `discussion`
--
ALTER TABLE `discussion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `doctor_job_history`
--
ALTER TABLE `doctor_job_history`
  MODIFY `doc_job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `dtrainings`
--
ALTER TABLE `dtrainings`
  MODIFY `dtr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `pt_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_appointment`
--
ALTER TABLE `tbl_appointment`
  MODIFY `appoint_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `working_hours`
--
ALTER TABLE `working_hours`
  MODIFY `hour_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
