<footer class="footer-wrp">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-logo-wid">
                    <img src="<?php echo $pre_link;?>images/logo.png" alt="logo">
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-logo-wid">
                    <h2>Navigation</h2>
                    <ul class="footer-menu">
                        <li><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="">Home</a></li>
                        <li><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="">Doctor</a></li>
                        <li><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="">Services</a></li>
                        <li><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="">Department</a></li>
                        <li><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-contact">
                    <h2>Contact Us</h2>
                    <div class="contact-icon">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>Medical street, Dhaka, Bangladesh</p>
                    </div>
                    <div class="contact-icon fa-size">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <p>info@medical.com</p>
                    </div>
                    <div class="contact-icon fa-size">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p>+0088 01711 223 344</p>
                    </div>
                    <div class="footer-social-icon">
                        <ul>
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="">
                    
                </div>
            </div>
        </div>
    </div>
</footer>
<footer class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-copyright">Copyright 2016 All Rights Reserved</div>
            </div>
        </div>
    </div>
</footer>

<!-- script -->
<?php 
if (($current_page == 'appointment-details') || ($current_page == 'message-details')){
?>
<script src="<?php echo $fm->base_url(); ?>js/jquery-3.2.1.min.js"></script>
<?php }else{ ?>
<script src="<?php echo $fm->base_url(); ?>js/jquery-1.11.3.min.js"></script>
<?php } ?>
<script src="<?php echo $fm->base_url(); ?>js/ie10-viewport-bug-workaround.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/select2.full.min.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo $fm->base_url(); ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/jquery.validate.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/doctor-js.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/patient-js.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/clinic-js.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/imageuploadify.js"></script>
<script src="<?php echo $fm->base_url(); ?>js/js.js"></script>
<script type="text/javascript">
    $('#img_file').imageuploadify();
</script> 
<!-- end of script -->
</body>
</html>
</body>
</html>
<?php  //ob_end_flush(); ?>