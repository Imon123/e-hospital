<?php
include '../header.php';
include './dsession.php';
$doctor_personal_details = $doctor->doctor_personal_details($did);
$doctor_education_info = $doctor->doctor_education_info($did);
$doctor_training_info = $doctor->doctor_training_info($did);
$doctor_job_history = $doctor->doctor_job_history($did);
//$doctor_info = $doctor->doctor_profile($did);
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['docproupdate'])) {
    $doctor->update_doctor_info($_POST, $did);
    $doctor_personal_details = $doctor->doctor_personal_details($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['docedupdate'])) {
    $dedu_id = $_POST['docedupdate'];
    $doctor->update_doctor_education_by_id($_POST, $dedu_id);
    $doctor_education_info = $doctor->doctor_education_info($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['doctrainingupdate'])) {
    $dtr_id = $_POST['doctrainingupdate'];
    $doctor->update_doctor_training_by_id($_POST, $dtr_id);
    $doctor_training_info = $doctor->doctor_training_info($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['docjobhisupdate'])) {
    $doc_job_id = $_POST['docjobhisupdate'];
    $doctor->update_doctor_job_history_by_id($_POST, $doc_job_id);
    $doctor_job_history = $doctor->doctor_job_history($did);
    $doctor_training_info = $doctor->doctor_training_info($did);
}

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['education-id'])) {
    $doc_edu_id = $_POST['education-id'];
    $doctor->delete_doctor_education($doc_edu_id);
    $doctor_education_info = $doctor->doctor_education_info($did);
}

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['training-id'])) {
    $doc_training_id = $_POST['training-id'];
    $doctor->delete_doctor_training($doc_training_id);
    $doctor_training_info = $doctor->doctor_training_info($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['doc-job-history-id'])) {
    $doc_job_history_id = $_POST['doc-job-history-id'];
    $doctor->delete_doctor_job_history_ajax($doc_job_history_id);
    $doctor_job_history = $doctor->doctor_job_history($did);
    $doctor_training_info = $doctor->doctor_training_info($did);
}

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['did'])) {
    $doctor->save_doctor_education($_POST);
    $doctor_education_info = $doctor->doctor_education_info($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['training_did'])) {
    $doctor->add_more_doctor_training($_POST);
    $doctor_education_info = $doctor->doctor_education_info($did);
    $doctor_training_info = $doctor->doctor_training_info($did);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['addmorejob'])) {
    $doctor->add_more_doctor_job($_POST);
    $doctor_education_info = $doctor->doctor_education_info($did);
    $doctor_training_info = $doctor->doctor_training_info($did);
    $doctor_job_history = $doctor->doctor_job_history($did);
}
?>
<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 height-control">
                <div class="row">
                    <?php include_once 'important-notification.php';?>
                    <div class="col-xs-12 col-sm-3">
                        <div class="leftsidebar">
                            <ul> 
                                <li>
                                    <a <?php
                                    if ($current_page == 'index'): echo 'class="menu-active"';
                                    endif;
                                    ?> href="index">My State</a>
                                </li>
                                <li>
                                    <a <?php
                                    if ($current_page == 'doctor-profile'): echo 'class="menu-active"';
                                    endif;
                                    ?> href="doctor-profile">View Profile</a>
                                </li>
                                <li>
                                    <a <?php
                                    if ($current_page == 'edit-profile'): echo 'class="menu-active"';
                                    endif;
                                    ?> href="edit-profile">Edit Profile</a>
                                </li>                        
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="mystate clearfix">
                        <div class="col-xs-4 col-xs-offset-8" style="padding-bottom: 30px">
                            <table class="table-hover">
                                <?php if ($doctor_personal_details['profile_image']) { ?>
                                    <tr>
                                        <td>
                                            <img class="preview_photo" src="../<?= $doctor_personal_details['profile_image'] ?>" width="200" height="200"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <button type="submit" onclick="deleteDoctorPhoto(<?= $did; ?>)" class="btn-primary">
                                                Change Photo
                                                </a>
                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td>
                                            <div class="dupdate-profile">
                                                <div class="profile-image">
                                                    <form id="updateimage" action="" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" id="d_id" value="<?php echo $did; ?>">
                                                        <div id="image_preview"><img id="previewing" src="noimage.png" width="200" height="200"/></div>
                                                        <div id="selectImage">
                                                            <input type="file" name="file" id="file"/>
                                                            <div class="message-wrrp"><div id="message"></div></div>
                                                            <div class="photo-button">
                                                                <span id='loading' ><img src="../images/loading_circle.gif"></span>
                                                                <input type="submit" value="Upload Photo" class="submit" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div> 
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <div class="col-xs-12">
                            <div style="border-bottom: 2px solid #000;">

                            </div>
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Personal Details</u></h4>
                            <table class="table table-hover">
                                <div class="col-xs-4 col-xs-offset-8">
                                    <a  href="#doctormodal_<?= $did; ?>" data-toggle="modal" class="btn-primary btn-lg btn-group">
                                        <span class="fa fa-pencil-square-o"></span>
                                    </a>
                                </div>
                                <tr>
                                    <td>First Name</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['first_name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['last_name'] ?></td>
                                </tr>
                                <tr>
                                    <td>Mobile No</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['mobile_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['email'] ?></td>
                                </tr>
                                <tr>
                                    <td>Identity No</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['idnetity_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>National ID Or Passport No.</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['nid_passport'] ?></td>
                                </tr>
                                <tr>
                                    <td>Birth Date</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['birthday'] ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['gender'] ?></td>
                                </tr>
                                <tr>
                                    <td>Specialists</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['specialists'] ?></td>
                                </tr>
                                <tr>
                                    <td>Present Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['present_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['permanent_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Self About</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['self_about'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <?php
                        $i = 0;
                        if (!empty($doctor_education_info)) {
                            ?>
                            <div class="col-xs-12">
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Academic Qualification</u></h4>

                                <?php
                                while ($doctor_education = $doctor_education_info->fetch_assoc()) {
                                    $i++;
                                    $dedu_id = $doctor_education['dedu_id'];
                                    $degree_title = $doctor_education['degree_title'];
                                    $degree_short_form = $doctor_education['degree_form'];
                                    $major_subject = $doctor_education['major_subject'];
                                    $institute_name = $doctor_education['institute_name'];
                                    $passing_year = $doctor_education['passing_year'];
                                    ?>
                                    <table class="table-hover">

                                        <div class="col-xs-4 col-xs-offset-8">
                                            <a style="" href="#docedumodal_<?= $dedu_id ?>" data-toggle="modal" class="btn-primary btn-lg inline-group" title="Edit-Education">
                                                <span class="fa fa-pencil-square-o"></span>
                                            </a>
                                            <form action="" method="post" style="display: inline; text-align: right" class="right">
                                                <input type="hidden" name="education-id" value="<?= $dedu_id ?>" />
                                                <button type="submit" onclick="return confirm('Are you sure you want to delete this item?');" name="btn" class="button-right btn-lg btn-danger inline-group" title="Delete-Education" style="margin-left: 20px">
                                                    <span class="fa fa-trash-o"></span>
                                                </button>
                                            </form>
                                        </div>
                                        <?php
                                        //if($doctor_education_info>1){
                                        ?>
                                        <tr>
                                            <td><h5 class=""><u><?= "Qualification" . " " . $i; ?></u></h5></td>
                                            <td class="col-sm-1"></td>
                                            <td></td>
                                        </tr>
                                        <?php //}    ?>
                                        <tr>
                                            <td>Exam Title</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $degree_title ?></td>
                                        </tr>
                                        <tr>
                                            <td>Degree Form</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $degree_short_form ?></td>
                                        </tr>
                                        <tr>
                                            <td>Concentration/Major</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $major_subject ?></td>
                                        </tr>
                                        <tr>
                                            <td>Institute</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $institute_name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Passing Year</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $passing_year ?></td>
                                        </tr>
                                    </table>

                                    <!-- Doctor education modal -->
                                    <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="docedumodal_<?= $dedu_id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div id="doctor-form" class="doctor-wrapp">
                                                        <div class="form-outer">
                                                            <h2>Update your educational information</h2>
                                                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                            <hr>
                                                            <div id="doct_notify"></div>
                                                            <form name="" id="" action="" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="ddegree_title">Degree Title<span class="required">*</span></label>
                                                                            <input type="text" name="degree_title" class="form-control" id="degree_title" value="<?= $degree_title ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="degree_form">Degree Short Form<span class="required">*</span></label>
                                                                            <input type="text" name="degree_form" class="form-control" id="degree_form" value="<?= $degree_short_form; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="major_subject">Major Subject<span class="required">*</span></label>
                                                                            <input type="text" name="major_subject" class="form-control" id="major_subject" value="<?= $major_subject ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="institute_name">Institute Name<span class="required">*</span></label>
                                                                            <input type="text" name="institute_name" class="form-control" id="institute_name" value="<?= $institute_name ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="passing_year">Passing Year<span class="required">*</span></label>
                                                                            <input type="text" name="passing_year" class="form-control" id="passing_year" value="<?= $passing_year; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <hr>
                                                                        <input type="hidden" name="docedupdate" value="<?= $dedu_id ?>">
                                                                        <input type="submit" class="doct-submit button btn-primary" value="Update">
                                                                    </div>
                                                                    <div id="erro_notify"></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <br>
                                <a href="#qualification_<?= $did; ?>" data-toggle="modal" class="btn-lg fa fa-plus-circle" title="Add More Qualification"> Add More</a>
                                <br><br>
                                <!-- Doctor education modal -->
                                <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="qualification_<?= $did ?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div id="doctor-form" class="doctor-wrapp">
                                                    <div class="form-outer">
                                                        <h2>Add more educational information</h2>
                                                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                        <hr>
                                                        <div id="doct_notify"></div>
                                                        <form name="" id="" action="" method="post">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="dg_title">Degree Title<span class="required">*</span></label>
                                                                        <input type="text" name="dg_title" class="form-control" id="dg_title">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="dg_form">Degree Short Form<span class="required">*</span></label>
                                                                        <input type="text" name="dg_form" class="form-control" id="dg_form">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="major_sub">Major Subject<span class="required">*</span></label>
                                                                        <input type="text" name="major_sub" class="form-control" id="major_sub">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="int_name">Institute Name<span class="required">*</span></label>
                                                                        <input type="text" name="int_name" class="form-control" id="int_name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="pass_year">Passing Year<span class="required">*</span></label>
                                                                        <input type="text" name="pass_year" class="form-control" id="pass_year">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <hr>
                                                                    <input type="hidden" name="did" value="<?= $did ?>">
                                                                    <input type="submit" class="doct-submit button btn-primary" value="Save">
                                                                </div>
                                                                <div id="erro_notify"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i = 0;
                        if ($doctor_training_info) {
                            ?>
                            <div class="col-xs-12">
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Training Summary</u></h4>
                                <?php
                                while ($doctor_training = $doctor_training_info->fetch_assoc()) {
                                    $i++;
                                    $dtr_id = $doctor_training['dtr_id'];
                                    ?>
                                    <table class="table-hove">
                                        <div class="col-xs-4 col-xs-offset-8">
                                            <a href="#doctrainingmodal_<?= $dtr_id ?>" data-toggle="modal" class="btn-primary btn-lg" title="Edit-Training">
                                                <span class="fa fa-pencil-square-o"></span>
                                            </a>
                                            <form action="" method="post" style="display: inline">
                                                <input type="hidden" name="training-id" value="<?= $dtr_id ?>" />
                                                <button onclick="return confirm('Are you sure you want to delete this item?');" type="submit" name="btn" class="btn-lg btn-danger inline-group" title="Delete-Training" style="margin-left: 20px">
                                                    <span class="fa fa-trash-o"></span>
                                                </button>
                                            </form>
                                        </div>
                                        <tr>
                                            <td><h5><u><?= "Training" . " " . $i; ?></u></h5></td>
                                            <td class="col-sm-1"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Training Title</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_training['training_title'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Institute</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_training['institute_name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_training['address'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Training Topic</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_training['description'] ?></td>
                                        </tr>
                                    </table>
                                    <!-- Doctor training modal -->
                                    <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="doctrainingmodal_<?= $dtr_id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div id="doctor-form" class="doctor-wrapp">
                                                        <div class="form-outer">
                                                            <h2>Update your training information</h2>
                                                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                            <hr>
                                                            <div id="doct_notify"></div>
                                                            <form name="" id="" action="" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="training_title">Training Title<span class="required">*</span></label>
                                                                            <input type="text" name="training_title" class="form-control" id="training_title" value="<?= $doctor_training['training_title'] ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="institute_name">Institute<span class="required">*</span></label>
                                                                            <input type="text" name="institute_name" class="form-control" id="institute_name" value="<?= $doctor_training['institute_name']; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="address">Address<span class="required">*</span></label>
                                                                            <input type="text" name="address" class="form-control" id="address" value="<?= $doctor_training['address'] ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="description">Training Topic<span class="required">*</span></label>
                                                                            <input type="text" name="description" class="form-control" id="description" value="<?= $doctor_training['description'] ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <hr>
                                                                        <input type="hidden" name="doctrainingupdate" value="<?= $dtr_id ?>">
                                                                        <input type="submit" class="doct-submit button btn-primary" value="Update">
                                                                    </div>
                                                                    <div id="erro_notify"></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <br>
                                <a href="#training_<?= $did; ?>" data-toggle="modal" class="btn-lg fa fa-plus-circle" title="Add More Training"> Add More</a>
                                <br><br>
                                <!-- Add more Doctor training modal -->
                                <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="training_<?= $did ?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div id="doctor-form" class="doctor-wrapp">
                                                    <div class="form-outer">
                                                        <h2>Update your training information</h2>
                                                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                        <hr>
                                                        <div id="doct_notify"></div>
                                                        <form name="" id="" action="" method="post">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="training_title">Training Title<span class="required">*</span></label>
                                                                        <input type="text" name="training_title" class="form-control" id="training_title">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="institute_name">Institute<span class="required">*</span></label>
                                                                        <input type="text" name="institute_name" class="form-control" id="institute_name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="address">Address<span class="required">*</span></label>
                                                                        <input type="text" name="address" class="form-control" id="address">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="description">Training Topic<span class="required">*</span></label>
                                                                        <input type="text" name="description" class="form-control" id="description">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <hr>
                                                                    <input type="hidden" name="training_did" value="<?= $did ?>">
                                                                    <input type="submit" class="doct-submit button btn-primary" value="Save">
                                                                </div>
                                                                <div id="erro_notify"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i = 0;
                        if (!empty($doctor_job_history)) {
                            ?>
                            <div class="col-xs-12">
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Experience Summary</u></h4>

                                <?php
                                while ($doctor_job_history_info = $doctor_job_history->fetch_assoc()) {
                                    $i++;
                                    $doc_job_id = $doctor_job_history_info['doc_job_id'];
                                    ?>
                                    <table class="table-hover">

                                        <div class="col-xs-4 col-xs-offset-8">
                                            <a href="#docjobhistorymodal_<?= $doc_job_id ?>" data-toggle="modal" class="btn-primary btn-lg" title="Edit-Job History">
                                                <span class="fa fa-pencil-square-o"></span>
                                            </a>

                                            <form action="" method="post" style="display: inline">
                                                <input type="hidden" name="doc-job-history-id" value="<?= $doc_job_id ?>" />
                                                <button onclick="return confirm('Are you sure you want to delete this item?');" type="submit" name="btn" class="btn-lg btn-danger inline-group" title="Delete-Job History" style="margin-left: 20px">
                                                    <span class="fa fa-trash-o"></span>
                                                </button>
                                            </form>
                                        </div>

                                        <tr>
                                            <td><h5><u><?= "Experience" . " " . $i; ?></u></h5></td>
                                            <td class="col-sm-1"></td>
                                            <td></td>
                                        </tr><tr>
                                            <td>Office Name</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_job_history_info['doc_office_name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Office Address</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_job_history_info['office_address'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Start Date</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_job_history_info['start_date'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>End Date</td>
                                            <td class="col-sm-1">:</td>
                                            <td><?= $doctor_job_history_info['end_date'] ?></td>
                                        </tr>
                                    </table>
                                    <!-- Doctor education modal -->
                                    <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="docjobhistorymodal_<?= $doc_job_id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div id="doctor-form" class="doctor-wrapp">
                                                        <div class="form-outer">
                                                            <h2>Update Your Job History</h2>
                                                            <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                            <hr>
                                                            <div id="doct_notify"></div>
                                                            <form name="" id="" action="" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="doc_office_name">Office Name<span class="required">*</span></label>
                                                                            <input type="text" name="doc_office_name" class="form-control" id="doc_office_name" value="<?= $doctor_job_history_info['doc_office_name'] ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="office_address">Office Address<span class="required">*</span></label>
                                                                            <input type="text" name="office_address" class="form-control" id="office_address" value="<?= $doctor_job_history_info['office_address']; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="start_date">Start Date<span class="required">*</span></label>
                                                                            <input type="text" name="start_date" class="form-control" id="start_date" value="<?= $doctor_job_history_info['start_date'] ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="end_date">End Date<span class="required">*</span></label>
                                                                            <input type="text" name="end_date" class="form-control" id="end_date" value="<?= $doctor_job_history_info['end_date'] ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <hr>
                                                                        <input type="hidden" name="docjobhisupdate" value="<?= $doc_job_id ?>">
                                                                        <input type="submit" class="doct-submit button btn-primary" value="Update">
                                                                    </div>
                                                                    <div id="erro_notify"></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <br>
                                <a href="#job_<?= $did; ?>" data-toggle="modal" class="btn-lg fa fa-plus-circle" title="Add More Job"> Add More</a>
                                <br><br>
                                <!-- Doctor Job History modal -->
                                <div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="job_<?= $did; ?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div id="doctor-form" class="doctor-wrapp">
                                                    <div class="form-outer">
                                                        <h2>Update Your Job History</h2>
                                                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                                                        <hr>
                                                        <div id="doct_notify"></div>
                                                        <form name="" id="" action="" method="post">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="doc_office_name">Office Name<span class="required">*</span></label>
                                                                        <input type="text" name="doc_office_name" class="form-control" id="doc_office_name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="office_address">Office Address<span class="required">*</span></label>
                                                                        <input type="text" name="office_address" class="form-control" id="office_address">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="start_date">Start Date<span class="required">*</span></label>
                                                                        <input type="date" name="start_date" class="form-control" id="start_date">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="end_date">End Date<span class="required">*</span></label>
                                                                        <input type="date" name="end_date" class="form-control" id="end_date">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <hr>
                                                                    <input type="hidden" name="addmorejob" value="<?= $did ?>"> 
                                                                    <input type="submit" class="doct-submit button btn-primary" value="Save">
                                                                </div>
                                                                <div id="erro_notify"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Doctor personal modal -->
<div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="doctormodal_<?= $did; ?>" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div id="doctor-form" class="doctor-wrapp">
                    <div class="form-outer">
                        <h2>Update your personel information</h2>
                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                        <hr>
                        <div id="doct_notify"></div>
                        <form name="" id="" action="" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="required">*</span></label>
                                        <input type="text" name="first_name" class="form-control" id="first_name" value="<?= $doctor_personal_details['first_name']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="required">*</span></label>
                                        <input type="text" name="last_name" class="form-control" id="last_name" value="<?= $doctor_personal_details['last_name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="idnetity_no">Identity Number <span class="required">*</span></label>
                                        <input type="text" name="idnetity_no" class="form-control" id="idnetity_no" value="<?= $doctor_personal_details['idnetity_no'] ?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="nid_passport">NID or Passport Number <span class="required">*</span></label>
                                        <input type="text" name="nid_passport" class="form-control" id="nid_passport" value="<?= $doctor_personal_details['nid_passport'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email Address <span class="required">*</span></label>
                                        <input type="text" name="email" class="form-control" id="email" value="<?= $doctor_personal_details['email'] ?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="mobile_no">Mobile Number <span class="required">*</span></label>
                                        <input type="text" name="mobile_no" class="form-control" id="mobile_no" value="<?= $doctor_personal_details['mobile_no'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="gender">Gender <span class="required">*</span></label>
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="<?= $doctor_personal_details['gender'] ?>"><?= $doctor_personal_details['gender'] ?></option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="birthday">Birth Date <span class="required">*</span></label>
                                        <input type="text" name="birthday" class="form-control" id="birthday" value="<?= $doctor_personal_details['birthday'] ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="present_address">Present Address <span class="required">*</span></label>
                                        <textarea id="present_address" name="present_address" class="form-control" style="resize: none;"><?= $doctor_personal_details['present_address'] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="permanent_address">Permanent Address <span class="required">*</span></label>
                                        <textarea id="permanent_address" name="permanent_address" class="form-control" style="resize: none;"><?= $doctor_personal_details['permanent_address'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="specialists">Specialist<span class="required">*</span></label>
                                        <input type="text" name="specialists" class="form-control" id="specialists" value="<?= $doctor_personal_details['specialists'] ?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="self_about">Self About<span class="required">*</span></label>
                                        <textarea name="self_about" class="form-control" id="self_about" style="resize: none"><?= $doctor_personal_details['self_about'] ?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr>

                                        <button type="submit" class="doct-submit button btn-primary" id="doct_button" name="docproupdate" value="">Update</button>
                                    </div>

                                </div>
                                <div id="erro_notify"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<?php
include '../footer.php';
?>

