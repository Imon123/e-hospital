<?php
include '../header.php';
include './dsession.php';
$doctor_personal_details = $doctor->doctor_personal_details($did);
$doctor_education_info = $doctor->doctor_education_info($did);
$doctor_training_info = $doctor->doctor_training_info($did);
$doctor_job_history = $doctor->doctor_job_history($did);
//$doctor_info = $doctor->doctor_profile($did);
?>
<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 height-control">
                <div class="row">
                    <?php include_once 'important-notification.php';?>
                    <div class="col-xs-12 col-sm-3">
                        <div class="leftsidebar">
                            <ul> 
                                    <li>
                                        <a <?php if ($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="index">My State</a>
                                    </li>
                                    <li>
                                        <a <?php if ($current_page == 'doctor-profile'): echo 'class="menu-active"'; endif; ?> href="doctor-profile">View Profile</a>
                                    </li>
                                    <li>
                                        <a <?php if ($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?> href="edit-profile">Edit Profile</a>
                                    </li>                      
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="mystate clearfix">
                        <?php if(!empty($doctor_personal_details)){ ?>
                        <div class="col-xs-8">
                            <table class="table-hover">
                                <tr>
                                    <td><h1 class="text-bold" style="color: blue; font-weight: bold"><?= $doctor_personal_details['first_name'] . ' ' . $doctor_personal_details['last_name'] ?></h1></td>
                                </tr>
                                <tr>
                                    <td>Address : <?= $doctor_personal_details['present_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Mobile No. : <?= $doctor_personal_details['mobile_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>Email : <?= $doctor_personal_details['email'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-4" style="padding-bottom: 30px">
                            <table class="table-hover table-bordered">
                                <tr>
                                    <td><img src="../<?= $doctor_personal_details['profile_image'] ?>" width="200" height="200"/></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-12">
                            <div style="border-bottom: 2px solid #000;">

                            </div>
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Personal Details</u></h4>
                            <table class="table-hover">
                                <tr>
                                    <td>Identity No</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['idnetity_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>National ID Or Passport No.</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['nid_passport'] ?></td>
                                </tr>
                                <tr>
                                    <td>Birth Date</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['birthday'] ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['gender'] ?></td>
                                </tr>
                                <tr>
                                    <td>Specialists</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['specialists'] ?></td>
                                </tr>
                                <tr>
                                    <td>Present Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['present_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['permanent_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Self About</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_personal_details['self_about'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <?php } 
                                if (!empty($doctor_education_info)) {?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Academic Qualification</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Exam Title</th>
                                    <th class="text-center">Degree Form</th>
                                    <th class="text-center">Concentration/Major</th>
                                    <th class="text-center">Institute</th>
                                    <th class="text-center">Passing Year</th>
                                    <th class="text-center">Result</th>
                                    <th class="text-center">Duration</th>
                                </tr>
                                <?php
                                while ($doctor_education = $doctor_education_info->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td><?= $doctor_education['degree_title'] ?></td>
                                    <td><?= $doctor_education['degree_form'] ?></td>
                                    <td><?= $doctor_education['major_subject'] ?></td>
                                    <td><?= $doctor_education['institute_name'] ?></td>
                                    <td><?= $doctor_education['passing_year'] ?></td>
                                    <td>Passed</td>
                                    <td></td>
                                </tr>
                                <?php }  ?>
                            </table>
                        </div>
                        <?php } 
                                if(!empty($doctor_training_info)){?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Training Summary</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Training Title</th>
                                    <th class="text-center">Institute</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Training Topic</th>
                                </tr>
                                <?php
                                while($doctor_training = $doctor_training_info->fetch_assoc()){
                                ?>
                                <tr>
                                    <td><?= $doctor_training['training_title'] ?></td>
                                    <td><?= $doctor_training['institute_name'] ?></td>
                                    <td><?= $doctor_training['address'] ?></td>
                                    <td><?= $doctor_training['description'] ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <?php }
                                if(!empty($doctor_job_history)){ ?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Experience Summary</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Office Name</th>
                                    <th class="text-center">Office Address</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End Date</th>
                                </tr>
                                <?php
                                while($doctor_job_history_info = $doctor_job_history->fetch_assoc()){
                                ?>
                                <tr>
                                    <td><?= $doctor_job_history_info['doc_office_name'] ?></td>
                                    <td><?= $doctor_job_history_info['office_address'] ?></td>
                                    <td><?= $doctor_job_history_info['start_date'] ?></td>
                                    <td><?= $doctor_job_history_info['end_date'] ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include '../footer.php';
?>

