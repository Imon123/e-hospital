<?php
include '../header.php';
include 'dsession.php';

$dinfo = $doctor->get_doctor_allinfo_by_id($did);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['dquery'])) {
    $patient->conversation_doctor_to_patient($_FILES, $_POST);
}elseif($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['prescribe'])){
    $doctor->send_prescribe_doctor_to_patient($_POST);
}
?>
<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="leftsidebar">
                            <ul> 
                                <li>
                                    <a <?php if ($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="<?= $fm->base_url().'doctor-panel'?>">My State</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'doctor-profile'): echo 'class="menu-active"'; endif; ?> href="doctor-profile">View Profile</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?> href="edit-profile">Edit Profile</a>
                                </li>                      
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="mystate">
                            <div id="dup_message"></div>
                            <h2>Appointment Details</h2>
                            <hr>
                            <?php 
                            if(isset($_GET['ap-id']) && $_GET['ap-id'] != null){
                                $result = $doctor->latest_appointment_list($_GET['ap-id']);
                                $mresult = $doctor->get_new_message_from_patient($_GET['ap-id']);
                            }
                                if($result):
                                while ( $arow = $result->fetch_assoc()) {
                            ?>
                            <div class="app-details">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="p-image">
                                            <?php if(!empty($arow['profile_img'])){ ?>
                                            <img class="preview_photo" src="../<?= $arow['profile_img'] ?>" width="150" height="150"/>
                                            <?php }else{ ?>
                                            <img src="../images/avatar-default.png" class="img-responsive" alt="avatar default">
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <h3>Patient Personal Info.</h3>
                                        <table class="table table-hover" style="wi50%">
                                            <tr>
                                                <td>Full Name </td>
                                                <td>:</td>
                                                <td><?= $arow['first_name'] . ' ' . $arow['last_name']; ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Height </td>
                                                <td>:</td>
                                                <td><?= $arow['height'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Weight</td>
                                                <td>:</td>
                                                <td><?= $arow['weight'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Blood Group</td>
                                                <td>:</td>
                                                <td><?= $arow['blood_group'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td>Gender </td>
                                                <td>:</td>
                                                <td><?= $arow['gender'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Age</td>
                                                <td>:</td>
                                                <td><?= $arow['birthday'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td>:</td>
                                                <td><?= $arow['address'] ?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2>Patient Problem</h2>
                                        <hr>
                                        <div class="spacific-problem"><span><b>Send Spacific Problem to Patient:</b>
                                        <span><?= $fm->getDate($arow['created_at'], 'jS M Y,  g:i:s'); ?></span>
                                        </span> <p><?= $arow['problem']; ?></p></div>
                                        <?php if(!empty($arow['problem_image'])): ?>
                                        <div class="image-gellary">
                                            <?php
                                             $array =  explode(',', $arow['problem_image']);
                                             foreach ($array as $value) {
                                                $value = str_replace(' ','',$value);
                                                echo '<a rel="example_group" href="../'.$value.'" title=""><img alt="" src="../'.$value.'"  width="150" height="150"/></a>';
                                              } 
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(!empty($arow['audio_video'])): ?>
                                    <div class="audio-message">
                                        <?php 
                                            $audio_type     = array( 'mp3', 'ogg', 'wav');
                                            $audio_divided = explode('.', $arow['audio_video']);
                                            $audio_ext      = strtolower( end($audio_divided) );
                                            if( in_array( $audio_ext, $audio_type) === TRUE ){
                                        ?>
                                        <audio controls>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/mpeg"/>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/ogg"/>
                                          <source src="../<?= $arow['audio_video']; ?>" type="audio/wav"/>
                                            Your browser does not support the video tag.
                                        </audio>
                                        <?php }else{ ?>
                                            <video width="320" height="240" controls>
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/mp4">
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/ogg">
                                              <source src="../<?= $arow['audio_video']; ?>" type="video/webm">
                                            Your browser does not support the video tag.
                                            </video>
                                        <?php } ?>
                                    </div>
                                    <?php endif; ?>
                                    <hr>
                                    </div>
                                </div>
                            <?php if($mresult){ ?>
                            <div class="row">
                                <?php while ($mrow = $mresult->fetch_assoc()) {?>
                                <div class="col-xs-12 bottom-border">
                                    <div class="spacific-problem"><span><b>Reply to <?= $mrow['sender']?>:</b>
                                    <span><?= $fm->getDate($mrow['create_message'], 'jS M Y,  g:i:s'); ?></span>
                                    </span> <p><?= $mrow['message']; ?></p></div>
                                    <?php if(!empty($mrow['image'])): ?>
                                    <div class="image-gellary">
                                        <?php
                                         $array =  explode(',', $mrow['image']);
                                         foreach ($array as $value) {
                                            $value = str_replace(' ','',$value);
                                            echo '<a rel="example_group" href="'.$value.'" title=""><img alt="" src="'.$value.'"  width="150" height="150"/></a>';
                                          } 
                                        ?>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(!empty($mrow['media'])): ?>
                                    <div class="audio-message">
                                        <?php 
                                            $audio_type     = array( 'mp3', 'ogg', 'wav');
                                            $audio_divided = explode('.', $mrow['media']);
                                            $audio_ext      = strtolower( end($audio_divided) );
                                            if( in_array( $audio_ext, $audio_type) === TRUE ){
                                        ?>
                                        <audio controls>
                                          <source src="<?= $mrow['media']; ?>" type="audio/mpeg"/>
                                          <source src="<?= $mrow['media']; ?>" type="audio/ogg"/>
                                          <source src="<?= $mrow['media']; ?>" type="audio/wav"/>
                                            Your browser does not support the video tag.
                                        </audio>
                                        <?php }else{ ?>
                                            <video width="320" height="240" controls>
                                              <source src="<?= $mrow['media']; ?>" type="video/mp4">
                                              <source src="<?= $mrow['media']; ?>" type="video/ogg">
                                              <source src="<?= $mrow['media']; ?>" type="video/webm">
                                            Your browser does not support the video tag.
                                            </video>
                                        <?php } ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div id="bottom_scroll"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                   <div class="accordion">
                                        <div class="accordion-section">
                                            <a class="accordion-section-title" href="#accordion-1">Send Message to Patient</a>
                                            <div id="accordion-1" class="accordion-section-content">
                                            <form name="" id="" action="" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="appoint_id" value="<?= $arow['appoint_id']; ?>">
                                                <input type="hidden" name="sender" value="Doctor">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <label for="doc_office_name">Message</label>
                                                            <textarea name="message" style="resize: none;" cols="6" rows="6" class="required form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="form-group row">
                                                            <label for="inputEmail3" class="col-sm-3">Audio or Video</label>
                                                            <div class="col-sm-9">
                                                                <input name="audiovideo" type="file" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group row">
                                                            <hr>
                                                            <div class="col-sm-12">
                                                                <input type="submit"  id="upload" class="upload doct-submit button btn-primary" name="dquery" value="Send">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div><!--end .accordion-section-content-->
                                        </div><!--end .accordion-section-->

                                        <div class="accordion-section">
                                            <a class="accordion-section-title" href="#accordion-2">Prescription</a>
                                            <div id="accordion-2" class="accordion-section-content">
                                            <form name="" id="" action="" method="post">
                                                <input type="hidden" name="appoint_id" value="<?= $arow['appoint_id']; ?>">
                                                <input type="hidden" name="did" value="<?= $arow['did']; ?>">
                                                <input type="hidden" name="pid" value="<?= $arow['ptid']; ?>">
                                                <div class="row">
                                                    <div class="col-xs-3 ">
                                                        <div class="form-group">
                                                            <label for="doc_office_name">Deseise(optionl)</label>
                                                            <textarea name="deseise" style="resize: none;" cols="6" rows="6" class="required form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="form-group">
                                                            <label for="doc_office_name">Medicine <span class="required">*</span></label>
                                                            <textarea name="medicine"  style="resize: none;" cols="6" rows="6" class="required form-control" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group row">
                                                            <hr>
                                                            <div class="col-sm-12">
                                                                <input type="submit"  id="upload" class="upload doct-submit button btn-primary" name="prescribe" value="Send">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div><!--end .accordion-section-content-->
                                        </div><!--end .accordion-section-->

                                        <div class="accordion-section">
                                            <a class="accordion-section-title" href="#accordion-3">Test</a>
                                            <div id="accordion-3" class="accordion-section-content">
                                                <p>Mauris interdum fringilla augue vitae tincidunt. Curabitur vitae tortor id eros euismod ultrices. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent nulla mi, rutrum ut feugiat at, vestibulum ut neque? Cras tincidunt enim vel aliquet facilisis. Duis congue ullamcorper vehicula. Proin nunc lacus, semper sit amet elit sit amet, aliquet pulvinar erat. Nunc pretium quis sapien eu rhoncus. Suspendisse ornare gravida mi, et placerat tellus tempor vitae.</p>
                                            </div><!--end .accordion-section-content-->
                                        </div><!--end .accordion-section-->
                                    </div><!--end .accordion-->
                                </div>
                            </div> 
                            <?php   
                                }
                                else: 

                                endif; 
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include '../footer.php'; ?>