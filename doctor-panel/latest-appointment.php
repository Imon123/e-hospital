<?php
include '../header.php';
include 'dsession.php';

$dinfo = $doctor->get_doctor_allinfo_by_id($did);
?>

<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="leftsidebar">
                            <ul> 
                                <li>
                                    <a <?php if ($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="<?= $fm->base_url().'doctor-panel'?>">My State</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'doctor-profile'): echo 'class="menu-active"'; endif; ?> href="doctor-profile">View Profile</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?> href="edit-profile">Edit Profile</a>
                                </li>                      
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="mystate">
                            <h2>Latest Appointment List</h2>
                            <div id="get_appointemnt">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include '../footer.php'; ?>   
