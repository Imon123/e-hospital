<?php 
    include '../header.php';
    include 'dsession.php';
 ?>
 <section class="patient-area sec-pdd1">
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <?php include 'sidebar.php'; ?>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="dupdate-profile">
                <div class="profile-image">
                <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" id="d_id" value="<?php echo $did; ?>">
                <div id="image_preview"><img id="previewing" src="noimage.png" /></div>
                <div id="selectImage">
                <input type="file" name="file" id="file"/>
                <div class="message-wrrp"><div id="message"></div></div>
                <div class="photo-button">
                <span id='loading' ><img src="../images/loading_circle.gif"></span>
                <input type="submit" value="Upload Photo" class="submit" />
                <input type="submit" value="Cancel" class="submit" />
                </div>
                </div>
                </form>
                
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php 
    include '../footer.php';
 ?>