<?php 
include './dsession.php';
    $msg = array();
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['dpersonal_info'])){
        $doctor->update_doctor_info($_POST, $did);
        $dinfo = $doctor->get_doctor_allinfo_by_id($did);
    }
?>
<div class="row">
    <div class="col-xs-12 col-md-3">
        <?php include 'sidebar.php'; ?>
    </div>
    <div class="col-xs-12 col-md-9">
        <div class="dupdate-profile">
                <form action="" method="post" id="dpersonal-info">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="first_name">First Name <span class="required">*</span></label>
                                <input type="text" name="first_name" class="form-control" id="first_name" value="<?= $dinfo['first_name']; ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="last_name">Last Name <span class="required">*</span></label>
                                <input type="text" name="last_name" class="form-control" id="last_name" value="<?= $dinfo['last_name']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="idnetity_no">Identity Number <span class="required">*</span></label>
                                <input type="text" name="idnetity_no" class="form-control" id="idnetity_no" value="<?= $dinfo['idnetity_no']; ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="nid_passport">NID or Passport Number <span class="required">*</span></label>
                                <input type="text" name="nid_passport" class="form-control" id="nid_passport" value="<?= $dinfo['nid_passport']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="email">Email Address <span class="required">*</span></label>
                                <input type="text" name="email" class="form-control" id="email" value="<?= $dinfo['email']; ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="mobile_no">Mobile Number <span class="required">*</span></label>
                                <input type="text" name="mobile_no" class="form-control" id="mobile_no" value="<?= $dinfo['mobile_no']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="gender">Gender <span class="required">*</span></label>
                                <select name="gender" class="form-control" id="gender">
                                    <option value="">Choose Gender </option>
                                    <option <?php
                                    if ($dinfo['gender'] == 'Male'): echo 'selected';
                                    endif;
                                    ?> value="Male">Male</option>
                                    <option <?php
                                    if ($dinfo['gender'] == 'Female'): echo 'selected';
                                    endif;
                                    ?> value="Female">Female</option>
                                    <option <?php
                                    if ($dinfo['gender'] == 'Others'): echo 'selected';
                                    endif;
                                    ?> value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="birthday">Birth Date <span class="required">*</span></label>
                                <input type="text" name="birthday" class="form-control" id="birthday" value="<?= $dinfo['birthday']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="specialists">Specialists <span class="required">*</span></label>
                                <input type="text" name="specialists" class="form-control" id="specialists" value="<?= $dinfo['specialists']; ?>">
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="self_about">About You (Optional)</label>
                                <textarea class="form-control" id="self_about" name="self_about" style="resize: none"><?= $dinfo['self_about']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Address <span class="required">*</span></h3>
                            <hr>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="present_address">Presant Address</label>
                                <textarea class="form-control" id="present_address" name="present_address" style="resize: none"><?= $dinfo['present_address']; ?></textarea>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="permanent_address">Permanent Address</label>
                                <textarea class="form-control" id="permanent_address" name="permanent_address" style="resize: none"><?= $dinfo['permanent_address']; ?></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <hr>
                            <button type="submit" class="doct-submit button btn-primary" id="doct_button" name="dpersonal_info" value="ptbutton">Update</button>
                        </div>

                    </div>
                </form>
        </div>
    </div>

</div>