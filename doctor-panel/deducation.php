<?php 
    include '../header.php';
    include 'dsession.php';
 ?>
 <section class="patient-area sec-pdd1">
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
          <?php include 'sidebar.php'; ?>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="dupdate-profile">
                <div id="status" style="display: none;"></div>
                <div id="display_dedu"></div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- deducation modal -->
<div class="modal fade education-modal" data-keyboard="false" data-backdrop="static" id="educationmodal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                <div class="result"></div>
                <form>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group">
                                <label for="degree_title">Degree Title <span class="required">*</span></label>
                                <input type="hidden" id="deduid" value="<?php echo $did; ?>">
                                <input type="text" class="form-control" id="degree_title">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="degree_form">Degree Short Form <span class="required">*</span></label>
                                <input type="text" class="form-control" id="degree_form">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="institute_name">Institute Name <span class="required">*</span></label>
                                <input type="text" class="form-control" id="institute_name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="passing_year">Passing Year <span class="required">*</span></label>
                                <input type="text" class="form-control" id="passing_year">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="major_subject">Major/Group <span class="required">*</span></label>
                                <input type="text" class="form-control" id="major_subject">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr>
                            <button type="submit" onclick="dsaveData()" class="doct-submit button btn-primary">Save</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


</script>
<?php 
    include '../footer.php';
 ?>