<?php 
	$path = realpath(dirname(__FILE__));
	include_once($path.'/../lib/Session.php');
	Session::check_login();

	include '../controllers/Admin_Login.php';
	$admin_login = new Admin_Login();
?>
<?php 
	$msg = array();
	if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
		$msg = $admin_login->author_login($_POST);
	}



?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login Authentication</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets/css/login.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 
</head>
<body style="min-height: 500px;">
	<div id='login'>
		<!--<h1>World University of<span class='orangestop'>Bangladesh</span></h1>-->
		<h1 class="sitelogo">Telemedicine</h1>
		<?php 
			if(isset($msg['error'])){
				printf('<div class="error-area"><span class="error">%s</span></div>', $msg['error']);
			}
		?>
		<form action="" method="post">
		  <span class='input'>
			<span class='icon username-icon fontawesome-user'></span>
			<input type='text' name="username" class='username' placeholder='Enter your email or username'>
		 </span>
		  <span class='input'>
			<span class='password-icon-style icon password-icon fontawesome-lock'></span>
			<input type='password' name="password" class='password' placeholder='Password'>
		  </span>
		  <div class='divider'></div>
		  <button type="submit" name="login">LOG IN</button>
		</form>
		<div class='forgot'><a href="#">Forgot Password?</a></div>
		<div class='divider'></div>
		<p class="backtosite"><a href="">Back to <span class='reg'>Site</span></a></p>
	</div>
<script src="assets/js/index.js"></script>
</body>
</html>