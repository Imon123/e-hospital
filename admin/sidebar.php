<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="active treeview">
                <a href="dashboard">
                    <i class="fa fa-dashboard"></i> <span> Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span> Doctor Manage</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="doctor-list"><i class="fa fa-circle-o"></i> Doctor List</a></li>
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span> Patient Manage </span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="patient-list"><i class="fa fa-circle-o"></i> Patient List</a></li>
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span> Appointment Manage </span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="appointment-list"><i class="fa fa-circle-o"></i> Latest Appointment List</a></li>
                    <li><a href="approved-appointment-list"><i class="fa fa-circle-o"></i> Approved Appointment List</a></li>
                </ul>
            </li>
            <li class="active treeview">
                <a href="patient-payment-list">
                    <i class="fa fa-dashboard"></i> <span> Payment Manage </span> 
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
