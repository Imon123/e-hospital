<?php
include_once '../lib/Session.php';
Session::session_check();

include '../controllers/Admin_Login.php';
include '../controllers/Admin_Manage.php';
include '../controllers/Doctor.php';
//include '../controllers/Patient.php';
$admin_login = new Admin_Login();
$admin_mg = new Admin_Manage();
$doctor = new Doctor();
//$patient = new Patient();

if (isset($_GET['logout']) && $_GET['logout'] == 'logout'):
    $admin_login->logout();
endif;
//
$count = $admin_mg->notification_count();
$notifications = $admin_mg->notifications();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Telemedicine</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <script src="assets/js/jquery-min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" type="text/css" href="assets/js/datatable.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome-min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="assets/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

        <link rel="stylesheet" href="../css/admin.css">


        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/app.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/select2.min.css">
        <link rel="stylesheet" href="../css/datepicker3.css">
        <link rel="stylesheet" type="text/css" href="../fonts/fonts.css"/>	
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/FontAwesome.otf"/>
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/fontawesome-webfont.eot"/>
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/fontawesome-webfont.svg"/>
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/fontawesome-webfont.ttf"/>
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/fontawesome-webfont.woff"/>	
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/fontawesome-webfont.woff2"/>	
        <link rel="stylesheet" type="text/css" href="../style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/responsive.css"/>
        <style type="text/css">
            .content-wrapper {
                min-height: 520px;
            }
        </style>
        <!--
            <link href="../tables/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
            <link href="../tables/datatables-responsive/dataTables.responsive.css" rel="stylesheet">-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="dashboard" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="../images/avatar-default.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo Session::session_get('fullname'); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="../images/avatar-default.png" class="img-circle" alt="User Image">
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="?logout=logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>

                </nav>
            </header>