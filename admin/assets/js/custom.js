$(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group col-sm-3"><label for="publication__'+x+'">Publication_'+x+'</label><input type="text" class="form-control custom-field" name="publication_'+x+'"/><a href="#" class="remove_field">X</a></div>'); //add input box
        }else if(x == max_fields){
          alert("You can add only 10 publications. Thank you!!");
        }
    }); 
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    
    //Date picker
    $('#joining_date').datepicker({
      autoclose: true
    });
    $('#birth_date').datepicker({
      autoclose: true
    });
    $('#from_date').datepicker({
      autoclose: true
    });
    $('#to_date').datepicker({
      autoclose: true
    });
});

function deleteRecorcd(){

    return confirm('Are you sure to delete at this record!!');
}

// multiple field auto calculation student receipt money

$(function(){
$(".cal").each(function(){
  $(this).on('keyup', function(){
    calculateSum();
  });
});
});

function calculateSum(){
var sum = 0;
$(".cal").each(function(){
  if(!isNaN(this.value) && this.value.length != 0){
    sum +=parseFloat(this.value);
  }
});
$("#sum").html(sum.toFixed(2));
}


// Student money receipt dependable dropbdown list

$(function(){
    $('#department').on('change',function(){
        var programID = $(this).val();
        if(programID){
            $.ajax({
                type:'POST',
                url:'ajax_response.php',
                data:'programID='+programID,
                success:function(html){
                    
                    $('#batch').html(html);
                    $('#registration_id').html('<option value="">Select RegistrationID</option>'); 
                }
            }); 
        }else{
            $('#batch').html('<option value="">Select Batch</option>');
            $('#registration_id').html('<option value="">Select RegistrationID</option>'); 
        }
    });
    
    $('#batch').on('change',function(){
        var batchID = $(this).val();
        if(batchID){
          //alert(batchID);
            $.ajax({
                type:'POST',
                url:'ajax_response.php',
                data:'batchID='+batchID,
                success:function(html){
                    $('#registration_id').html(html);
                    $('#view_reg_id').html(html);
                    $('#st_reg_id').html(html);
                }
            }); 
        }else{
            $('#registration_id').html('<option value="">Select state first</option>'); 
        }
    });

    $('#registration_id').on('change',function(){
        var regID = $(this).val();
        if(regID){
          //alert(regID);
            $.ajax({
                type:'POST',
                url:'ajax_response.php',
                data:'regID='+regID,
                success:function(html){
                    $('#student_name').html(html);
                }
            }); 
        }else{
            $('#student_name').html(''); 
        }
    });
});

// Student money receipt view

$(function(){
    $('#view_reg_id').on('change',function(){
        var viewRegID = $(this).val();
        if(viewRegID){
            $.ajax({
                type:'POST',
                url:'ajax_response.php',
                data:'view_reg_id='+viewRegID,
                success:function(html){
                    $('#student_fee_details').html(html);
                }
            }); 
        }else{
            $('#student_fee_details').html(''); 
        }
    });
});

// student admission information

jQuery(document).ready(function($){
    $('#st_reg_id').on('change',function(){
        var viewRegID = $(this).val();
        if(viewRegID){
            $.ajax({
                type:'POST',
                url:'ajax_response.php',
                data:'st_reg_id='+viewRegID,
                success:function(html){
                    $('#student_info_details').html(html);
                }
            }); 
        }else{
            $('#student_info_details').html(''); 
        }
    });
});

//student_info_delete

function ActionFunction(urls, empids, work){
    
    if (work == "delete") {
     
     var result = confirm("Are you sure you want to delete?");
      if (result == true) {
    var spancontainer=$('table tr#record'+empids);
    $.ajax({
        url: urls,
        async: true,
        cache: false,
        data: {empid: empids},
        type: 'get',
        beforeSend: function() {
                spancontainer.animate({'backgroundColor':'#fb6c6c'},300);
        },            
        success: function (data) {
           data=data.replace(/\\s+/g,"");
           
           if(data != 0){
                spancontainer.slideUp(800,function() {
                    spancontainer.remove();
                });
            
            }
            else {
                spancontainer.slideUp('slow', function(){       
                spancontainer.html("Error While this deleting a record");
                });
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

      }
     
    }   
}

// student print invoice page

function printDiv(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}

//student_admission_receipt_delete

function DeleteFunction(urls, empids, del){
    if (del == "delete") {
     var result = confirm("Are you sure you want to delete?");
      if (result == true) {
    var spancontainer=$('table tr#record'+empids);
    $.ajax({
        url: urls,
        async: true,
        cache: false,
        data: {delid: empids},
        type: 'get',
        beforeSend: function() {
                spancontainer.animate({'backgroundColor':'#fb6c6c'},300);
        },            
        success: function (data) {
           data=data.replace(/\\s+/g,"");
           
           if(data != 0){
                spancontainer.slideUp(800,function() {
                    spancontainer.remove();
                });
            
            }
            else {
                spancontainer.slideUp('slow', function(){       
                spancontainer.html("Error While this deleting a record");
                });
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

      }
     
    }   
}
