<?php
include 'header.php';
include 'sidebar.php';
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['patient_id'])) {
    $patient_id = $_GET['patient_id'];
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
	$appointment_history = $admin_mg->appointment_history($patient_id);
}

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['new_user'])) {
    $patient_id = $_POST['new_user'];
    $admin_mg->read_notification($patient_id);
    $count = $admin_mg->notification_count();
	$notifications = $admin_mg->notifications();
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['approved_him'])){
    $approved_him = $_POST['approved_him'];
    $admin_mg->approve_this_appointment($approved_him);    
	$patient_id = $_GET['patient_id'];
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
	$appointment_history = $admin_mg->appointment_history($patient_id);
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['unapprove'])){
    $unapproved_him = $_POST['unapprove'];
    $admin_mg->unapprove_this_appointment($unapproved_him);
	$patient_id = $_GET['patient_id'];
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
	$appointment_history = $admin_mg->appointment_history($patient_id);
}

?>

<section class="content-wrapper">
    <div class="container" style="padding: 50px 0px">
        <div class="row"> 
            <div class="col-xs-12 height-control">
                <div class="row">

                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">						
                        <?php if (!empty($patient_details_by_id)) { ?>
                        <div class="col-xs-4 col-sm-offset-8" style="padding-bottom: 30px">
                            <table class="table-hover table-bordered">
                                <tr>
                                    <td><img src="../<?= $patient_details_by_id['profile_img'] ?>" width="200" height="200"/></td>
                                </tr>
                            </table>
                        </div>
						
                            <div class="col-xs-12">
                                <div style="border-bottom: 2px solid #000;">

                                </div>
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Patient Personal Details</u></h4>
                                <table class="table-hover">
                                    <tr>
                                        <td>Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['first_name'] . " " . $patient_details_by_id['last_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Father Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['father_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mother Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['mother_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email Address</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No.</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['mobile_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Height</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['height'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Weight</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['weight'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['blood_group'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>NID </td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['nid'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['gender'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['address'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Age</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['birthday'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Account Type</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['account_type'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Expire Date</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['expire_date'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Active Status</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?php if ($patient_details_by_id['active_status'] == 0) { ?> Not Active
                                            <?php } else { ?>
                                                Active<?php } ?></td>
                                    </tr>
                                </table>
                            </div>
						<div class="col-xs-12" style="margin-top: 30px;">
						
                                <div style="border-bottom: 2px solid #000;">

                                </div>
						<?php if(!empty($appointment_history)){
						?>
							<h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Patient Appointment History</u></h4>
                            <table width="100%" class="text-center table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Sl. No.</th>
										<th>Dr Name</th>
										<th>Dr Speciality</th>
										<th>Appoint Date</th>
										<th>Patient Problem</th>
										<th colspan="">Action</th>
									</tr>
								</thead>
								<?php ?>
								<tbody>
								<?php
									$i = 0;
									while($patient_appt_history = $appointment_history->fetch_assoc()){
										$i++;
								?>
									<tr>
										<td><?= $i?></td>
										<td>
											<form action="view-doctor-profile" method="get">
												<input type="hidden" name="doctor_id" value="<?=$patient_appt_history['did']?>"/>
												<input type="hidden" name="doctor_id" value="<?=$patient_appt_history['did']?>"/>
												<button style="color: #3c8dbc;"><?= $patient_appt_history['first_name']." ".$patient_appt_history['last_name']?></button>
											</form>
										</td>
										<td><?= $patient_appt_history['specialists']?></td>
										<td><?= $patient_appt_history['created_at']?></td>
										<td><?= $patient_appt_history['problem']?></td>
										<td>
											<form action="appointment-details" method="get" style="display: inline;">
												<input type="hidden" name="appoint_id" value="<?=$patient_appt_history['appoint_id']?>"/>
												<button class="btn-info btn-sm fa fa-eye" title="View Appointment Details"></button>
											</form>
											
											<form action="" method="post" style="display: inline;">
												<?php
													if($patient_appt_history['appoint_approval_status'] == 1){?>
													<input type="hidden" name="unapprove" value="<?= $patient_appt_history['appoint_id'] ?>">
													<button class="btn-primary fa fa-check-circle btn-sm btn-group" title="This Is Approved"></button>
													<?php }else{ ?>
													<input type="hidden" name="approved_him" value="<?= $patient_appt_history['appoint_id'] ?>">
														<button class="btn-warning fa fa-exclamation-triangle btn-sm btn-group" title="Please-Approve-Appointment"></button>
												  <?php  }
												?>
											</form>
										</td>
									<tr>
									<?php }?>
								</tbody>
                            </table>
							<?php  }else{echo "This patient does not create any appointment.";}?>
                        </div>
						
						
                        <?php } ?>
						
                    </div>
					
					
					
					
					
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>