<?php
include 'header.php';
include 'sidebar.php';

$appointment_list = $admin_mg->appointment_list();

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete_appointment'])){
    $appoint_id = $_POST['delete_appointment'];
    $admin_mg->delete_appointment($appoint_id);
	$appointment_list = $admin_mg->appointment_list();
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['approved_him'])){
    $approved_him = $_POST['approved_him'];
    $admin_mg->approve_this_appointment($approved_him);
	$appointment_list = $admin_mg->appointment_list();
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['unapprove'])){
    $unapproved_him = $_POST['unapprove'];
    $admin_mg->unapprove_this_appointment($unapproved_him);
	$appointment_list = $admin_mg->appointment_list();
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 500px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Appointment List
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="text-center table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Sl. No.</th>
                                <th>Patient Name</th>
                                <th>Doctor Name</th>
                                <th>Doctor Specialists</th>
                                <th>Image</th>
                                <th colspan="">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
						if(!empty($appointment_list)){
                        while ($all_appointments = $appointment_list->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $i; ?></td>
                                <td> <?= $all_appointments['pt_fname'] . " " . $all_appointments['pt_lname']; ?> </td>
                                <td><?= $all_appointments['first_name'] . " " . $all_appointments['last_name'];?></td>
                                <td><?= ucfirst($all_appointments['specialists']); ?></td>
                                <td><img src="../<?= $all_appointments['profile_img']; ?>" height="70" width="65"></td>
                                <td>
                                    <form action="appointment-details" method="get" style="display: inline;">
                                        <input type="hidden" name="appoint_id" value="<?= $all_appointments['appoint_id'] ?>">
                                        <button class="btn-info fa fa-eye btn-group btn-lg" title="View Appointment Details"></button>
                                    </form>
                                    <form action="" method="post" style="display: inline;">
                                        <?php
                                            if($all_appointments['appoint_approval_status'] == 1){?>
											<input type="hidden" name="unapprove" value="<?= $all_appointments['appoint_id'] ?>">
                                            <button class="btn-primary fa fa-check-circle btn-group btn-lg" title="This Is Approved"></button>
                                            <?php }else{ ?>
                                            <input type="hidden" name="approved_him" value="<?= $all_appointments['appoint_id'] ?>">
                                                <button class="btn-warning fa fa-exclamation-triangle btn-group btn-lg" title="Please-Approve-Appointment"></button>
                                          <?php  }
                                        ?>
                                    </form>
                                    <form action="" method="post" style="display: inline;">
                                        <input type="hidden" name="delete_appointment" value="<?= $all_appointments['appoint_id'] ?>">
                                        <button class="btn-danger fa fa-trash-o btn-group btn-lg" title="Delete-appointment" onclick="return confirm('Are you sure you want to delete this item?');"></button>
                                    </form>
                                </td>
                            </tr>
                        <?php } }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>

