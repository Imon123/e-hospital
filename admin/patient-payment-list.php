<?php
include 'header.php';
include 'sidebar.php';
$paid_patients = $admin_mg->patient_payment_list();
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete_patient'])) {
    $delete_patient = $_POST['delete_patient'];
    $admin_mg->delete_patient($delete_patient);
    $paid_patients = $admin_mg->patient_payment_list();
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['verify'])) {
    $payment_id = $_POST['verify'];
    $admin_mg->payment_verify($payment_id, $_POST);
    $paid_patients = $admin_mg->patient_payment_list();
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 500px">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Paid Patient List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="text-center table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Sl. No.</th>
                                    <th>Name</th>
                                    <th>Transaction ID</th>
                                    <th>Amount</th>
                                    <th colspan="">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                while ($paid_patients_list = $paid_patients->fetch_assoc()) {
                                    $i++;
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?= $i; ?></td>
                                        <td><?= $paid_patients_list['first_name'] . " " . $paid_patients_list['last_name']; ?></td>
                                        <td><?= $paid_patients_list['transection_id']; ?></td>
                                        <td>BDT <?= $paid_patients_list['amount']; ?> TK</td>
                                        <td>
                                            <form action="patient-transaction-history" method="get" style="display: inline;">
                                                <?php if ($paid_patients_list['account_type'] == "Premium") { ?>
                                                    <input type="hidden" name="patient_id" value="<?= $paid_patients_list['pt_id'] ?>">
                                                    <button class="btn-primary fa fa-usd btn-group btn-lg" title="Transaction-history"></button>
                                                <?php } ?>
                                            </form>
                                            <form action="view-patient-profile" method="get" style="display: inline;">
                                                <input type="hidden" name="patient_id" value="<?= $paid_patients_list['pt_id'] ?>">
                                                <button class="btn-info fa fa-eye btn-group btn-lg" title="View patient profile"></button>
                                            </form>
                                            <form action="" method="post" style="display: inline;">
                                                <?php if ($paid_patients_list['payment_approve_status'] == 1) { ?>
                                                    <span class="btn-primary fa fa-check-square-o btn-group btn-lg" title="Payment-Verified"></span>
                                                <?php } else { ?>
                                                    <input type="hidden" name="verify" value="<?= $paid_patients_list['id'] ?>">
                                                    <?php
                                                    if ($paid_patients_list['package_day'] == "1 year") {
                                                        ?> 
                                                        <input type="hidden" name="expire_date" value="<?= 365 ?>">
                                                    <?php } elseif ($paid_patients_list['package_day'] == "6 months") { ?>
                                                        <input type="hidden" name="expire_date" value="<?= 182 ?>">
                                                    <?php } ?>
                                                    <input type="hidden" name="verify" value="<?= $paid_patients_list['id'] ?>">
                                                    <input type="hidden" name="pt_id" value="<?= $paid_patients_list['pt_id'] ?>">
                                                    <button class="btn-warning fa fa-exclamation-triangle btn-group btn-lg" title="Not-Verified"></button>
                                                <?php }
                                                ?>
                                            </form>
                                            <form action="" method="post" style="display: inline;">
                                                <input type="hidden" name="delete_patient" value="<?= $paid_patients_list['pt_id'] ?>">
                                                <button class="btn-danger fa fa-trash-o btn-group btn-lg" title="Delete-patient" onclick="return confirm('Are you sure you want to delete this item?');"></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>