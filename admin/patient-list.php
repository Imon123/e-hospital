<?php
include 'header.php';
include 'sidebar.php';
$patients = $admin_mg->patient_list();
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete_patient'])) {
    $delete_patient = $_POST['delete_patient'];
    $admin_mg->delete_patient($delete_patient);
    $patients = $admin_mg->patient_list();
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Patient List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="text-center table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Sl. No.</th>
                                    <th>Name</th>
                                    <th>Account Type</th>
                                    <th>Image</th>
                                    <th colspan="">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($patients){
                                $i = 0;
                                while ($patients_list = $patients->fetch_assoc()) {
                                    $i++;
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?= $i; ?></td>
                                        <td><?= $patients_list['first_name'] . " " . $patients_list['last_name']; ?></td>
                                        <td><?= $patients_list['account_type']; ?></td>
                                        <td><img src="../<?= $patients_list['profile_img']; ?>" height="70" width="65"></td>
                                        <td>
                                            <form action="view-patient-profile" method="get" style="display: inline;">
                                                <input type="hidden" name="patient_id" value="<?= $patients_list['pt_id'] ?>">
                                                <button class="btn-info fa fa-eye btn-group btn-lg" title="View patient profile"></button>
                                            </form>
                                            <form action="" method="post" style="display: inline;">
                                                <?php if ($patients_list['account_type'] == "Premium") { ?>
                                                    <span class="btn-primary fa fa-check-square-o btn-group btn-lg" title="Payment-Verified"></span>
                                                <?php } elseif ($patients_list['account_type'] == "Free Trial") { ?>
                                                    <input type="hidden" name="upgrade" value="<?= $patient_details_by_id['id'] ?>">
                                                    <button class="btn-warning fa fa-exclamation-triangle btn-group btn-lg" title="Free Trial"></button>
                                                <?php } else { ?>
                                                    <input type="hidden" name="verify" value="<?= $patient_details_by_id['id'] ?>">
                                                    <button class="btn-warning fa fa-exclamation-triangle btn-group btn-lg" title="Not-Verified"></button>
                                                <?php }
                                                ?>
                                            </form>
                                            <form action="" method="post" style="display: inline;">
                                                <input type="hidden" name="delete_patient" value="<?= $patients_list['pt_id'] ?>">
                                                <button class="btn-danger fa fa-trash-o btn-group btn-lg" title="Delete-patient" onclick="return confirm('Are you sure you want to delete this item?');"></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php }  } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>