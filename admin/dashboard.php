<?php
include 'header.php';
include 'sidebar.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua fa fa-user-md"></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Doctors</span>
                        <span class="info-box-number"><?php
                            $drow = $admin_mg->all_doctors_apporve_show();
                            if($drow){
                                $result = mysqli_num_rows($drow);
                            echo $result;
                            }
                            
                            ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red fa fa-wheelchair"></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Patients</span>
                        <span class="info-box-number">
                           <?php
                            $prow = $admin_mg->all_patient_show_num();
                            if($prow){
                                $patient = mysqli_num_rows($prow);
                                echo $patient;
                            }
                            ?>             
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green fa fa-hospital-o"></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Diagnostics</span>
                        <span class="info-box-number"><?php
                            $prow = $admin_mg->all_clinic_show_num();
                            if($prow){
                                $clinic = mysqli_num_rows($prow);
                                echo $clinic;
                            }
                            ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="col-xs-12">
                        <h4 style="margin-bottom: 0px; background-color: grey; width: 100%; padding: 10px; color: #FFF; border-radius: 2px;" class="fa fa-align-justify">  All Stats</h4>
                        <div>
                            <table class="text-left table table-hover">
                                <tr>
                                    <td>Latest Appointment</td>
                                    <td class="text-blue"><h3><u>
                                                <?php
                                                $apt_list = $admin_mg->appointment_list();
                                                if (!empty($apt_list)) {
                                                    $total_row = @mysqli_num_rows($apt_list);
                                                    ?>
                                                    <a href="appointment-list"><?= $total_row; ?></a>
                                                    <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
                                <tr>
                                    <td>Approved Appointment</td>
                                    <td class="text-blue"><h3><u>
                                                <?php
                                                $approved_list = $admin_mg->approved_appointment_list();
                                                if (!empty($approved_list)) {
                                                    $total_approved = @mysqli_num_rows($approved_list);
                                                    ?>
                                                    <a href="approved-appointment-list"> <?= $total_approved; ?></a>
                                                    <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
                                <tr>
                                    <td>All Patient</td>
                                    <td class="text-blue"><h3><u>
                                                <?php if (!empty($patient)) { ?>
                                                    <a href="patient-list"><?= $patient; ?></a>
                                                <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
								<tr>
                                    <td>Paid Patients</td>
                                    <td class="text-blue"><h3><u>
                                                <?php
                                                $paid_patients = $admin_mg->paid_patients();
                                                if (!empty($paid_patients)) {
                                                    $total_paid_patients = @mysqli_num_rows($paid_patients);
                                                    ?>
                                                    <a href="patient-payment-list"> <?= $total_paid_patients; ?></a>
                                                    <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
                                <tr>
                                    <td>Active Doctors</td>
                                    <td class="text-blue"><h3><u>
                                                <?php if (!empty($result)) { ?>
                                                    <a href="doctor-list"><?= $result; ?></a>
                                                <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
                                <tr>
                                    <td>Inactive/Not Approved Doctors</td>
                                    <td class="text-blue"><h3><u>
                                                <?php 
												$inactive_doctors = $admin_mg->inactive_doctors();
												if (!empty($inactive_doctors)) { 
												$all_inactive_doctors = @mysqli_num_rows($inactive_doctors);?>
                                                    <a href="doctor-list"><?= $all_inactive_doctors; ?></a>
                                                <?php
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </u></h3></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'footer.php'; ?>
