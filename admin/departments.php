<?php 
  include 'header.php';
  include 'sidebar.php';
?>
<?php
$msg = array();
  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['dep_btn'])){
    $msg = $admin_mg->save_dep_cat($_POST, $_FILES);
  }
  $result = $admin_mg->show_dep_cat();

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-5">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Department</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
              <?php 
                if(isset($msg['success'])){
                  printf('<span class="error_success">%s</span>', $msg['success']); 
                }
              ?>
            <form method="post" action="" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label for="dep_title">Department Title </label>
                    <input class="form-control" name="dep_title" id="dep_title" type="text">
                    <?PHP if(isset($msg['dep_title'])){printf('<span class="error">%s</span>', $msg['dep_title']);} ?>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="dep_description">Description </label>
                    <textarea class="form-control" name="dep_description" id="dep_description"></textarea>
                    <?PHP if(isset($msg['dep_description'])){printf('<span class="error">%s</span>', $msg['dep_description']);} ?>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="image">Catagory Photo </label>
                    <input name="image" id="image" type="file">
                    <?PHP if(isset($msg['image'])){printf('<span class="error">%s</span>', $msg['image']);} ?>
                  </div>
                  <?php
                    if(isset($msg['error'])){
                  printf('<span class="error-msg">%s</span>', $msg['error']); 
                }
                  ?>
                  <div class="col-md-12">
                    <div class="button_sep text-right">
                       <input class="btn btn-info" id="submitbutton" type="submit" name="dep_btn" value="Submit">
                    </div>
                  </div>        
                </div>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Department</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                
                  <table class="t-stlye">                  
                    <tr>
                      <th>Title</th>
                      <th>Description</th>
                      <th>image</th>
                    </tr>
                    <?php
                    
                      while($row = $result->fetch_assoc()){ 
                    ?>
                    <tr>
                      <td><?php echo $row['dep_title']; ?></td>
                      <td><?php echo $row['dep_description']; ?></td>
                      <td><img src="<?php echo $row['image']; ?>" width="150" height="150"></td>
                    </tr>
                    <?php } ?>                  
                  </table>
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'footer.php'; ?>
