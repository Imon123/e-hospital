<?php
include 'header.php';
include 'sidebar.php';

if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['patient_id'])) {
    $patient_id = $_GET['patient_id'];
    $patient_details_by_id = $admin_mg->patient_transaction_history($patient_id);
}
if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['verify'])) {
    $patient_id = $_GET['patient_id'];
    $payment_id = $_POST['verify'];
    $admin_mg->payment_verify($payment_id, $_POST);
    $patient_details_by_id = $admin_mg->patient_transaction_history($patient_id);
}
?>

<section class="content-wrapper">
    <div class="container" style="padding: 50px 0px">
        <div class="row"> 
            <div class="col-xs-12 height-control">
                <div class="row">

                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <?php if (!empty($patient_details_by_id)) { ?>
                            <div class="col-xs-8">
                                <table class="table-hover">
                                    <tr>
                                        <td>
                                            <h1 class="text-bold" style="color: blue; font-weight: bold; display: inline;"><?= $patient_details_by_id['first_name'] . ' ' . $patient_details_by_id['last_name'] ?></h1>

                                            <?php if ($patient_details_by_id['payment_approve_status'] == 1) { ?>
                                                <span style="margin-left: 20px;" class="btn-info fa fa-check-square-o btn-group btn-sm" title="Payment-Verified"></span>
                                            <?php } else { ?>

                                                <form action="" method="post" style="display: inline;">
                                                    <?php
                                                    if ($patient_details_by_id['package_day'] == "1 year") {
                                                        ?> 
                                                        <input type="hidden" name="expire_date" value="<?= 365 ?>">
                                                    <?php } elseif ($patient_details_by_id['package_day'] == "6 months") { ?>
                                                        <input type="hidden" name="expire_date" value="<?= 182 ?>">
                                                    <?php } ?>
                                                    <input type="hidden" name="verify" value="<?= $patient_details_by_id['id'] ?>">
                                                    <input type="hidden" name="pt_id" value="<?= $patient_details_by_id['pt_id'] ?>">
                                                    <button style="margin-left: 20px;" class="btn-warning fa fa-exclamation-triangle btn-group btn-sm" title="Not-Verified"></button>

                                                </form>
                                            <?php }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address : <?= $patient_details_by_id['address'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No. : <?= $patient_details_by_id['mobile_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email : <?= $patient_details_by_id['email'] ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-xs-4" style="padding-bottom: 30px">
                                <table class="table-hover table-bordered">
                                    <tr>
                                        <td><img src="../<?= $patient_details_by_id['profile_img'] ?>" width="200" height="200"/></td>
                                    </tr>
                                </table>
                            </div>


                            <div class="col-xs-12">
                                <div style="border-bottom: 2px solid #000;">

                                </div>
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Patient Transaction Details</u></h4>
                                <table class="table-hover">
                                    <tr>
                                        <td>Transaction ID</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['transection_id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Paid Amount</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['amount'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Package Take For</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['package_day']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Transaction Date</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['pay_date']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Approve Status</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?php if ($patient_details_by_id['payment_approve_status'] == 0) { ?> Not Approved
                                            <?php } else { ?>
                                                Approved<?php } ?></td>
                                    </tr>
                                </table>
                            </div>
                            <?php
                        } else {
                            echo "NO DATA AVAILABLE";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>