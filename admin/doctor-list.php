<?php
include 'header.php';
include 'sidebar.php';

$doctor_list = $admin_mg->doctor_list();

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete_doctor'])){
    $doctor_id = $_POST['delete_doctor'];
    $admin_mg->delete_doctor_all_info($doctor_id);
    $doctor_list = $admin_mg->doctor_list();
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['approved_him'])){
    $approved_him = $_POST['approved_him'];
    $admin_mg->approve_this_doctor($approved_him);
    $doctor_list = $admin_mg->doctor_list();
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['unapprove'])){
    $unapproved_him = $_POST['unapprove'];
    $admin_mg->unapprove_this_doctor($unapproved_him);
    $doctor_list = $admin_mg->doctor_list();
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Doctor List
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="text-center table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Sl. No.</th>
                                <th>Name</th>
                                <th>Specialists</th>
                                <th>Image</th>
                                <th colspan="">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        while ($all_doctors = $doctor_list->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $i; ?></td>
                                <td><?= $all_doctors['first_name'] . " " . $all_doctors['last_name']; ?></td>
                                <td><?= $all_doctors['specialists']; ?></td>
                                <td><img src="../<?= $all_doctors['profile_image']; ?>" height="70" width="65"></td>
                                <td>
                                    <form action="view-doctor-profile" method="get" style="display: inline;">
                                        <input type="hidden" name="doctor_id" value="<?= $all_doctors['d_id'] ?>">
                                        <button class="btn-info fa fa-eye btn-group btn-lg" title="View doctor profile"></button>
                                    </form>
                                    <form action="" method="post" style="display: inline;">
                                        <?php
                                            if($all_doctors['approval_status'] == 1){?>
                                        <input type="hidden" name="unapprove" value="<?= $all_doctors['d_id'] ?>">
                                            <button class="btn-primary fa fa-check-circle btn-group btn-lg" title="Approved"></button>
                                            <?php }else{ ?>
                                            <input type="hidden" name="approved_him" value="<?= $all_doctors['d_id'] ?>">
                                                <button class="btn-warning fa fa-exclamation-triangle btn-group btn-lg" title="Please-Approve"></button>
                                          <?php  }
                                        ?>
                                    </form>
                                    <form action="" method="post" style="display: inline;">
                                        <input type="hidden" name="delete_doctor" value="<?= $all_doctors['d_id'] ?>">
                                        <button class="btn-danger fa fa-trash-o btn-group btn-lg" title="Delete-doctor" onclick="return confirm('Are you sure you want to delete this item?');"></button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>

