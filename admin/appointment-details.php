<?php
include 'header.php';
include 'sidebar.php';
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['appoint_id'])) {
    $appoint_id = $_GET['appoint_id'];
	$appointment_history = $admin_mg->appointment_history_by_appoint_id($appoint_id);
}


if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['approved_him'])){
    $approved_him = $_POST['approved_him'];
    $admin_mg->approve_this_appointment($approved_him);
    $appoint_id = $_GET['appoint_id'];
	$appointment_history = $admin_mg->appointment_history_by_appoint_id($appoint_id);
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['unapprove'])){
    $unapproved_him = $_POST['unapprove'];
    $admin_mg->unapprove_this_appointment($unapproved_him);
    $appoint_id = $_GET['appoint_id'];
	$appointment_history = $admin_mg->appointment_history_by_appoint_id($appoint_id);
}

?>

<section class="content-wrapper">
    <div class="container" style="padding: 50px 0px">
        <div class="row">					
            <div class="col-xs-9">
                <div class="mystate">
                    <div id="pup_message"></div>
                    <div class="row">
                     <?php //include '../patient-panel/membership-message.php'; ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <?php
							if(!empty($appointment_history)):
                                while($appointment_details = $appointment_history->fetch_assoc()){
                            ?>
                            <div class="app-details">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="p-image">
                                            <?php if(!empty($appointment_details['profile_image'])){ ?>
													<img class="preview_photo" src="../<?= $appointment_details['profile_image'] ?>" width="150" height="150"/>
                                            <?php }else{ ?>
													<img src="../images/avatar-default.png" class="img-responsive" alt="avatar default">
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <h3>
										<form action="view-doctor-profile" method="get">
											<input type="hidden" name="doctor_id" value="<?= $appointment_details['d_id'] ?>">
											<button><span class="doctor-title" style="color:#3c8dbc;">Dr. <?= $appointment_details['first_name'] . ' ' . $appointment_details['last_name'] ?></span></button>
										</form>
										</h3>
                                        <div class="qualif">
                                            <?php
                                            $edu_result = $doctor->get_education_info_by_id($appointment_details['d_id']);
                                            if ($edu_result):
                                                while ($edu_row = $edu_result->fetch_assoc()):
                                                    $degree[] = $edu_row['degree_form'] . ' (' . $edu_row['major_subject'] . ') ';
                                                    $print_degree = implode(',  ', $degree);
                                                endwhile;
                                                echo '<p>' . $print_degree . '</p>';
                                            endif;
                                            ?>
                                        </div>
                                        <div class="dp-dg"><?= $appointment_details['specialists']; ?></div>
                                    </div>
                                    <div class="col-sm-5" style="text-align: right;">
											<form action="" method="post" style="display: inline;">
												<?php
													if($appointment_details['appoint_approval_status'] == 1){?>
													<input type="hidden" name="unapprove" value="<?= $appointment_details['appoint_id'] ?>">
													<button class="btn-primary  btn-group btn-lg" title="This Is Approved">Already Approved</button>
													<?php }else{ ?>
													<input type="hidden" name="approved_him" value="<?= $appointment_details['appoint_id'] ?>">
														<button class="btn-warning  btn-group btn-lg" title="Please-Approve-Appointment">Approve This Appointment</button>
												  <?php  }
												?>
											</form>
                                    </div>
                                    <div class="col-xs-12">  
                                        <hr>
                                        <div class="spacific-problem"><span><b>
										<form action="view-patient-profile" method="get">
											<input type="hidden" name="patient_id" value="<?= $appointment_details['pt_id'] ?>">
											Appointment by <button><b><span style="color:#3c8dbc;"><?= $appointment_details['pt_fname'] . ' ' . $appointment_details['pt_lname'] ?> </span>:</b></button>
											<span><?= 
											//$fm->getDate($appointment_details['created_at'], 'jS M Y,  g:i:s'); 
											$appointment_details['created_at']
											?></span>
										</form>
                                        
                                        </span> <p><?= $appointment_details['problem']; ?></p></div>
                                        <?php if(!empty($appointment_details['problem_image'])): ?>
                                        <div class="image-gellary">
                                            <?php
                                             $array =  explode(',', $appointment_details['problem_image']);
                                             foreach ($array as $value) {
                                                $value = str_replace(' ','',$value);
                                                echo '<a rel="example_group" href="../'.$value.'" title=""><img alt="" src="../'.$value.'"  width="150" height="150"/></a>';
                                              } 
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(!empty($appointment_details['audio_video'])): ?>
                                    <div class="audio-message">
                                        <?php 
                                            $audio_type     = array( 'mp3', 'ogg', 'wav');
                                            $audio_divided = explode('.', $appointment_details['audio_video']);
                                            $audio_ext      = strtolower( end($audio_divided) );
                                            if( in_array( $audio_ext, $audio_type) === TRUE ){
                                        ?>
                                        <audio controls>
                                          <source src="../<?= $appointment_details['audio_video']; ?>" type="audio/mpeg"/>
                                          <source src="../<?= $appointment_details['audio_video']; ?>" type="audio/ogg"/>
                                          <source src="../<?= $appointment_details['audio_video']; ?>" type="audio/wav"/>
                                            Your browser does not support the video tag.
                                        </audio>
                                        <?php }else{ ?>
                                            <video width="320" height="240" controls>
                                              <source src="../<?= $appointment_details['audio_video']; ?>" type="video/mp4">
                                              <source src="../<?= $appointment_details['audio_video']; ?>" type="video/ogg">
                                              <source src="../<?= $appointment_details['audio_video']; ?>" type="video/webm">
                                            Your browser does not support the video tag.
                                            </video>
                                        <?php } ?>
                                    </div>
                                    <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php   
                                }
                                else: 

                                endif; 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
					
					
			<?php ?>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>