(function($) { 
  $("form[name='clinic_reg']").validate({
    // Specify validation rules
      rules: {
      clinic_name: {
        required: true,
        alphas: true
      },
      clinic_location: "required",
      ownertype: "required",
      ownername: {
        required: true,
        alphas: true
      },
      clinictype: "required",
      licnumber: {
        required: true,
        numeric:true
      },
      cemail: {
        required: true,
        email: true
      },
      cmobile_no: {
        required: true,
        numeric: true
      },
      clinicaddress: "required",
      passwordc: {
        required: true,
        minlength: 5,
        maxlength: 15
      },
      repasswordc: {
          required: true,
          equalTo: "#passwordc"
      }
      
    },
    // Specify validation error messages
    messages: {
      clinic_name: {
        required: "Please enter your clinic name",
        alphas : "Please enter only alphabets and space."
      },
      ownername: {
        required: "Please enter your ownername",
        alphas : "Please enter only alphabets and space."
      },
      licnumber: {
        required: "Please enter license No",
        numeric:"Please enter only no. For example 012xxxxxxxx"
      },
      cmobile_no: {
        required: "Please enter your phone no.",
        numeric:"Please enter a valid cell phone no. For example 019xxxxxxxx"
      },
      passwordc: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long",
        maxlength: "Your password must be less than 10 characters"
      },
      cemail:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var c_name     = $('#clinic_name').val();
    var c_location = $('#clinic_location').val();
    var ownertype  = $('#ownertype').val();
    var ownername  = $('#ownername').val();
    var c_type     = $('#clinictype').val();
    var licnumber  = $('#licnumber').val();
    var cemail      = $('#cemail').val();
    var cmobile_no     = $('#cmobile_no').val();
    var startdate  = $('#startdate').val();
    var c_address  = $('#clinicaddress').val();
    var passwordc   = $('#passwordc').val();
    var clibtn     = $('#clinic_button').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'c_name='+c_name+'&c_location='+c_location+'&ownertype='+ownertype+'&ownername='+ownername+'&c_type='+c_type+'&licnumber='+licnumber+'&cemail='+cemail+'&cmobile_no='+cmobile_no+'&startdate='+startdate+'&c_address='+c_address+'&passwordc='+passwordc+'&clibtn='+clibtn,
      type : 'POST',
      success:function(result){
        if(result == "checkmail"){
          $('#ci_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> You are already registered !</div>');
        }else if(result == "mail"){
              $("#clinicsignup").remove();
              $('#ci_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Registered Successfully, Wait a second!<i class="fa fa-check-circle-o"></i> <img class="loading-img" src="images/loading.gif" alt=""></div>');
              function redirect_page()
              {
                document.location="login.php";
              }
              setTimeout(redirect_page,5000);
        }else{
            $("#clinicsignup").remove();
            $('#ci_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Error in registering...Please try again later!! </div>'); 
            function hide_message()
              {
               $('#ci_notify').fadeOut('slow');
               $('#clinicmodel').remove().fadeOut('slow');
              }
              setTimeout(hide_message,5000);
        }
      },
    });
    return false;             
  }

  });

  // clinic login 

   $("form[name='clogin_form']").validate({
    // Specify validation rules
    rules: {
      cemail: {
        required: true,
        email: true
      },

      cpassword: {
        required: true
      }
      
    },
    // Specify validation error messages
    messages: {
    
      cpassword: {
        required: "Please provide a password",
      },
      cemail:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var cemail      = $('#cemail').val();
    var cpassword   = $('#cpassword').val();
    var clogin      = $('#clogin-submit').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'cemail='+cemail+'&cpassword='+cpassword+'&clogin='+clogin,
      type : 'POST',
      success:function(result){
        if(result == "csuccesLogon"){
          $('#clogin_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Login Successfully, Wait a second!<i class="fa fa-check-circle-o"></i><div class="pt_login"><img class="loading-img" src="images/loading.gif" alt=""></div></div>');
          function redirect_page()
          {
            document.location="clinic-panel";
          }
          setTimeout(redirect_page,5000);
        }else{
          $('#clogin_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Email Or Password Invalid... Please Try Again!! </div>');    
        }
      },
    });
    return false;             
  }

  });

 jQuery.validator.addMethod("alphas", function(value, element) {
      // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[a-zA-Z ]+$/.test( value );
      });
      jQuery.validator.addMethod("numeric", function(value, element) {
        // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[\-+]?[0-9]*\.?[0-9]+$/.test( value );
      });
})( jQuery );


(function($) {
  $("form[name='edit_clinic_reg']").validate({
    // Specify validation rules
    rules: {
      clinic_name: {
        required: true,
        alphas: true
      },
      clinic_location: "required",
      ownertype: "required",
      ownername: {
        required: true,
        alphas: true
      },
      clinictype: "required",
      licnumber: {
        required: true,
        numeric:true
      },
      cemail: {
        required: true,
        email: true
      },
      mobile_no: {
        required: true,
        numeric: true
      },
      clinicaddress: "required",
      
    },
    // Specify validation error messages
    messages: {
      clinic_name: {
        required: "Please enter your clinic name",
        alphas : "Please enter only alphabets and space."
      },
      ownername: {
        required: "Please enter your ownername",
        alphas : "Please enter only alphabets and space."
      },
      licnumber: {
        required: "Please enter license No",
        numeric:"Please enter only no. For example 012xxxxxxxx"
      },
      mobile_no: {
        required: "Please enter your phone no.",
        numeric:"Please enter a valid cell phone no. For example 019xxxxxxxx"
      },
      cemail:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();           
    }
  });

})( jQuery );


