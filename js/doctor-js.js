(function($){
  $("form[name='doctor_reg']").validate({

    // Specify validation rules
    rules: {
      first_name: {
        required: true,
        alphas: true
      },
      last_name: {
        required: true,
        alphas: true
      },
      idnetity_no: {
        required: true,
        numeric: true
      },
      nidpassport: {
        required: true,
        numeric: true
      },
      email: {
        required: true,
        email: true
      },
      mobile_no: {
        required: true,
        numeric:true
      },
      gender:"required",
      docbirthday: "required",
      password: {
        required: true,
        minlength: 5,
        maxlength: 15
      },
      repassword: {
          required: true,
          equalTo: "#password"
      }
      
    },
    // Specify validation error messages
    messages: {
      first_name: {
        required: "Please enter your firstname",
        alphas : "Please enter only alphabets and space."
      },
      last_name: {
        required: "Please enter your lastname",
        alphas : "Please enter only alphabets and space."
      },
      idnetity_no: {
        required: "Please enter your identity no.",
        numeric:"Please enter a valid identity no."
      },
      nidpassport: {
        required: "Please enter your nid/passport no.",
        numeric:"Please enter a valid nid/passport no."
      },
      mobile_no: {
        required: "Please enter your phone no.",
        numeric:"Please enter a valid cell phone no. For example 019xxxxxxxx"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long",
        maxlength: "Your password must be less than 10 characters"
      },
      email:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var f_name     = $('#first_name').val();
    var l_name     = $('#last_name').val();
    var id_no      = $('#idnetity_no').val();
    var nid        = $('#nidpassport').val();
    var email      = $('#email').val();
    var mobile_no  = $('#mobile_no').val();
    var gender     = $('#gender').val();
    var doctbirth  = $('#docbirthday').val();
    var password   = $('#password').val();
    var doctbtn    = $('#doct_button').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'f_name='+f_name+'&l_name='+l_name+'&id_no='+id_no+'&nid='+nid+'&email='+email+'&mobile_no='+mobile_no+'&doctbirth='+doctbirth+'&gender='+gender+'&password='+password+'&doctbtn='+doctbtn,
      type : 'POST',
      success:function(result){
        if(result == "checkmail"){
          $('#doct_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> You are already registered !</div>');
        }else if(result == "mail"){
              $("#doctorsignup").remove();
              $('#doct_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Registered Successfully, Wait a second!<i class="fa fa-check-circle-o"></i> <img class="loading-img" src="images/loading.gif" alt=""></div>');
              function redirect_page()
              {
                document.location="login.php";
              }
              setTimeout(redirect_page,5000);
        }else{
            $('#erro_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Error in registering...Please try again later!! </div>'); 
        }
      },
    });
    return false;             
  }

  });

  // patient login 

   $("form[name='dlogin_form']").validate({
    // Specify validation rules
    rules: {
      demail: {
        required: true,
        email: true
      },

      dpassword: {
        required: true
      }
      
    },
    // Specify validation error messages
    messages: {
    
      dpassword: {
        required: "Please provide a password",
      },
      demail:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var demail      = $('#demail').val();
    var dpassword   = $('#dpassword').val();
    var dlogin       = $('#dlogin-submit').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'demail='+demail+'&dpassword='+dpassword+'&dlogin='+dlogin,
      type : 'POST',
      success:function(result){
        if(result == "dsuccesLogon"){
          $('#dlogin_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Login Successfully, Wait a second!<i class="fa fa-check-circle-o"></i><div class="pt_login"><img class="loading-img" src="images/loading.gif" alt=""></div></div>');
          function redirect_page()
          {
            document.location="doctor-panel";
          }
          setTimeout(redirect_page,5000);
        }else{
          $('#dlogin_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Email Or Password Invalid... Please Try Again!! </div>');    
        }
      },
    });
    return false;             
  }

  });

 jQuery.validator.addMethod("alphas", function(value, element) {
      // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[a-zA-Z ]+$/.test( value );
      });
      jQuery.validator.addMethod("numeric", function(value, element) {
        // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[\-+]?[0-9]*\.?[0-9]+$/.test( value );
      });

}) ( jQuery );

(function($) { 
   $('input.continue').change(function(){
        if ($(this).is(':checked')){
        $(this).next('div.remove_date').hide();
        $(this).nextAll('div.text_continue').show();
        }else {
        $('div.text_continue').hide();
        $('div.remove_date').show();
        }
    }).change();

  //Date picker
    $('#docbirthday').datepicker({autoclose: true});
    $('#start_date').datepicker({autoclose: true});
    $('#end_date').datepicker({autoclose: true});


  $("#uploadimage").on('submit',(function(e) {
  e.preventDefault();
    $("#message").empty();
    $('#loading').show();
    var d_id = $('#d_id').val();
  $.ajax({
    url: "../ajax-action.php?imgadd="+d_id, // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {

    $('#loading').hide();
    $("#message").html(data);
    }
  });
  }));

  $("#updateimage").on('submit',(function(e) {
  e.preventDefault();
    $("#message").empty();
    $('#loading').show();
    var d_id = $('#d_id').val();
  $.ajax({
    url: "../ajax-action.php?imgadd="+d_id, // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {
    if(data !=''){
        $('#loading').hide();
        $("#message").html(data);
      // hide the message after 3ms
      setTimeout(window.location="edit-profile", 10000);
    }
    }
  });
  }));
  // Function to preview image after validation
  $(function() {
    $("#file").change(function() {
    $("#message").empty(); // To remove the previous error message
    var file = this.files[0];
    var imagefile = file.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
    {
      $('#previewing').attr('src','noimage.png');
      $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
    return false;
    }
    else
    {
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
    }
    });
  });
  function imageIsLoaded(e) {
    $("#file").css("color","green");
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#previewing').attr('width', '200px');
    $('#previewing').attr('height', '200px');
  };

  // doctor online / offline status
  setInterval(function(){
    $("#online_content").load("ajax-action.php?onlinestatus");
  }, 3000); // 2 milisecond

  // doctor online / offline status single page
  setInterval(function() {
        var did = $('#doct_id').val();
        $.ajax({
        url:"ajax-action.php?donline",
        type:"POST",
        async: false,
        data: {"did":did},
        success: function(result){
          $("#detail_online_status").html(result);
        }
      });
     }, 3000);

  // doctor update profile stap
  setInterval(function() {
        var did = $('#dcotor_id').val();
        $.ajax({
        url:"../ajax-action.php?getmenu",
        type:"POST",
        async: false,
        data: {"did":did},
        success: function(result){
          $("#display_profile_menu").html(result);
        }
      });
     }, 5000);


  // latest appointment count
  setInterval(function(){
    var did = $('#dcotor_id').val();
    $("#latest_app").load("../ajax-action.php?latestapp="+did);
    $("#dup_message").load("../ajax-action.php?dupmessage="+did);
    $("#get_appointemnt").load("../ajax-action.php?eachmessage="+did);
  }, 3000); // 2 milisecond
})( jQuery );
// accordion
jQuery(document).ready(function() {
    function close_accordion_section() {
        jQuery('.accordion .accordion-section-title').removeClass('active');
        jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = jQuery(this).attr('href');

        if(jQuery(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();

            // Add active class to section title
            jQuery(this).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        }

        e.preventDefault();
    });
});
displayProfileMenu();
displayDeducation();

function dsaveData(){
      var deduid = $('#deduid').val();
      var degree_title = $('#degree_title').val();
      var degree_form = $('#degree_form').val();
      var institute_name = $('#institute_name').val();
      var passing_year = $('#passing_year').val();
      var major_subject = $('#major_subject').val();
      if(degree_title == '' || degree_title == '' || institute_name == '' || passing_year == '' || major_subject == ''){
        alert("Field Must Not be Empty!!");
      }else{
        $.ajax({
          url: '../ajax-action.php?dedu=add',
          type : 'POST',
          async: false,
          data: "did="+deduid+"&dg_title="+degree_title+"&dg_form="+degree_form+"&int_name="+institute_name+"&pass_year="+passing_year+"&major_sub="+major_subject,
          success:function(result){
            if(result == "yes"){
              alert("The information has been insertd successfully");
            }else{ 
              alert("Not inserted");
            }
              displayDeducation();
              displayProfileMenu(); // get profile step complete menu
          }
        });
        } 
    }
// display data
function displayDeducation(){
        var deduid = $('#deduid').val();
        $.ajax({
        url:"../ajax-action.php?getdedu",
        type:"POST",
        async: false,
        data: {"did":deduid},
        success: function(result){
          $("#display_dedu").html(result);
        }
      });
}
// display menu stap
function displayProfileMenu(){
        var did = $('#dcotor_id').val();
        $.ajax({
        url:"../ajax-action.php?getmenu",
        type:"POST",
        async: false,
        data: {"did":did},
        success: function(result){
          $("#display_profile_menu").html(result);
        }
      });
}
// update data
function dupdateData(str){
    var dedu_id = str;
    var degree_title = $('#degree_title-'+str).val();
    var degree_form = $('#degree_form-'+str).val();
    var institute_name = $('#institute_name-'+str).val();
    var passing_year = $('#passing_year-'+str).val();
    var major_subject = $('#major_subject-'+str).val();
    $.ajax({
      url: '../ajax-action.php?dedit',
      data: "deduid="+dedu_id+"&degree_title="+degree_title+"&degree_form="+degree_form+"&institute_name="+institute_name+"&passing_year="+passing_year+"&major_subject="+major_subject,
      type : 'POST',
      success:function(data){ 
        if(data == "yes"){
            alert("The information has been updated successfully");
          }else{ 
            alert("Not updated");
          }
          displayDeducation();
          displayProfileMenu(); // get profile step complete menu
      }
    });
}

function deduDelete(str){
  var dedu_id = str;
  var checkstr =  confirm('are you sure you want to delete this?');
  if(checkstr == true){
      $.ajax({
      url: '../ajax-action.php?ddelete',
      data: "deduid="+dedu_id,
      type : 'POST',
      success:function(data){ 
        if(data == "yes"){
            alert("The information has been deleted successfully");
          }else{ 
            alert("Not deleted");
          }
          displayDeducation();
          displayProfileMenu(); // get profile step complete menu
      }
    });
  }else{
    return false;
  }
}


// Training 
displayDtraining();
function dsaveTrainingData(){
      var d_id = $('#d_id').val();
      var t_title = $('#training_title').val();
      var inst_name = $('#institute_name').val();
      var address = $('#address').val();
      var des = $('#description').val();
      if(t_title == '' || inst_name == '' || address == ''){
        alert("Field Must Not be Empty!!");
      }else{
        $.ajax({
          url: '../ajax-action.php?tadd',
          type : 'POST',
          async: false,
          data: "did="+d_id+"&training_title="+t_title+"&institute_name="+inst_name+"&address="+address+"&description="+des,
          success:function(result){
            if(result == "yes"){
              alert("The information has been insertd successfully");
            }else{ 
              alert("Not inserted");
            }
              displayDtraining();
              displayProfileMenu(); // get profile step complete menu
          }
        });
      } 
}

function displayDtraining(){
  var d_id = $('#d_id').val();
    $.ajax({
    url:"../ajax-action.php?gettraining",
    type:"POST",
    async: false,
    data: {"did":d_id},
    success: function(result){
      $("#display_training").html(result);
    }
  });
}

function dupdateTrainingData(str){
    var dtr_id = str;
    var t_title = $('#training_title-'+str).val();
    var inst_name = $('#institute_name-'+str).val();
    var address = $('#address-'+str).val();
    var des = $('#description-'+str).val();
    $.ajax({
      url: '../ajax-action.php?dtedit',
      data: "dtr_id="+dtr_id+"&training_title="+t_title+"&institute_name="+inst_name+"&address="+address+"&description="+des,
      type : 'POST',
      success:function(data){ 
        if(data == "yes"){
            alert("The information has been updated successfully");

          }else{ 
            alert("Not updated");
          }
          displayaining();
          displayProfileMenu(); // get profile step complete menu
      }
    });
}

function dtrainingDelete(str){
  var dtr_id = str;
  var checkstr =  confirm('are you sure you want to delete this?');
  if(checkstr == true){
      $.ajax({
      url: '../ajax-action.php?deltraining',
      data: "dtr_id="+dtr_id,
      type : 'POST',
      success:function(data){ 
        if(data == "yes"){
            alert("The information has been deleted successfully");
            }else{ 
              alert("Not deleted");
            }
          displayDtraining();
          displayProfileMenu(); // get profile step complete menu
      }
    });
  }else{
    return false;
  }
}


// Job history 
displayDjobhistory();
function dsaveJobhistoryData(){
      var d_id = $('#d_id').val();
      var off_name = $('#doc_office_name').val();
      var st_date = $('#start_date').val();
      var e_date = '';
      $('input.continue').change(function(){
        if ($(this).is(':checked')){
          e_date = $('#end_date_text').val();
        }else {
          e_date = $('#end_date').val();
        }
      }).change();
      var off_address = $('#office_address').val();

      if(off_name == '' || st_date == '' || e_date == '' || off_address == ''){
        alert("Field Must Not be Empty!!");
      }else{

        $.ajax({
          url: '../ajax-action.php?jobadd',
          type : 'POST',
          async: false,
          data: "did="+d_id+"&doc_office_name="+off_name+"&start_date="+st_date+"&end_date="+e_date+"&office_address="+off_address,
          success:function(result){
            if(result == "yes"){
              alert("The information has been inserted successfully");
            }else{ 
              alert("Not Inserted");
            }
            displayDjobhistory();
            displayProfileMenu(); // get profile step complete menu
          }
        });
      } 
}

function displayDjobhistory(){
  var d_id = $('#d_id').val();
    $.ajax({
    url:"../ajax-action.php?getjobhistory",
    type:"POST",
    async: false,
    data: {"did":d_id},
    success: function(result){
      $("#display_jobhistory").html(result);
    }
  });
}

function dupdateJobhistoryData(str){
    var doc_job_id = str;
    var off_name = $('#doc_office_name-'+str).val();
    var st_date = $('#start_date-'+str).val();
    var e_date = $('#end_date-'+str).val();
    var off_address = $('#office_address-'+str).val();
    if(off_name == '' || st_date == '' || e_date == '' || off_address == ''){
      alert("Field Must Not be Empty!!");
    }else{
      $.ajax({
        url: '../ajax-action.php?djobedit',
        data: "doc_job_id="+doc_job_id+"&doc_office_name="+off_name+"&start_date="+st_date+"&end_date="+e_date+"&office_address="+off_address,
        type : 'POST',
        success:function(data){
          if(data == "yes"){
              alert("The information has been updated successfully");
            }else{ 
              alert("Not Uupdated");
            }
            displayDjobhistory();
            displayProfileMenu(); // get profile step complete menu
        }
      });
    }
}

function djobhistoryDelete(str){
  var doc_job_id = str;
  var checkstr =  confirm('are you sure you want to delete this?');
  if(checkstr == true){
      $.ajax({
      url: '../ajax-action.php?deljob',
      data: "doc_job_id="+doc_job_id,
      type : 'POST',
      success:function(data){ 
        if(data == "yes"){
          alert("The information has been deleted successfully");
        }else{ 
          alert("Not deleted");
        }
        displayDjobhistory();
        displayProfileMenu(); // get profile step complete menu
      }
    });
  }else{
    return false;
  }
}

// photo delete
function deleteDoctorPhoto(str){
  var d_id = str;
  var checkstr =  confirm('are you sure you want to delete this?');
  if(checkstr == true){
      $.ajax({
      url: '../ajax-action.php?dphtodelete',
      data: "d_id="+d_id,
      type : 'POST',
      success:function(data){
        if(data == "yes"){
            alert("Photo has been deleted successfully");
            window.location="edit-profile";
          }else{ 
            alert("Not deleted");
          }
      }
    });
  }else{
    return false;
  }
}