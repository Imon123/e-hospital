jQuery(document).ready(function($){
  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });

  $('span.toggle-switch').click(function(){
    $('ul.topNavigation').slideToggle(400); 
  });

//login

    $('#login-form-link').click(function(e) {
    $("#login-form").delay(100).fadeIn(100);
    $("#dlogin-form").fadeOut(100);
      $("#clinic-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $('#clinic-login-form').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });

  $('#register-form-link').click(function(e) {
    $("#dlogin-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $("#clinic-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $('#clinic-login-form').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });
    $('#clinic-login-form').click(function(e) {
    $("#clinic-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $("#dlogin-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });
   // Drowpdown js
  $(".select2").select2();

  $("a[rel=example_group]").fancybox({
  'transitionIn'    : 'none',
  'transitionOut'   : 'none',
  'titlePosition'   : 'over',
  'titleFormat'   : function(title, currentArray, currentIndex, currentOpts) {
    return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
  }
});

 });

