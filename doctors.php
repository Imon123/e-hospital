<?php 
include 'header.php'; 

$result = $doctor->get_all_approved_doctor_list();
$row = mysqli_fetch_row($result);
$rows = $row[0];
$page_rows = 12;
$last = ceil($rows/$page_rows);
if($last < 1){
	$last = 1;
}
$pagenum = 1;
if(isset($_GET['pn'])){
	$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
}
if ($pagenum < 1) { 
    $pagenum = 1; 
} else if ($pagenum > $last) { 
    $pagenum = $last; 
}
$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
$paginationCtrls = '';
if($last != 1){
	if ($pagenum > 1) {
        $previous = $pagenum - 1;
		$paginationCtrls .= '<a class="prev" href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'"><<</a>';
		for($i = $pagenum-4; $i < $pagenum; $i++){
			if($i > 0){
		        $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a>';
			}
	    }
    }
	$paginationCtrls .= '<span class="active">'.$pagenum.'</span>';
	for($i = $pagenum+1; $i <= $last; $i++){
		$paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a> ';
		if($i >= $pagenum+4){
			break;
		}
	}

    if ($pagenum != $last) {
        $next = $pagenum + 1;
        $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'">>></a> ';
    }
}
$doctor_result = $doctor->get_all_approved_doctory($limit=null);
$specialist_result = $doctor->get_all_approved_specialist_doctor();
?>
<section class="Doctor-session-wrp sec-pdd3">
	<div class="container mystate">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="h_title">Our Doctors</h1>
				<h4 class="doc-sub-title">Specialists</h4>
            </div>
	        <div class="col-xs-12 col-sm-4">
				<form action="search-doctors" method="get">
	              <div class="form-group">
	                <label>Search Specialists</label>
	                <select name="s" onchange="this.form.submit()" class="form-control select2" style="width: 100%;">
	                  <option value="">Choosen Specialists</option>
					<?php 
						if($specialist_result):
						while ($value = $specialist_result->fetch_assoc()){
					?>
	                  <option value="<?=  $value['specialists']; ?>"><?=  $value['specialists']; ?></option>
	                  <?php } endif; ?>
	                </select>
	              </div>
		          </form>
	        </div>
		</div>
		<div class="row">
			<div id="">
			<?php 
			if($doctor_result):
			while($value = $doctor_result->fetch_assoc()){?>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="doc-sty-box">
					<a href="doctor-details?d=<?= $value['slug']; ?>">
						<?php if(!empty($value['profile_image'])){ ?>
						<img src="<?= $value['profile_image']; ?>" class="img-responsive" alt="<?= $value['first_name']; ?>">
						<?php }else{ ?>
						<img src="images/avatar-default.png" class="img-responsive" alt="avatar default">
						<?php } ?>
					</a>
					<div class="doc-sty-inner">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
						<h5>
						<div id="online_status">
							<?php 
								if($value['online_status'] == 1){
									echo '<span class="online"></span>';
								}else{
									echo '<span class="offline"></span>';
								}
							?>
						</div>
							<a href="doctor-details?d=<?php echo $value['slug']; ?>">Dr. <?= $value['first_name'].' '.$value['last_name']; ?></a>
						</h5>
						<span><?= $value['specialists']; ?></span>
					</div>
				</div>
			</div>
			<?php } endif; ?>
			</div>
		</div>
	<!-- pagination-->
	<div class="pagination">
		<?php echo $paginationCtrls; ?>
	</div>
	<!-- pagination-->
	</div>
</section>
<?php include 'footer.php'; ?>