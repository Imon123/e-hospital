<?php 
include 'header.php'; 
if(isset($_GET['s']) && $_GET['s'] != null){
	$doctor_result = $doctor->get_all_search_doctor($_GET['s']);
}
$specialist_result = $doctor->get_all_approved_specialist_doctor();
?>
<section class="Doctor-session-wrp sec-pdd1">
	<div class="container mystate">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="h_title">Our Doctors</h1>
				<h4 class="doc-sub-title">Specialists</h4>
            </div>
	        <div class="col-xs-12 col-sm-4">
				<form action="" method="get">
	              <div class="form-group">
	                <label>Search Specialists</label>
	                <select name="s" onchange="this.form.submit()" class="form-control select2" style="width: 100%;">
	                  <option value="">Choosen Specialists</option>
					<?php 
						if($specialist_result):
						foreach ($specialist_result as $value){
					?>
	                  <option <?php if($_GET['s'] == $value['specialists']){ echo 'selected'; }else{ echo '';}?> value="<?=  $value['specialists']; ?>"><?=  $value['specialists']; ?></option>
	                  <?php } endif; ?>
	                </select>
	              </div>
		          </form>
	        </div>
		</div>
		<div class="row">
			<div id="search_content">
			<?php 
			if($doctor_result):
			foreach ($doctor_result as $value){?>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="doc-sty-box">
					<a href="doctor-details?d=<?= $value['slug']; ?>">
						<?php if(!empty($value['profile_image'])){ ?>
							<img src="<?= $value['profile_image']; ?>" class="img-responsive" alt="<?= $value['first_name']; ?>">
							<?php }else{ ?>
							<img src="images/avatar-default.png" class="img-responsive" alt="avatar default">
						<?php } ?>
					</a>
					<div class="doc-sty-inner">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
						<h5>
						<div id="online_status">
							<?php 
								if($value['online_status'] == 1){
									echo '<span class="online"></span>';
								}else{
									echo '<span class="offline"></span>';
								}
							?>
						</div>
							<a href="doctor-details?d=<?php echo $value['slug']; ?>">Dr. <?= $value['first_name'].' '.$value['last_name']; ?></a>
						</h5>
						<span><?= $value['specialists']; ?></span>
					</div>
				</div>
			</div>
			<?php } endif; ?>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>