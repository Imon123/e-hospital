<?php 
   include 'header.php'; 
    $ptLogin = Session::session_get('ptLogin');
    $dLogin = Session::session_get('dLogin');
    $cLogin = Session::session_get('cLogin');
    if($ptLogin == true){
        header("Location:patient-panel");
    }elseif($dLogin == true){
        header("Location:doctor-panel");
    }elseif($cLogin == true){
        header("Location:clinic-panel");
    }
?>
    <div class="login-area sec-pdd1">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-4 loginMargin">
                                    <a href="#" class="active" id="login-form-link">Patient Login</a>
                                </div>
                                <div class="col-xs-4 registerAfter loginMargin">
                                    <a href="#" id="register-form-link">Doctor Login</a>
                                </div>
                                <div class="col-xs-4 registerAfter">
                                    <a href="#" id="clinic-login-form">Diagnostic Login</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="ptlogin_notify"></div>
                                    <form name="patient-login" id="login-form" style="display: block;">
                                        <div class="form-group">
                                            <input type="text" name="email" id="email" class="form-control" placeholder="Username">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn-primary btn-block" value="Patient LogIn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="" tabindex="5" class="forgot-password">Forgot Password?</a> / <a href="create-account">Create account</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div id="dlogin_notify"></div>
                                    <form name="dlogin_form" id="dlogin-form"role="form" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" name="demail" id="demail" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="dpassword" id="dpassword" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="submit" name="dlogin-submit" id="dlogin-submit" tabindex="4" class="form-control btn-block btn-primary" value="Doctor LogIn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="" tabindex="5" class="forgot-password">Forgot Password?</a> / <a href="create-account">Create account</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                  <div id="clogin_notify"></div>
                                    <form name="clogin_form" id="clinic-form" role="form" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" name="cemail" id="cemail" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="submit" name="clogin-submit" id="clogin-submit" tabindex="4" class="form-control btn-block btn-primary" value="Clinic LogIn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="" tabindex="5" class="forgot-password">Forgot Password?</a> / <a href="create-account">Create account</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php';?>    