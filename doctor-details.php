<?php
include 'header.php';
if (isset($_GET['d']) && $_GET['d'] != NULL) {
    $drow = $doctor->get_doctor_allinfo_by_id($_GET['d']);
    $reldoc_result = $doctor->get_relative_doctor($_GET['d']);
}
if($dLogin == true){

}elseif($cLogin == true){

}else{
    if (isset($ptID)) {
        $pt_prf = $patient->get_patient_profile_by_id($ptID);
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['makeappointment'])) {
    $patient->make_appointment($_FILES, $_POST);
}
?>
<section class="Doc-pro-wrp">
    <div class="container mystate">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h1 class="h_title">Doctor Profile</h1>
                <h4 class="doc-sub-title">Specialists</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php if(!empty($drow['profile_image'])){ ?>
                <img src="<?= $drow['profile_image']; ?>" class="img-circle doc-img" alt="Doctor">
                <?php }else{ ?>
                <img src="images/avatar-default.png" class="img-responsive" alt="avatar default">
                <?php } ?>
            </div>
            <div class="col-md-8">
                <div class="dp-box">
                    <h3 class="details-online-status">
                        <input type="hidden" id="doct_id" value="<?= $drow['d_id']; ?>">
                        <div id="detail_online_status">
                            <?php
                            if ($drow['online_status'] == 1) {
                                echo '<span class="online"></span>';
                            } else {
                                echo '<span class="offline"></span>';
                            }
                            ?>
                        </div>
                        <span class="doctor-title">Dr. <?= $drow['first_name'] . ' ' . $drow['last_name'] ?></span>
                    </h3>
                    <div class="qualif">
                        <?php
                        $edu_result = $doctor->get_education_info_by_id($drow['d_id']);
                        if ($edu_result):
                            while ($edu_row = $edu_result->fetch_assoc()):
                                $degree[] = $edu_row['degree_form'] . ' (' . $edu_row['major_subject'] . ') ';
                                $print_degree = implode(',  ', $degree);
                            endwhile;
                            echo '<p>' . $print_degree . '</p>';
                        endif;
                        ?>
                    </div>
                    <div class="dp-dg"><?= $drow['specialists']; ?></div>
                    <div class="doc-sicon">
                        <ul>
                            <li><a href="#" class="doc-fb"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="doc-twi"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="doc-gplus"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                    <?php
                    if (!empty($drow['self_about'])) {
                        echo '<p>' . $drow['self_about'] . '</p>';
                    } else {
                        $pdd_class = 'padd-top';
                    }
                    ?>
                    <div class="bt-appo <?php echo $pdd_class; ?>" id="pappintment">
                        <?php
                        if($dLogin == true){}elseif($cLogin == true){}else{
                            if ($ptLogin != false) {
                                $userMembershipDate = $pt_prf['expire_start'];
                                $membershipEnds = date("Y-m-d ", strtotime(date("Y-m-d", strtotime($userMembershipDate)) . ' + ' . $pt_prf['expire_date'] . ' day'));
                                $ex_dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                                if ($pt_prf['expire_date'] == '') {
                                    echo '<a class="btn-doc dv-btn btnbg" id="firsttime_membership" href="#">make an appointment</a>';
                                    echo '<h4 id="first_expired" class="expired">Please make first membership! <a href="patient-panel/package">click here</a></h4>';
                                } else {
                                    if ($ex_dt->format('Y-m-d') < $membershipEnds) {
                                        echo '<a class="btn-doc dv-btn btnbg" href="#make_appointment" data-toggle="modal">make an appointment</a>';
                                    } else {
                                        echo '<a class="btn-doc dv-btn btnbg" id="end_membrship" href="#">make an appointment</a>';
                                        echo '<h4 id="pexpired" class="expired">Membership has expired. Please renew! <a href="patient-panel/package">click here</a></h4>';
                                    }
                                }
                            } else {
                                echo '<a class="btn-doc dv-btn btnbg" id="payment_btn" href="#">make an appointment</a>';
                            }  
                        }

                        ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="wk-hour">
                                <h3 class="mb10">Working hours</h3>
                            </div>
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Friday</th>
                                        <th>Saturday</th>
                                        <th>Sunday</th>
                                        <th>Monday</th>
                                        <th>Wednesday</th>
                                        <th>Tuesday</th>
                                        <th>Thursday</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr class="success">
                                        <td>10:00am-06:00pm</td>
                                        <td>--:-- --:--</td>
                                        <td>10:00am-06:00pm</td>
                                        <td>10:00am-06:00pm</td>
                                        <td>--:-- --:--</td>
                                        <td>--:-- --:--</td>
                                        <td>10:00am-06:00pm</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php if($reldoc_result): ?>
<section class="Doctor-session-wrp dsdbg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="h_title">Doctors</h1>
                <h4 class="doc-sub-title">Same department</h4>	
            </div>
        </div>				
        <div class="row">
            <?php 
            foreach ($reldoc_result as $value){?>
            <div class="col-md-3 col-lg-3 ">
                <div class="doc-sty-box">
                    <a href="doctor-details?d=<?= $value['slug']; ?>">
                        <?php if(!empty($value['profile_image'])){ ?>
                        <img src="<?= $value['profile_image']; ?>" class="img-responsive" alt="<?= $value['first_name']; ?>">
                        <?php }else{ ?>
                        <img src="images/avatar-default.png" class="img-responsive" alt="avatar default">
                        <?php } ?>
                    </a>
                    <div class="doc-sty-inner">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                        <h5>
                        <div id="online_status">
                            <?php 
                                if($value['online_status'] == 1){
                                    echo '<span class="online"></span>';
                                }else{
                                    echo '<span class="offline"></span>';
                                }
                            ?>
                        </div>
                            <a href="doctor-details?d=<?php echo $value['slug']; ?>">Dr. <?= $value['first_name'].' '.$value['last_name']; ?></a>
                        </h5>
                        <span><?= $value['specialists']; ?></span>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- Doctor education modal -->
<div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="make_appointment" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div id="doctor-form" class="doctor-wrapp">
                    <div class="form-outer">
                        <h2>Make your appointment here.</h2>
                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                        <hr>
                        <div class="col-xs-5" style="border: 2px solid #1D74CA">
                            <h4 class="text-bold"><span style="color: #1D74CA" class="doctor-title">Dr. <?= $drow['first_name'] . ' ' . $drow['last_name'] ?></span></h4>
                        </div>
                        <div class="col-xs-5 col-xs-offset-2" style="border: 2px solid #1D74CA; color: #1D74CA;">
                            <h4 class="text-bold"><span class="doctor-title"><?= ucfirst($drow['specialists']); ?></span></h4>
                        </div>
                        <br/><br/><br/>
                        <div id="doct_notify"></div>
                        <form name="" id="" action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="doc_office_name">Please Describe Your Problem With Specific Description!!<span class="required">*</span></label>
                                        <textarea name="problem" style="resize: none;" cols="6" rows="6" class="required form-control" required></textarea>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input name="file[]" id="img_file" type="file" multiple accept="image/*"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Audio or Video</label>
                                        <div class="col-sm-9">
                                            <input name="audiovideo" type="file" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <hr>
                                        <div class="col-sm-12">
                                            <input type="hidden" name="d_id" value="<?= $drow['d_id']; ?>">
                                            <input type="hidden" name="pt_id" value="<?= $ptID; ?>">
                                            <input type="submit"  id="upload" class="upload doct-submit button btn-primary" name="makeappointment" value="Send Request">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php include 'footer.php'; ?>