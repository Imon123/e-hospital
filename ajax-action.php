<?php 
	include_once 'controllers/Patient.php';
	include_once 'controllers/Doctor.php';
	include_once 'controllers/Clinic.php';
	$patient = new Patient();
	$doctor = new Doctor();
	$clinic = new Clinic();
	if(isset($_POST['ptbtn'])){
	   $patient->patient_signup($_POST);
	}else if(isset($_POST['plbtn'])){
	   $patient->check_login($_POST);
	}elseif (isset($_GET['expire_member'])) {
		$patient->get_expire_membership($_POST);
	}elseif(isset($_GET['pimgadd'])){
		$patient->upload_patient_profile_image($_FILES, $_GET['pimgadd']);
	}elseif(isset($_GET['pphtodelete'])){
		$patient->delete_patient_photo($_POST['pt_id']);
	}else if(isset($_POST['doctbtn'])){
	   $doctor->doctor_signup($_POST);
	}elseif(isset($_POST['packageadd'])){
		$patient->save_membership_package($_POST);
	}elseif(isset($_GET['newmessage'])){
		$patient->doctor_patient_discussion_count();
	}elseif(isset($_GET['upmessage'])){
		$patient->get_patient_unread_discussion();
	}elseif(isset($_GET['upmessage_details'])){
		$patient->get_patient_unread_discussion_details();
	}elseif(isset($_GET['presscribe'])){
		$patient->get_patient_presscribe_test();
	}else if(isset($_POST['dlogin'])){
	   $doctor->doctor_check_login($_POST);
	}elseif(isset($_GET['dedu'])){
		$doctor->save_doctor_education($_POST);
	}elseif(isset($_GET['getdedu'])){
		$doctor->get_deducation_ajax($_POST);
	}elseif(isset($_GET['dedit'])){
		$doctor->update_doctor_education($_POST);
	}elseif(isset($_GET['ddelete'])){
		$doctor->delete_doctor_education($_POST['deduid']);
	}elseif(isset($_GET['tadd'])){
		$doctor->save_doctor_training($_POST);
	}elseif(isset($_GET['gettraining'])){
		$doctor->get_dtraining_info($_POST);
	}elseif(isset($_GET['deltraining'])){
		$doctor->delete_doctor_training($_POST['dtr_id']);
	}elseif(isset($_GET['dtedit'])){
		$doctor->update_doctor_training($_POST);
	}elseif(isset($_GET['jobadd'])){
		$doctor->save_doctor_job_history_ajax($_POST);
	}elseif(isset($_GET['getjobhistory'])){
		$doctor->get_djobhistory_info_ajax($_POST['did']);
	}elseif(isset($_GET['deljob'])){
		$doctor->delete_doctor_job_history_ajax($_POST['doc_job_id']);
	}elseif(isset($_GET['djobedit'])){
		$doctor->update_doctor_job_history_ajax($_POST);
	}elseif(isset($_GET['imgadd'])){
		$doctor->upload_docotr_profile_image($_FILES, $_GET['imgadd']);
	}elseif (isset($_GET['getmenu'])) {
		$doctor->profile_stap_complete($_POST['did']);
	}elseif (isset($_GET['onlinestatus'])) {
		$doctor->check_doctor_online();
	}elseif (isset($_GET['donline'])) {
		$doctor->doctor_online_offline_status($_POST);
	}elseif(isset($_GET['dphtodelete'])){
		$doctor->delete_doctor_photo($_POST['d_id']);
	}elseif(isset($_GET['latestapp'])){
		$doctor->latest_appointment_count();
	}elseif(isset($_GET['dupmessage'])){
		$doctor->get_doctor_unread_discussion();
	}elseif(isset($_GET['eachmessage'])){
		$doctor->latest_appointment_list_ajax($_GET['eachmessage']);
	}elseif(isset($_POST['clibtn'])){
		$clinic->clinic_signup($_POST);
	}elseif(isset($_POST['clogin'])){
	   $clinic->clinic_check_login($_POST);
	}

?>