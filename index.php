<?php include 'header.php'; ?>
<section class="about-wrp ptb80">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="images/home2-about.jpg" class="img-responsive" alt="About">
            </div>
            <div class="col-md-6 right_side">
                <div class="about_style">
                    <h1 class="h_title">About us</h1>
                    <h4 class="sub_title">What we are and our history</h4>
                    <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</P>
                    <a href="#" class="btn-doc dv-btn">Read More</a>
                </div>
                <hr>
                <div class="about_style">
                    <h1 class="h_title">vision & mission</h1>
                    <h4 class="sub_title">our goal and thoughts</h4>
                    <P>Opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</P>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="make-appointment-wrp">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="appo-texts">
                    <h1>Make your appointment now</h1>
                    <p>Lorem Ipsum which looks reasonable generated Ipsum is therefore always.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="bt-appo app-btn">
                    <a class="btn-doc dv-btn btnbg" href="doctors">make appointment</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
>

